(begin
;  (define v0 (val "v0"))
;  (define v1 (val "v1"))

  (define x (list 5.05 6.75 3.21 2.66))
  (define y (list 1.65 26.5 -5.93 7.96))

;  (start-building)

; should output 0.72443514**2 = 0.5248062720668195
  (corr2 x y)
)
