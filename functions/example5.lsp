(begin

  (define v0 (val "USC/SAL/214/temp"))
  (define v1 (val "USC/SAL/217/temp"))
  (define v2 (val "USC/SAL/219/temp"))

  (define temps (list v0 v1 v2))

  (start-building)
;  (sum temps)
;  (+ v1 v2)
;  (- v1 v2)
;  (~ v1)
;  (var temps)
  (* v0 v1)
;  v0
)
