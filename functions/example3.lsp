(begin

;  (define v0 (val "USC/SAL/214/temp"))
;  (define v1 (val "USC/SAL/217/temp"))
  (define v0 (val "1"))
  (define v1 (val "2"))

  (define temps (list v0 v1))

  (start-building)

  (sum temps)
)
