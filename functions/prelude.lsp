(begin
  (define len
    (lambda (l)
      (if (equal? l ())
        0
        (+ 1 (len (cdr l))))))

  (define map
    (lambda (f l)
      (if (equal? l ())
      ()
      (cons (f (car l)) (map f (cdr l))))))

  (define fold
    (lambda (f l)
      (if (equal? (cdr l) ())
        (car l)
        (f (car l) (fold f (cdr l))))))

  (define zip
    (lambda (la lb)
      (if (equal? (car la) ())
        ()
        (cons (list (car la) (car lb)) (zip (cdr la) (cdr lb))))))

  (define sum
    (lambda (l)
      (fold + l)))

  (define mul
    (lambda (l)
      (fold * l)))

  (define min
    (lambda (l)
      (fold min2 l)))

  (define max
    (lambda (l)
      (fold max2 l)))

  (define pow
    (lambda (x y)
      (if (equal? y 1)
        x
        (* x (pow x (- y 1))))))

  (define mean
    (lambda (l)
      (/ (sum l) (const (len l)))))

  (define var
    (lambda (l)
      (let ((mu (mean l)))
        (/
          (sum (map (lambda (x) (pow (- x mu) 2))
                    l))
          (len l)))))

  ; Fits the points in vectors xs~ys to a line a*x+c, returns (c a)
  ; Takes two lists of same length, returns a list (intercept gradient)
  ; Coefficients are estimated using OLS:
  ; https://en.wikipedia.org/wiki/Linear_regression#Least-squares_estimation_and_related_techniques
  (define linreg
    (lambda (xs ys)
      (let ((n (len xs))
            (sum_xs (sum xs))
            (sum_ys (sum ys))
            (sum_xs2 (sum (map (lambda (a) (pow a 2)) xs)))
            (sum_xs_ys (sum (map (lambda (pair) (mul pair)) (zip xs ys)))))
        (let ((den (- (* n sum_xs2) (pow sum_xs 2))))
          (list (/
                  (- (* sum_xs2 sum_ys) (* sum_xs sum_xs_ys))
                  den)
                (/
                  (+ (- 0 (* sum_xs sum_ys)) (* n sum_xs_ys))
                  den)
                )))))

  ; Squared correlation (Pearson's product-moment coefficient) of vectors xs and ys
  (define corr2
    (lambda (xs ys)
      (let ((mu_xs (mean xs))
            (mu_ys (mean ys))
            (var_mu (lambda (l mu)
                      (sum (map (lambda (x) (pow (- x mu) 2))
                                l))))
            )
        (/
          (pow (sum (map (lambda (pair) (*
                                          (- (car pair) mu_xs)
                                          (- (car (cdr pair)) mu_ys)
                                          )) (zip xs ys)))
               2)
          (* (var_mu xs mu_xs) (var_mu ys mu_ys))
          ))))
)
