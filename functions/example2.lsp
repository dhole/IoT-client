(begin

  (define v0 (val "v0"))
  (define v1 (val "v1"))
;  (define v2 (val "USC/SAL/219/temp"))

;  (define temps (list v0 v1 v2))
  (define temps (list v0 v1))

  (start-building)
;  (sum temps)
;  (+ v1 v2)
;  (- v1 v2)
;  (~ v1)
;  (min temps)
  (max2 v0 v1)
;  (* v0 v1)
;  v0
)
