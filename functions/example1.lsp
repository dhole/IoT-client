(begin

  (define v1 (val "USC/SAL/214/temp"))
  (define v2 (val "USC/SAL/217/temp"))
  (define v3 (val "USC/SAL/219/temp"))
  (define v4 (val "USC/SAL/223/temp"))
  (define v5 (val "USC/SAL/227/temp"))

  (define temps (list v1 v2 v3 v4 v5))

  (start-building)
  (var temps)
)
