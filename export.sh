#! /bin/sh

WORK=~/Documents/research/IoT
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$WORK/paho.mqtt.c.build/src/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$WORK/libgarble/src/.libs/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$WORK/libgarble/builder/.libs/
