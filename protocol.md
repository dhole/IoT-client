# General information

- Byte arrays are encoded in base64url (RFC 4648).  The reason for this is that
  regular base64 uses the '+' and '/' characters, which can't appear freely in
  an MQTT topic.  base64url replaces them by '-' and ' _ '.

- Every party (Publishers, Subscribers, Broker and Third Party) has a key-pair
  (public and private) for signing.  We call the public key sig.pk.

- Every party is uniquely identified by it's signing public key sig.pk.  This
  key is the basis for authentication: Publishers and Subscribers use the Broker
  and TP's sig.pk to authenticate them.  Broker and TP have a list of sig.pk of
  allowed Publishers and Subscribers.

- Every device will use the first 23 characters of its sig.pk as their Client
  ID for the MQTT protocol.  When we mention the device ID in this document, we
  refer to the full sig.pk in base64url.

- Publishers/Subscribers establish a bidirectional channel to the Broker/TP by
  subscribing and publishing to a device specific topic unique for the device's
  ID.
    - The device specific topic for Publishers is `secfun/v0/pub/{ID}`.
    - The device specific topic for Subscribers is `secfun/v0/sub/{ID}`.

- All messages have the following structure, where `{TYPE}` defines the `msg` type:

```
{
  from: "{ID}",
  type: {MSG_TYPE},
  msg: {MSG},
  sig: "{msg.sig}"
}
```

- A message sent to the device specific topic is always json encoded and has the
  following form, where '{ID}' will either be the Publisher/Subscriber that
  appears in the device specific topic or the Broker/TP:

```
{
  from: "{ID}",
  type: MSG_TYPE_MSG,
  msg: {
    token: "{token}",
    action: {ACTION},
    body: { ... }
  },
  sig: "{msg.sig}"
}
```

- Every message will be signed and verified with "{msg.sig}".
   - To sign a message:
      1. Add `topic: "{topic}"` to the json object, where `{topic}` is the topic
         where the message will be published.
      2. Serialize the json object following a canonical form (`JSON_SORT_KEYS |
         JSON_COMPACT` in jansson).
      3. Generate a signature of the serialized json object using the sender's
         signing private key.
      4. Add `sig: "{msg.sig}"` to the json object, where `"{msg.sig}"` is the
         just computed signature encoded in base64url.

   - To verify a message:
      1. Add `topic: "{topic}"` to the json object, where `{topic}` is the topic
         where the message will be published.
      2. Remove the `sig: "{msg.sig}"` object from the json object, while
         keeping a copy of the decoded `"{msg.sig}"`.
      3. Serialize the json object following a canonical form (`JSON_SORT_KEYS |
         JSON_COMPACT` in jansson).
      4. Verify that the detached signature matches the serialized json object.

- All the keys and byte strings that appear in json objects will be encoded in
  base64url.

- Every transaction between the client and the server will contain a token
  object in the initial message `token: "{token}"`.  All following messages
  related to that transaction will also include the token object in the `msg`
  object.

### Receiving message at server

#### Device specific topic

1. If the `from` is us, ignore packet -> This provably never happens
2. Verify that `from` matches the `{ID}` in the topic
3. Verify that `from` is an allowed pub/sub
4. Verify signature using the `from`, else ignore
5. Obtain action from `msg`
6. Verify that the action is allowed by a client
7. Send message to the parsing function

#### Value publishing topic

1. Verify that `from` matches the `{ID}` in the topic
2. Verify that `from` is an allowed pub
4. Verify signature using the `from`, else ignore
3. Verify that `from` is allowed to publish at `{subtopic}`
4. Send message to the parsing function

#### Function register topic

1. Verify that `from` matches the `{ID}` in the topic
2. Verify that `from` is an allowed sub
4. Verify signature using the `from`, else ignore
4. Send message to the parsing function

### Receiving messages at client

#### Device specific topic

1. If the `from` is us, ignore packet
2. If the `from` is not B or TP, ignore packet
3. Verify signature using the `from`, else ignore
3. Obtain action from `msg`
4. Verify that the action is allowed by the `from` (either B or TP)
5. Send message to the parsing function

#### Subscriber

1. If the `from` is us, ignore packet
2. If the `from` is not B, ignore packet
3. Verify signature using the `from`, else ignore
5. Send message to the parsing function

## Key echange between Publihser and TP to establish the ratchet key rkp

The established rkp will be used to map-label the values sent to the `{val_topic}` topic.

The Publisher sends:
```
msg: {
  token: "{token}",
  action: ACTION_PUB_VAL_REG_START,
  body: {
    val_topic: "{val_topic}",
    val_len: {val_len},
    kx_pk: "{client_kx_pk}"
  }
}
```

The TP (through the Broker) replies:
```
msg: {
  token: "{token}",
  action: ACTION_PUB_VAL_REG_END,
  body: {
    kx_pk: "{tp_kx_pk}"
  }
}
```

On failure, any party will send:
```
msg: {
  token: "{token}",
  action: ACTION_PUB_VAL_REG_ABORT,
  body: {
    err_code: {err_code},
    reason: "{reason}",
    id: ""
  }
}
```

## Publisher publishing a value to a registered topic

- The topic will be `secfun/v0/val/{ID}/{subtopic}`.  `type` will be `MSG_TYPE_VALMSG`.

```
msg: {
  t: {t},
  ts: {ts},
  v: ["{label0}", "{label1}", "{label2}", "{label3}", "{label4}", ... ]
}
```

Where each `{labelX}` is the bit X of the value mapped to the labels generated
using the ratchet key rkp, and `{t}` is the round at which the rkp was.  `{ts}`
is a timestamp that indicates when the value was sent.

## Broker sending function value output to Subscriber

- The topic will be `secfun/v0/fun/{ID}/{fun_id}`.  `type` will be `MSG_TYPE_FUNMSG`.

```
msg: {
  t: {t},
  ts: {ts},
  v: "{masked_val}"
}
```

Where `{masked_val}` is the result of evaluating the function `{fun_id}` and
masking it with the mask generated by `{rkf}` at round `{t}`. `{ts}` is a
timestamp that indicates when the value was sent.

## Subscriber registers a function

Where `fun_id` is the string generated by taking the function description
`fun`, serializing it, compting its ShakeSum256 and encoding it in base64url.
This allows a way to uniquely identify functions globally.

```
msg: {
  token: "{token}",
  action: ACTION_SUB_FUN_REG_START,
  body: {
    fun_id: "{fun_id}",
    fun: { ... },
  }
}
```

From the Broker:
```
msg: {
  token: "{token}",
  action: ACTION_SUB_FUN_REG_END,
  body: {
    fun_id: "{fun_id}",
  }
}
```

If anything fails
```
msg: {
  token: "{token}",
  action: ACTION_SUB_FUN_REG_ABORT,
  body: {
    err_code: {err_code},
    reason: "{reason}",
    id: "{fun_id}"
  }
}
```

On success, the Broker now creates the topic `secfun/v0/fun/{fun_id}`.

## Subscriber subscribes to a function

```
msg: {
  token: "{token}",
  action: ACTION_SUB_FUN_REQ_START,
  body: {
    fun_id: "{fun_id}",
    kx_pk: "{client_kx_pk}"
  }
}
```

The TP (through the Broker) replies:
```
msg: {
  token: "{token}",
  action: ACTION_SUB_FUN_REQ_END,
  body: {
    fun_id: "{fun_id}",
    kx_pk: "{tp_kx_pk}"
    nonce: "{nonce}"
    rkf: "{E(rkf || t)}"
  }
}
```

Where `{E(rkf || t)}` is the ratchet key `rkf` along with its current round,
encrypted using the shared key established with `{client_kx_pk}` and
`{top_kx_pk}`, using the nonce `{nonce}`.

If anything fails
```
msg: {
  token: "{token}",
  action: ACTION_SUB_FUN_REQ_ABORT,
  body: {
    err_code: {err_code},
    reason: "{reason}",
    id: "{fun_id}"
  }
}
```
