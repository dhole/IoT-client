#! /bin/bash
#set -x

start_sum_fun() {
  FUN=$1
  N=$2
  rm functions/test.lsp

  echo "(begin" >> functions/test.lsp

  for i in `seq 1 9`;
  do
    echo "  (define v$i (val \"$i\"))" >> functions/test.lsp
    l=`perl -E "say \"v$i \" x $N"`
    echo "  (define l$i (list $l))" >> functions/test.lsp
  done

  echo "  (start-building)" >> functions/test.lsp
  for i in `seq 1 9`;
  do
    echo "  (define t$i (sum l$i))" >> functions/test.lsp
  done
  l=`perl -E "say \"t$i \" x 9"`
  echo "  ($FUN (list $l))" >> functions/test.lsp
  echo ")" >> functions/test.lsp
}

start_fn_fun() {
  FUN=$1
  N=$2
  rm functions/test.lsp

  echo "(begin" >> functions/test.lsp

  for i in `seq 1 9`;
  do
    echo "  (define v$i (val \"$i\"))" >> functions/test.lsp
    #l=`perl -E "say \"v$i \" x $N"`
    #echo "  (define l$i (list $l))" >> functions/test.lsp
  done

  echo "  (start-building)" >> functions/test.lsp
  l=`seq -s " " -f "v%.0f" 1 9`
  for i in `seq 1 $N`;
  do
    echo "  (define t$i (sum (list $l)))" >> functions/test.lsp
  done

  l=`perl -E "say \"t$i \" x $N"`

  echo "  ($FUN (list $l))" >> functions/test.lsp
  echo ")" >> functions/test.lsp
}

FUNS="mean min max var"
#FUNS="mean"

#trap 'tmux kill-session -t FunSecTest; kill_test; killall -9 publisher; exit' SIGINT SIGTERM EXIT

REMOTE_HOST="dev@192.168.0.103"
kill_test() {
  while [ "`wc -l broker_stats.csv | cut -d ' ' -f 1`" -lt "6" ];
  do
    sleep 2
  done
  kill $(jobs -pr)
  killall -9 publisher
  killall run_test.sh
  killall -9 publisher
  tmux kill-session -t FunSecTest
  #ssh "$REMOTE_HOST" "killall run_publishers.sh"
  #ssh "$REMOTE_HOST" "killall -9 publisher"
}

mkdir -p results
N="288"

for fun in $FUNS;
do
  echo $fun $N

  start_fn_fun $fun $N

  rm broker_stats.csv
  touch broker_stats.csv
  kill_test &
  ./run_test.sh -t=12 -n=9 --skipfun
  echo "# `date +'%F %T'`, n=$N" >> results/parking_${fun}_stats.csv
  tail -n +2 broker_stats.csv >> results/parking_${fun}_stats.csv
done

start_sum_fun max $N

echo max_free
rm broker_stats.csv
touch broker_stats.csv
kill_test &
./run_test.sh -t=12 -n=9 --skipfun
echo "# `date +'%F %T'`, n=$N" >> results/parking_max_free_stats.csv
tail -n +2 broker_stats.csv >> results/parking_max_free_stats.csv

start_sum_fun min $N

echo min_free
rm broker_stats.csv
touch broker_stats.csv
kill_test &
./run_test.sh -t=12 -n=9 --skipfun
echo "# `date +'%F %T'`, n=$N" >> results/parking_min_free_stats.csv
tail -n +2 broker_stats.csv >> results/parking_min_free_stats.csv
