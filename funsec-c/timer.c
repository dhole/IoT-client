#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <time.h>

#include "log.c/log.h"
#include "chan/chan.h"
#include "timer.h"
#include "mqtt_pub.h"

// NOTE: In order for these times to work, the process needs to catch SIGTERM
// signals and ignore them

void *
timer_fn(void *_args)
{
    struct timer_args *args = (struct timer_args *) _args;
    uint64_t i = 0;
    int t;
    struct timespec ts;
    ts.tv_nsec = 0;

    log_debug("[T] Starting timer thread...");

    while (true) {
        chan_recv_int(args->timer_chan, &t);
        //log_debug("[T] Chan recv %d", t);
        if (t == -1) {
            log_debug("[T] Stopping timer thread...");
            break;
        } else if (t == 0) {
            continue;
        }
        log_debug("[T] Setting timer %d for %d seconds", i, t);
        ts.tv_sec = t;
        if (nanosleep(&ts, NULL) == 0) {
            log_debug("[T] Timer %d (%u s) finished", i, t);
            chan_send_int(args->chan, REG_SIG_TIMEOUT);
        } else {
            log_debug("[T] Timer %d (%u s) stopped", i, t);
        }
        i++;
    }
    chan_close(args->timer_chan);
    chan_dispose(args->timer_chan);
    free(args);
    return NULL;
}

struct timer
timer_init(chan_t *chan)
{
    struct timer timer;
    struct timer_args *args = malloc(sizeof(struct timer_args));
    timer.chan = chan_init(0);
    args->chan = chan;
    args->timer_chan = timer.chan;
    pthread_create(&timer.th, NULL, timer_fn, (void *) args);
    // Block until timer thread is ready
    chan_send_int(args->timer_chan, 0);
    return timer;
}

void
timer_free(struct timer *timer)
{
    pthread_kill(timer->th, SIGTERM);
    chan_send_int(timer->chan, -1);
    pthread_join(timer->th, NULL);
}

void
timer_set(struct timer *timer, const int t)
{
    pthread_kill(timer->th, SIGTERM);
    chan_send_int(timer->chan, t);
}


