#ifndef SECFUN_MQTT_PUB_H
#define SECFUN_MQTT_PUB_H

#include "client.h"

#define CON_RETRY_SECONDS 30
#define ACTION_RETRY_SECONDS 30
//#define QOS 2
// Make go Broker happy
#define QOS 1

enum reg_sig {
    REG_SIG_WAKEUP = 0,
    REG_SIG_STOP,
    REG_SIG_DELIVERY,
    REG_SIG_MSG,
    // Leave this always at the end
    REG_SIG_TIMEOUT,
};

int mqtt_pub_loop(struct publisher *pub,
                  const char *host, const unsigned int port,
                  const char *listen_addr, const unsigned int listen_port,
                  const char *publisher_keys_filename);

#endif
