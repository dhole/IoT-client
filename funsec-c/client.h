#ifndef SECFUN_CLIENT_H
#define SECFUN_CLIENT_H

#include "keys.h"
#include "protocol.h"

//#define ID_LEN 44
#define ID_LEN 44
// MQTT only allows Client ID to be at most 23 characters long
#define MQTT_ID_LEN 23
#define SUBTOPIC_MAX_LEN 81

struct subtopic {
    char name[SUBTOPIC_MAX_LEN];
    unsigned int val_len;
    //enum subtopic_state state;
};

struct client
{
    char id[ID_LEN + 1];
    char mqtt_id[MQTT_ID_LEN + 1];
    unsigned char broker_pk[crypto_sign_PUBLICKEYBYTES];
    char broker_pk_b64[crypto_sign_PUBLICKEYBYTESB64];
    unsigned char tp_pk[crypto_sign_PUBLICKEYBYTES];
    char tp_pk_b64[crypto_sign_PUBLICKEYBYTESB64];
    struct sign_key sig;
};

struct publisher
{
    enum pub_state state; // Requires lock to access
    struct subtopic subtopic;
    struct client cl;
    struct ratchet_key rkp; // Requires lock to access
};

void client_set_id(struct client *cl);
int client_broker_pk_load(struct client *cl, const char *filename);
int client_tp_pk_load(struct client *cl, const char *filename);

void publisher_keys_init(struct publisher *pub);
int publisher_conf_load(struct publisher *pub, const char *filename);
int publisher_keys_store(const struct publisher *pub, const char *filename);
int publisher_keys_load(struct publisher *pub, const char *filename);

#endif
