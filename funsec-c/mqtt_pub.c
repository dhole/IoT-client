#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "MQTTClient.h"

#include "mqtt_pub.h"
#include "log.c/log.h"
#include "chan/chan.h"
//#include "chan/queue.h"
#include "client.h"
#include "message.h"
#include "protocol.h"
#include "json_utils.h"
#include "utils.h"
#include "timer.h"

#define TIMER_THS_LEN 128

static volatile bool stop;

static MQTTClient_deliveryToken reg_mqtt_token;

static struct publisher *pub;
static pthread_rwlock_t pub_lock;

static enum reg_state reg_state;

chan_t *reg_chan;
chan_t *reg_msg_chan;

static MQTTClient mqtt_cl;
static pthread_mutex_t mqtt_cl_lock;

static pthread_mutex_t log_lock;

static pthread_t pub_reg_th;
static pthread_t main_th;

static char *dev_topic;
static char *pub_topic;
const char *publisher_keys_filename;

static int err;

void
sig_handler(int signo)
{
    if (signo == SIGINT) {
        log_info("SIGINT received, terminating...");
        err = -1;
    } else if (signo == SIGUSR1) {
        log_info("SIGUSR1 received, reconnecting...");
        err = 0;
    } else if (signo == SIGTERM) { // We use this to unblock syscalls
        return;
    }
    stop = true;
}

int
chan_send_msg(chan_t *chan, json_t *j_msg)
{
    return chan_send(chan, j_msg);
}

int
chan_recv_msg(chan_t *chan, json_t **j_msg)
{
    return chan_recv(chan, (void **) j_msg);
}

void
log_lock_fn(void *udata, int lock)
{
    if (lock == 1) {
        pthread_mutex_lock(&log_lock);
    } else if (lock == 0) {
        pthread_mutex_unlock(&log_lock);
    }
}

void *
pub_reg(void *arg)
{
    struct kx_key kx;
    unsigned char tp_pk[crypto_sign_PUBLICKEYBYTES];
    int sig, res;
    bool restart = false;
    json_t *j_msg = NULL, *j_wmsg = NULL;
    json_t *j_msg_arrvd = NULL;
    json_t *j_body_arrvd = NULL;
    char *wmsg_str = NULL;
    char *reason = NULL;
    int err_code;
    enum prot_action action_arrvd;
    char msg_token[TOKEN_LEN];
    struct timer timer = timer_init(reg_chan);

    log_debug("[R] Starting pub_reg thread");
    while (true) {
        j_msg_arrvd = NULL;
        j_wmsg = NULL;
        j_msg = NULL;
        chan_recv_int(reg_chan, &sig);

        if (sig == REG_SIG_STOP) {
            log_debug("[R] Stopping pub_reg thread...");
            timer_free(&timer);
            chan_close(reg_chan);
            chan_dispose(reg_chan);
            reg_chan = NULL;

            chan_close(reg_msg_chan);
            // Free messages in the queue
            while (chan_recv_msg(reg_msg_chan, &j_msg_arrvd) == 0) {
                if (j_msg_arrvd) { json_decref(j_msg_arrvd); j_msg_arrvd = NULL; }
            }
            chan_dispose(reg_msg_chan);
            reg_msg_chan = NULL;
            return NULL;

        } else if (sig == REG_SIG_MSG) {
            chan_recv_msg(reg_msg_chan, &j_msg_arrvd);
            action_arrvd = msg_get_action(j_msg_arrvd);
            if (action_arrvd == ACTION_PUB_VAL_REG_ABORT) {
                json_unpack(j_msg_arrvd, "{s:{s:i, s:s}}",
                            "body", "err_code", &err_code, "reason", &reason);
                log_error("[R] Received msg with abort action, err: %d: %s",
                          err_code, reason);
                pthread_rwlock_wrlock(&pub_lock);
                if (pub->state == PUB_STATE_ACTIVE) {
                    pub->state = PUB_STATE_REG;
                }
                pthread_rwlock_unlock(&pub_lock);
                restart = true;
                goto pub_reg_restart;
            }
        }

        switch (reg_state) {
        case REG_STATE_START:
            log_debug("[R] pub_reg: REG_STATE_START");
            wmsg_str = NULL;
            kx_key_init(&kx);
            gen_token(msg_token);
            //pthread_rwlock_rdlock(&pub_lock);
            j_msg = msg_make_pub_val_reg_start(msg_token, pub->subtopic.name,
                                               pub->subtopic.val_len, kx.pk);
            //pthread_rwlock_unlock(&pub_lock);
            if (j_msg == NULL) {
                restart = true;
                break;
            }
            // Note: pub->cl is never written, so it should be safe to read it
            // without acquiring the lock
            //pthread_rwlock_rdlock(&pub_lock);
            j_wmsg = msg_wrap_sign(j_msg, &pub->cl, dev_topic, MSG_TYPE_MSG);
            //pthread_rwlock_unlock(&pub_lock);
            if (j_wmsg == NULL) {
                json_decref(j_msg); j_msg = NULL;
                restart = true;
                break;
            }
            json_decref(j_msg); j_msg = NULL;
            if ((wmsg_str = json_dumps(j_wmsg, JSON_SORT_KEYS | JSON_COMPACT)) == NULL) {
                log_error("[R] pub_reg: Error encoding json msg");
                restart = true;
                goto reg_state_start_clean;
            }
            pthread_mutex_lock(&mqtt_cl_lock);
            // Remember that strlen excludes the terminating null byte!
            if (MQTTClient_publish(mqtt_cl, dev_topic, strlen(wmsg_str), wmsg_str,
                               QOS, false, &reg_mqtt_token) != MQTTCLIENT_SUCCESS) {
                log_error("[R] pub_reg: Error publishing MQTT msg");
                restart = true;
                pthread_mutex_unlock(&mqtt_cl_lock);
                goto reg_state_start_clean;
            }
            pthread_mutex_unlock(&mqtt_cl_lock);

            timer_set(&timer, 10);
reg_state_start_clean:
            if (wmsg_str) { free(wmsg_str); }
            json_decref(j_msg); j_msg = NULL;
            json_decref(j_wmsg); j_wmsg = NULL;
            reg_state = REG_STATE_SENT_PK;
            break;

        case REG_STATE_SENT_PK:
            log_debug("[R] pub_reg: REG_STATE_SENT_PK");
            if (sig == REG_SIG_TIMEOUT) {
                log_error("[R] Waiting for pub_pk delivery confirmation timed out");
                restart = true;
            } else if (sig == REG_SIG_DELIVERY) {
                reg_state = REG_STATE_SENT_PK_CONF;
                timer_set(&timer, 10);
            }
            break;

        case REG_STATE_SENT_PK_CONF:
            log_debug("[R] pub_reg: REG_STATE_SENT_PK_CONF");
            if (sig == REG_SIG_TIMEOUT) {
                log_error("[R] Waiting for tp_pk timed out");
                restart = true;
                break;
            } else if (sig == REG_SIG_MSG) {
                if (!msg_match_token(j_msg_arrvd, msg_token)) {
                    log_error("[R] Token of received msg doesn't match");
                    restart = true;
                    break;
                }
                if (!(action_arrvd == ACTION_PUB_VAL_REG_END)) {
                    log_error("[R] Received message with unexpected action %d", action_arrvd);
                    restart = true;
                    break;
                }
                if (json_unpack(j_msg_arrvd, "{s:o}", "body", &j_body_arrvd) != 0) {
                    log_error("[R] Unable to extract body from msg");
                    restart = true;
                    break;
                }
                if (json_get_b64(j_body_arrvd, "kx_pk", tp_pk,
                             crypto_sign_PUBLICKEYBYTES) != 0) {
                    restart = true;
                    break;
                }
                json_decref(j_body_arrvd);
                j_body_arrvd = NULL;
                reg_state = REG_STATE_RECVD_PK;
                // Continue to following state
            } else {
                break;
            }

        case REG_STATE_RECVD_PK:
            log_debug("[R] pub_reg: REG_STATE_RECVD_PK");
            pthread_rwlock_wrlock(&pub_lock);
            res = ratchet_key_init(&pub->rkp, &kx, tp_pk);
            pthread_rwlock_unlock(&pub_lock);
            if (res != 0) {
                log_error("[R] Key exchange error: "
                          "received invalid public key for key echange");
                restart = true;
                break;
            } else {
                pthread_rwlock_rdlock(&pub_lock);
                res = publisher_keys_store(pub, publisher_keys_filename);
                pthread_rwlock_unlock(&pub_lock);
                if (res != 0) {
                    log_fatal("Unable to store publisher keys");
                    err = -2;
                    stop = true;
                    chan_send_int(reg_chan, REG_SIG_STOP);
                    break;
                }
                reg_state = REG_STATE_FINALIZE;
                // Continue to following state
            }

        case REG_STATE_FINALIZE:
            log_debug("[R] pub_reg: REG_STATE_FINALIZE");
            pthread_rwlock_wrlock(&pub_lock);
            pub->state = PUB_STATE_ACTIVE;
            pthread_rwlock_unlock(&pub_lock);
            reg_state = REG_STATE_FINISHED;
            break;

        case REG_STATE_FINISHED:
            log_debug("[R] pub_reg: REG_STATE_FINISHED");
            break;
        }

pub_reg_restart:
        if (restart) {
            // TODO: Send abort
            log_info("[R] Going back to REG_STATE_START, retrying in 30 seconds...");
            timer_set(&timer, 30);
            reg_state = REG_STATE_START;
            restart = false;
        }
        if (j_msg_arrvd) { json_decref(j_msg_arrvd); }
    }
}

void
delivered(void *context, MQTTClient_deliveryToken dt)
{
    log_debug("[D] Message with token value %d delivery confirmed", dt);
    if (reg_mqtt_token == dt) {
        if (reg_chan != NULL) {
            chan_send_int(reg_chan, REG_SIG_DELIVERY);
        }
    }
}

int
msgarrvd(void *context, char *topic, int topic_len, MQTTClient_message *msg)
{
    char *payload = NULL;

    log_debug("[M] Message arrived with topic %s", topic);

    //log_debug("Recv PayLen = %d", msg->payloadlen);
    payload = malloc(msg->payloadlen+1);
    strncpy(payload, msg->payload, msg->payloadlen);
    // Null terminate the array to handle it safely as a C string
    payload[msg->payloadlen] = '\0';

    log_debug("[M] Payload: %s", payload);

    json_t *j_wmsg = NULL;
    json_t *j_msg = NULL;
    json_error_t error;
    char *from_str = NULL;
    unsigned char *pk = NULL;
    enum role from = UNKNOWN;
    int action;

    if (topic_len != 0) {
        log_error("[M] Topic contains one more NULL characters");
        goto msgarrvd_clean;
    }
    // Handle a message delivered to the device specific topic
    if (strcmp(topic, dev_topic) == 0) {
        // Parse json object
        if ((j_wmsg = json_loads(payload, 0, &error)) == NULL) {
            log_error("[M] Received invalid json MQTT msg: %s, %s",
                      error.text, error.source);
            goto msgarrvd_clean;
        }
        // Validate json object according to spec
        if (json_unpack_ex(j_wmsg, &error, JSON_VALIDATE_ONLY,
                           "{s:s, s:{s:s, s:i, s:o}, s:s}",
                           "from", "msg", "token", "action", "body", "sig") != 0) {
            log_error("[M] Unable to validate json MQTT msg: %s: %s",
                      error.source, error.text);
            goto msgarrvd_clean;
        } else {
            log_debug("[M] Received json MQTT msg validated");
        }
        // Figure out the source of the msg
        json_unpack(j_wmsg, "{s:s}", "from", &from_str);
        if (strcmp(from_str, pub->cl.broker_pk_b64) == 0) {
            log_debug("[M] Received MQTT msg from Broker");
            from = BROKER;
            pk = pub->cl.broker_pk;
        } else if (strcmp(from_str, pub->cl.tp_pk_b64) == 0) {
            log_debug("[M] Received MQTT msg from TP");
            from = THIRD_PARTY;
            pk = pub->cl.tp_pk;
        } else if (strcmp(from_str, pub->cl.id) == 0) {
            // Ignore msg
            log_debug("[M] Received echo MQTT msg from ourselves");
            from = PUBLISHER;
            goto msgarrvd_clean;
        } else {
            // Ignore msg
            log_error("[M] Received MQTT msg from unknown source");
            from = UNKNOWN;
            goto msgarrvd_clean;
        }
        // Verify msg signature
        if (wmsg_verify(j_wmsg, topic, pk) != 0) {
            log_error("[M] MQTT msg signature verification failed");
            goto msgarrvd_clean;
        } else {
            log_debug("[M] MQTT msg signature verification succeeded");
        }
        // Verify that the action is allowed by the sender
        if (json_unpack(j_wmsg, "{s:{s:i}}", "msg", "action", &action) != 0) {
                log_error("[M] Unable to extract action from msg");
                goto msgarrvd_clean;
        }
        if (allowed_action_from(action, from)) {
            if (json_unpack(j_wmsg, "{s:O}", "msg", &j_msg) != 0) {
                log_error("[M] Unable to extract message from wmsg");
                goto msgarrvd_clean;
            }
        } else {
            log_error("[M] Role %d is not allowed do action %d", from, action);
            goto msgarrvd_clean;
        }
        // Verify that the message follows the spec according to the action
        if (msg_validate_action(j_msg, action)) {
            // send msg to pub_reg
            chan_send_int(reg_chan, REG_SIG_MSG);
            action = msg_get_action(j_msg);
            chan_send_msg(reg_msg_chan, j_msg);
            j_msg = NULL;
        }
    }

msgarrvd_clean:
    if (j_msg) { json_decref(j_msg); }
    if (j_wmsg) { json_decref(j_wmsg); }
    MQTTClient_freeMessage(&msg);
    MQTTClient_free(topic);
    if (payload) { free(payload); }
    return 1;
}

void
connlost(void *context, char *cause)
{
    log_error("[C] Connection to the Broker lost: %s", cause);
    pthread_kill(main_th, SIGUSR1);
}

// Start TCP socket to listen for values
int
listen_values(const char *listen_addr, const unsigned int listen_port,
              const char *publisher_keys_filename)
{
    int r = 0;
    int listenfd = 0, connfd = 0, read_size;
    struct sockaddr_in serv_addr;

    unsigned int val_len = pub->subtopic.val_len;
    //unsigned int val_len_bytes = ceil(((float) val_len) / 8);
    //unsigned char *val_buf = NULL;
    unsigned char *val_labels = NULL;
    bool *val_bin = NULL;

    bool pub_state_active;
    unsigned int rkp_t;
    int64_t ts;
    char *wmsg_str = NULL;
    json_t *j_msg = NULL, *j_wmsg = NULL;

    if ((listenfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        log_fatal("[V] Could not create value socket");
        return -3;
    }

    // Enable reusing address for socket so that it's available after the
    // client crashes.
    const int enable = 1;
    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR,
                   &enable, sizeof(int)) != 0) {
        log_fatal("[V] Failed to set value socket to reuse address");
        r = -3; goto listen_values_clean;
    }

    // We tust the client sending us values, so it's safe to ommit this
    //
    // Set timeout in the socket when receiving data from a connection.
    //
    // FIXME: The accept() timeouts after 30 seconds.  I only want the
    // recv to timeout after 30 seconds, how can i fix this?
    // i'm disabling the timeout for now.
    //
    //struct timeval tv;
    //tv.tv_sec = 30;
    //tv.tv_usec = 0;
    //if (setsockopt(listenfd, sol_socket, so_rcvtimeo,
    //               (const char *) &tv, sizeof(struct timeval)) != 0) {
    //    log_fatal("failed to set value socket timeout");
    //    err = -3; goto mqtt_pub_loop_clean;
    //}

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(listen_addr);
    serv_addr.sin_port = htons(listen_port);

    if (bind(listenfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        log_fatal("[V] Binding value socket failed");
        r = -3; goto listen_values_clean;
    }

    if (listen(listenfd, 10) != 0) {
        log_fatal("[V] Could not listen to value socket");
        r = -3; goto listen_values_clean;
    }
    log_info("[V] Listening for values at tcp://%s:%u", listen_addr, listen_port);

    log_info("[V] MQTT will publish to topic: %s", pub_topic);
    // Accept incomming connections that will send values to publish to the Broker
    char val_str[64];
    double val = 0;
    int32_t val_int;
    val_bin = malloc(val_len * sizeof(bool));
    struct timeval tv;
    tv.tv_sec = 6;
    tv.tv_usec = 0;
    while (true) {
        if ((connfd = accept(listenfd, (struct sockaddr *) NULL, NULL)) < 0) {
            if (stop) {
                break;
            }
            log_error("[V] Accepting value connection failed");
            continue;
        }
        log_debug("[V] Accepted value connection");
        if (setsockopt(connfd, SOL_SOCKET, SO_RCVTIMEO,
                       (const char *) &tv, sizeof(struct timeval)) != 0) {
            log_error("failed to set value socket timeout");
        }
        if ((read_size = recv(connfd, val_str, 64, 0)) <= 0) {
            if (stop) {
                close(connfd);
                break;
            }
            log_error("[V] Receiving value failed");
            close(connfd);
            continue;
        }
        val_str[64-1] = '\0';
        errno = 0;
        val = strtod(val_str, NULL);
        if (errno != 0) {
            log_error("[V] Received malformed value");
            continue;
        }
        log_debug("[V] Received value: %.04f", val);
        val_int = (int32_t) (val * FIXED_SCALE);

        pthread_rwlock_rdlock(&pub_lock);
        pub_state_active = (pub->state == PUB_STATE_ACTIVE) ? true : false;
        pthread_rwlock_unlock(&pub_lock);
        if (!pub_state_active) {
            log_error("[V] Publisher is not in PUB_STATE_ACTIVE state, skipping value");
            goto listen_values_loop_clean;
        } else {
            ts = time(NULL);
            buf2bin((unsigned char *) &val_int, val_bin, 32);
            val_labels = malloc(GRBL_BLCK_SIZE * val_len);

            pthread_rwlock_rdlock(&pub_lock);
            label_map_value(&pub->rkp, val_bin, val_len, val_labels);
            rkp_t = pub->rkp.t;
            ratchet_key_advance(&pub->rkp);
            publisher_keys_store(pub, publisher_keys_filename);
            pthread_rwlock_unlock(&pub_lock);

            j_msg = msg_make_val(val_labels, val_len, rkp_t, ts);
            if (j_msg == NULL) {
                goto listen_values_loop_clean;
            }

            j_wmsg = msg_wrap_sign(j_msg, &pub->cl, pub_topic, MSG_TYPE_VALMSG);
            if (j_wmsg == NULL) {
                json_decref(j_msg); j_msg = NULL;
                goto listen_values_loop_clean;
            }
            if ((wmsg_str = json_dumps(j_wmsg, JSON_SORT_KEYS | JSON_COMPACT)) == NULL) {
                log_error("[V] listen_values_loop: Error encoding json msg");
                goto listen_values_loop_clean;
            }
            log_debug("[V] Publishing received value label-mapped...");
            pthread_mutex_lock(&mqtt_cl_lock);
            // Remember that strlen excludes the terminating null byte!
            if (MQTTClient_publish(mqtt_cl, pub_topic, strlen(wmsg_str), wmsg_str,
                               QOS, false, NULL) != MQTTCLIENT_SUCCESS) {
                log_error("[V] listen_values_loop: Error publishing MQTT msg");
                pthread_mutex_unlock(&mqtt_cl_lock);
                goto listen_values_loop_clean;
            }
            pthread_mutex_unlock(&mqtt_cl_lock);
        }

listen_values_loop_clean:
        if (wmsg_str) { free(wmsg_str); wmsg_str = NULL; }
        if (val_labels) { free(val_labels); val_labels = NULL; }
        if (j_msg) { json_decref(j_msg); j_msg = NULL; }
        if (j_wmsg) { json_decref(j_wmsg); j_wmsg = NULL; }
        close(connfd);
    }

listen_values_clean:
    if (val_bin) { free(val_bin); val_bin = NULL; }
    //if (val_buf) { free(val_buf); val_bin = NULL; }
    close(listenfd);
    return r;
}

int
mqtt_pub_loop(struct publisher *_pub,
              const char *host, const unsigned int port,
              const char *listen_addr, const unsigned int listen_port,
              const char *_publisher_keys_filename)
{
    pub = _pub;
    publisher_keys_filename = _publisher_keys_filename;
    err = 0;
    stop = false;
    reg_chan = NULL;
    reg_msg_chan = NULL;
    dev_topic = NULL;
    pub_topic = NULL;

    main_th = pthread_self();

    //
    // Enable thread-safe log
    //
    log_set_lock(log_lock_fn);

    //
    // State setup
    //

    if (pub->state == PUB_STATE_REG) {
        reg_state = REG_STATE_START;
    } else if (pub->state == PUB_STATE_ACTIVE) {
        reg_state = REG_STATE_FINISHED;
    }

    //
    // MQTT configuration
    //
    char *address = malloc(strlen("tcp://:") + strlen(host) +
                           ceil(log10(port)) + 1);
    sprintf(address, "tcp://%s:%d", host, port);
    dev_topic = malloc(strlen(PUB_DEV_TOPIC) + strlen(pub->cl.id) + 1);
    sprintf(dev_topic, "%s%s", PUB_DEV_TOPIC, pub->cl.id);
    pub_topic = malloc(strlen(PUB_VAL_TOPIC) + strlen(pub->cl.id) + 1 +
                       strlen(pub->subtopic.name) + 1);
    sprintf(pub_topic, "%s%s/%s", PUB_VAL_TOPIC, pub->cl.id, pub->subtopic.name);

    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    conn_opts.keepAliveInterval = 60;
    conn_opts.cleansession = true;
    conn_opts.connectTimeout = 30;
    conn_opts.retryInterval = 20;
    // Make the client compatible with the go Broker
    conn_opts.MQTTVersion = MQTTVERSION_3_1;

    MQTTClient_create(&mqtt_cl, address, pub->cl.mqtt_id,
                      MQTTCLIENT_PERSISTENCE_NONE, NULL);

    MQTTClient_setCallbacks(mqtt_cl, NULL, connlost, msgarrvd, delivered);

    //
    // Init locks
    //
    if (pthread_rwlock_init(&pub_lock, NULL) != 0) {
        log_fatal("Error initializing pub_lock");
        err = -3; goto mqtt_pub_loop_clean_mqtt;
    }
    //if (pthread_rwlock_init(&reg_state_lock, NULL) != 0) {
    //    log_fatal("Error initializing reg_state_lock");
    //    err = -3; goto mqtt_pub_loop_clean_mqtt;
    //}
    if (pthread_mutex_init(&mqtt_cl_lock, NULL) != 0) {
        log_fatal("Error initializing mqtt_cl_lock");
        pthread_rwlock_destroy(&pub_lock);
        err = -3; goto mqtt_pub_loop_clean_mqtt;
    }
    if (pthread_mutex_init(&log_lock, NULL) != 0) {
        pthread_rwlock_destroy(&pub_lock);
        pthread_mutex_destroy(&mqtt_cl_lock);
        log_fatal("Error initializing log_lock");
        err = -3; goto mqtt_pub_loop_clean_locks;
    }

    //
    // Register a handler to catch Ctrl-C in order to terminate nicely.
    //
    struct sigaction sa;
    sa.sa_handler = sig_handler;
    sa.sa_flags = 0;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGINT, &sa, NULL) != 0) {
        log_error("Can't catch SIGINT");
        err = -1; goto mqtt_pub_loop_clean_locks;
    }
    if (sigaction(SIGUSR1, &sa, NULL) != 0) {
        log_error("Can't catch SIGUSR1");
        err = -1; goto mqtt_pub_loop_clean_locks;
    }
    if (sigaction(SIGTERM, &sa, NULL) != 0) {
        log_error("Can't catch SIGTERM");
        err = -1; goto mqtt_pub_loop_clean_locks;
    }

    // From now on, acquire pub_lock to read/write the pub struct (only state
    // and rpk fields).

    //
    // MQTT connect to Broker
    //
    int r;
    while (!stop &&
            ((r = MQTTClient_connect(mqtt_cl, &conn_opts)) != MQTTCLIENT_SUCCESS)) {
        log_error("Failed to connect to Broker %s, return code %d", address, r);
        log_error("Retrying connection in %d seconds...", CON_RETRY_SECONDS);
        sleep(CON_RETRY_SECONDS);
    }
    if (stop) {
        goto mqtt_pub_loop_clean_locks;
    }
    log_info("Succesfully connected to the Broker %s", address);

    //
    // MQTT subscribe to publisher device specific topic
    //
    log_info("MQTT Subscribing to device specific topic: %s", dev_topic);
    MQTTClient_subscribe(mqtt_cl, dev_topic, QOS);

    //
    // Start pub_reg thread (Key Exchange to register a value subtopic)
    //
    reg_chan = chan_init(128);
    reg_msg_chan = chan_init(128);
    if (pthread_create(&pub_reg_th, NULL, pub_reg, NULL) != 0) {
        log_fatal("Could not create pub_reg thread");
        err = -3; goto mqtt_pub_loop_clean_locks;
    }
    chan_send_int(reg_chan, REG_SIG_WAKEUP);

    // We block here, listening for values to publish in a loop found at listen_values()
    if ((r = listen_values(listen_addr, listen_port,
                           publisher_keys_filename)) != 0) {
        err = r;
    }

    stop = true;
    chan_send_int(reg_chan, REG_SIG_STOP);
    pthread_join(pub_reg_th, NULL);
mqtt_pub_loop_clean_locks:
    pthread_rwlock_destroy(&pub_lock);
    pthread_mutex_destroy(&mqtt_cl_lock);
    pthread_mutex_destroy(&log_lock);
mqtt_pub_loop_clean_mqtt:

    if (MQTTClient_isConnected(mqtt_cl)) {
        if (MQTTClient_disconnect(mqtt_cl, 10000) == MQTTCLIENT_SUCCESS) {
            log_info("Disconnected from MQTT Broker");
        } else {
            log_error("Could not disconnect from MQTT Broker");
        }
    }
    MQTTClient_destroy(&mqtt_cl);

    if (dev_topic) { free(dev_topic); }
    if (pub_topic) { free(pub_topic); }
    free(address);

    if (err == 0) {
        log_info("Trying again in 30 seconds...");
        sleep(30);
    }
    return err;
}
