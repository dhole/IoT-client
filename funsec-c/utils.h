#ifndef SECFUN_UTILS_H
#define SECFUN_UTILS_H

#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>

void u642bin(uint64_t v, bool *str, size_t n);
uint64_t bin2u64(bool *str, size_t n);
void buf2bin(unsigned char *buf, bool *str, size_t n);
bool buf2bin_test();

#endif
