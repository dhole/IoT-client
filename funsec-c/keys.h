#ifndef SECFUN_KEYS_H
#define SECFUN_KEYS_H

#include <sodium.h>
#include <stdbool.h>
#include <jansson.h>

#define RK_ADV_CTX "RAdvance"
#define RK_GEN_SEED_CTX "RGenSeed"
#define SEED_ID_DELTA 0
#define SEED_ID_LABELS 1

#define GRBL_BLCK_SIZE (128 / 8)

// There are actually more bytes here than necessary, to accomodate the cases
// where base64 adds padding
#define crypto_sign_PUBLICKEYBYTESB64 (crypto_sign_PUBLICKEYBYTES * 4 / 3 + 4 + 1)
#define crypto_sign_SECRETKEYBYTESB64 (crypto_sign_SECRETKEYBYTES * 4 / 3 + 4 + 1)

struct sign_key
{
    unsigned char pk[crypto_sign_PUBLICKEYBYTES];
    unsigned char sk[crypto_sign_SECRETKEYBYTES];
};

struct kx_key
{
    unsigned char pk[crypto_kx_PUBLICKEYBYTES];
    unsigned char sk[crypto_kx_SECRETKEYBYTES];
};

struct ratchet_key
{
    unsigned char key[crypto_kdf_KEYBYTES];
    long long t;
};

//struct client_keys
//{
//    struct sign_key sig;
//    struct ratchet_key rk;
//};

typedef unsigned char server_public_key[crypto_sign_PUBLICKEYBYTES];

int ratchet_key_init(struct ratchet_key *rk, struct kx_key *kk,
                     unsigned char server_pk[crypto_kx_PUBLICKEYBYTES]);
void ratchet_key_advance(struct ratchet_key *rk);
void ratchet_key_gen_seed(const struct ratchet_key *rk,
                          unsigned char seed[randombytes_SEEDBYTES], uint64_t id);
void ratchet_key_cprng(const struct ratchet_key *rk, void * const buf,
                       const size_t size, uint64_t id);

void sign_key_init(struct sign_key *sig);
void kx_key_init(struct kx_key *kx);

void server_public_key_load(server_public_key pk, const char *filename);

void label_map_value(const struct ratchet_key *rk, bool *val, size_t len,
                     unsigned char *map_val);

#endif
