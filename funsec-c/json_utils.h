#ifndef SECFUN_JSON_UTILS_H
#define SECFUN_JSON_UTILS_H

#include <string.h>
#include <jansson.h>

int json_add_str(json_t *j_root, const char *key, const char *str);
int json_add_int(json_t *j_root, const char *key, const int v);
int json_add_b64(json_t *j_root, const char *key, const unsigned char *buf,
                const size_t len);
int json_get_b64(json_t *j_root, const char *key, unsigned char *buf,
                const size_t len);
json_t *map_value_json(unsigned char *val_map, size_t len);

#endif
