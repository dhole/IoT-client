/*
 * Original: Base64 encoding/decoding (RFC1341)
 * Modification: base64url encoding/decoding (RFC 4648)
 * Copyright (c) 2005, Jouni Malinen <j@w1.fi>
 * Modification by Eduard Sanou <sanou@usc.edu>
 *
 * This software may be distributed under the terms of the BSD license.
 */

#include <unistd.h>

#ifndef BASE64_H
#define BASE64_H

unsigned char * base64_encode(const unsigned char *src, size_t len,
			      size_t *out_len);
unsigned char * base64_decode(const unsigned char *src, size_t len,
			      size_t *out_len);

#endif /* BASE64_H */
