#include "base64.h"
#include "message.h"
#include "json_utils.h"
#include "log.c/log.h"

int
wmsg_add_from(json_t *j_root, const char dev_id[ID_LEN])
{
    return json_add_str(j_root, "from", dev_id);
}

int
wmsg_add_type(json_t *j_root, const enum msg_type type)
{
    return json_add_int(j_root, "type", type);
}

int
wmsg_add_topic(json_t *j_root, const char *topic)
{
    return json_add_str(j_root, "topic", topic);
}

int
msg_add_token(json_t *j_root, const char *token)
{
    return json_add_str(j_root, "token", token);
}

bool
msg_match_token(json_t *j_msg, const char *token)
{
    char *msg_token = NULL;
    if (json_unpack(j_msg, "{s:s}", "token", &msg_token) != 0) {
        log_error("msg_match_token: msg doesn't contain any token");
        return false;
    }
    return (strcmp(token, msg_token) == 0) ? true : false;
}

enum prot_action
msg_get_action(json_t *j_msg)
{
    int action;
    if (json_unpack(j_msg, "{s:i}", "action", &action) != 0) {
        log_error("msg_get_action: msg doesn't contain any action");
        return ACTION_UNKNOWN;
    }
    return action;
}

// Generates a random token of TOKEN_LEN-1 ASCII characters (plus '\0')
// with 9 bytes of entropy.
void
gen_token(char token[TOKEN_LEN])
{
    unsigned char t_buf[9];
    char *t;
    randombytes_buf(t_buf, 9);
    t = (char *) base64_encode(t_buf, 9, NULL);
    strncpy(token, t, TOKEN_LEN);
    free(t);
}

// Verify a wrapped message according to spec v0
int
wmsg_verify(json_t *j_root, const char *topic,
       const unsigned char pk[crypto_sign_PUBLICKEYBYTES])
{
    int err = 0;
    unsigned char sig[crypto_sign_BYTES];
    json_t *j_sig_val;

    // Add the topic key into the json object
    if (wmsg_add_topic(j_root, topic) != 0) {
        return -1;
    }
    // Extract the signature
    if (json_get_b64(j_root, "sig", sig, crypto_sign_BYTES) != 0) {
        return -1;
    }
    if ((j_sig_val = json_object_get(j_root, "sig")) == NULL) {
        log_error("wmsg_verify: Error getting sig key");
        return -1;
    }
    json_incref(j_sig_val);
    // Remove the signature
    if (json_object_del(j_root, "sig") != 0) {
        log_error("wmsg_verify: Error removing sig key");
        return -1;
    }
    // Encode the json object into sorted compact form
    char *json_str;
    if ((json_str = json_dumps(j_root, JSON_SORT_KEYS | JSON_COMPACT)) == NULL) {
        log_error("wmsg_verify: Error encoding json msg");
        return -1;
    }
    if (crypto_sign_verify_detached(sig, (unsigned char *) json_str,
                                    strlen(json_str), pk) != 0) {
        err = -2; goto wmsg_verify_clean;
    }
    // Add the signature back
    if (json_object_set_new(j_root, "sig", j_sig_val) != 0) {
        log_error("wmsg_verify: Error adding sig key");
        err = -1; goto wmsg_verify_clean;
    }

    // Remove the topic key from json object
    if (json_object_del(j_root, "topic") != 0) {
        log_error("wmsg_verify: Error deleting topic key");
        err = -1; goto wmsg_verify_clean;
    }

wmsg_verify_clean:
    free(json_str);
    return err;
}

// Sign a wrapped message according to spec v0
int
wmsg_sign(json_t *j_root, const char *topic,
     const unsigned char sk[crypto_sign_SECRETKEYBYTES])
{
    int err = 0;
    // Add topic key into the json object
    wmsg_add_topic(j_root, topic);

    // Encode the json object into sorted compact form
    char *json_str;
    if ((json_str = json_dumps(j_root, JSON_SORT_KEYS | JSON_COMPACT)) == NULL) {
        log_error("wmsg_sign: Error encoding json msg");
        return -1;
    }
    // DEBUG
    printf("compact:\n%s\n", json_str);

    // Sign the serialized json object
    unsigned char sig[crypto_sign_BYTES];
    crypto_sign_detached(sig, NULL, (unsigned char *) json_str,
                         strlen(json_str), sk);

    // Add sig key into the json object
    if (json_add_b64(j_root, "sig", sig, crypto_sign_BYTES) != 0) {
        err = -1; goto wmsg_sign_clean;
    }

    // Remove the topic key from json object
    if (json_object_del(j_root, "topic") != 0) {
        log_error("wmsg_sign: Error deleting topic key");
        err = -1; goto wmsg_sign_clean;
    }

    // DEBUG
    //char *_json_str = json_dumps(j_root, JSON_SORT_KEYS | JSON_INDENT(2));
    //printf("signed:\n%s\n", _json_str);
    //free(_json_str);

wmsg_sign_clean:
    free(json_str);
    return err;
}

// Wrap a message according to spec v0
json_t *
msg_wrap(json_t *j_msg, const struct client *cl, const enum msg_type type)
{
    json_t *j_root;
    if ((j_root = json_object()) == NULL) {
	log_error("msg_wrap: Error creating json object");
	return NULL;
    }
    if (json_object_set(j_root, "msg", j_msg) != 0) {
	log_error("msg_wrap: Error adding msg key to json object");
        json_decref(j_root);
	return NULL;
    }
    if (wmsg_add_from(j_root, cl->id) != 0) {
        json_decref(j_root);
	return NULL;
    }
    if (wmsg_add_type(j_root, type) != 0) {
        json_decref(j_root);
	return NULL;
    }
    return j_root;
}

// Wrap a message and sign it according to spec v0
json_t *
msg_wrap_sign(json_t *j_msg, const struct client *cl, const char *topic,
              const enum msg_type type)
{
    json_t *j_root;
    if ((j_root = msg_wrap(j_msg, cl, type)) == NULL) {
	return NULL;
    }

    if (wmsg_sign(j_root, topic, cl->sig.sk) != 0) {
        json_decref(j_root);
	return NULL;
    }
    return j_root;
}

json_t *
msg_make_pub_val_reg_start(const char *token, const char *val_topic,
                           const unsigned int val_len,
                           unsigned char kx_pk[crypto_kx_PUBLICKEYBYTES])
{
    json_t *j_root;
    char *kx_pk_b64 = (char *) base64_encode(kx_pk, crypto_kx_PUBLICKEYBYTES, NULL);
    if ((j_root = json_pack("{s:s, s:i, s:{s:s, s:i, s:s}}",
                            "token", token,
                            "action", ACTION_PUB_VAL_REG_START,
                            "body",
                            "val_topic", val_topic,
                            "val_len", val_len,
                            "kx_pk", kx_pk_b64
                            )) == NULL) {
        log_error("msg_make_pub_val_reg_start: Error generating json msg");
        free(kx_pk_b64);
        return NULL;
    }
    free(kx_pk_b64);
    return j_root;
}

json_t *
msg_make_val(unsigned char *val_map, size_t len, unsigned int t, int64_t ts)
{
    json_t *j_root = NULL, *j_array = NULL;
    if ((j_array = map_value_json(val_map, len)) == NULL) {
        log_error("msg_make_val: Error generating json array");
        return NULL;
    }
    if ((j_root = json_pack("{s:i, s:i, s:o}",
                            "t", t,
                            "ts", ts,
                            "v", j_array)) == NULL) {
        log_error("msg_make_val: Error generating json msg");
        json_decref(j_array);
        return NULL;
    }
    //json_decref(j_array);
    return j_root;
}
