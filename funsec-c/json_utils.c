#include "base64.h"
#include "keys.h"
#include "json_utils.h"
#include "log.c/log.h"

int
json_add_str(json_t *j_root, const char *key, const char *str)
{
    json_t *j_str;
    if ((j_str = json_string(str)) == NULL) {
        log_error("json_add_str: Error constructing %s string", key);
        return -1;
    }
    if (json_object_set_new(j_root, key, j_str) != 0) {
        log_error("json_add_str: Error adding %s key", key);
        return -1;
    }
    return 0;
}

int
json_add_int(json_t *j_root, const char *key, const int v)
{
    json_t *j_int;
    if ((j_int = json_integer(v)) == NULL) {
        log_error("json_add_int: Error constructing %s integer", key);
        return -1;
    }
    if (json_object_set_new(j_root, key, j_int) != 0) {
        log_error("json_add_int: Error adding %s key", key);
        return -1;
    }
    return 0;
}

int
json_add_b64(json_t *j_root, const char *key, const unsigned char *buf,
            const size_t len)
{
    int err = 0;
    char *buf_b64;
    if ((buf_b64 = (char *) base64_encode(buf, len, NULL)) == NULL) {
        log_error("json_add_b64: Error encoding to base64");
        return -1;
    }
    if (json_add_str(j_root, key, buf_b64) != 0) {
        err = -1; goto json_add_b64_clean;
    }
json_add_b64_clean:
    free(buf_b64);
    return err;
}

int
json_get_b64(json_t *j_root, const char *key, unsigned char *buf,
            const size_t len)
{
    json_t *j_buf_b64;
    const char *buf_b64;
    unsigned char *_buf;
    size_t _len;
    if ((j_buf_b64 = json_object_get(j_root, key)) == NULL) {
        log_error("json_get_b64: Error getting %s key ", key);
        return -1;
    }
    if ((buf_b64 = json_string_value(j_buf_b64)) == NULL) {
        log_error("json_get_b64: Error getting string from %s key ", key);
        return -1;
    }
    if ((_buf = base64_decode((unsigned char *) buf_b64, strlen(buf_b64),
                              &_len)) == NULL) {
        log_error("json_get_b64: Error decoding string from %s key ", key);
        return -1;
    }
    if (len != _len) {
        free(_buf);
        log_error("json_get_b64: Decoded buffer length doesn't match");
        return -1;
    }

    memcpy(buf, (char *) _buf, len);
    free(_buf);
    return 0;
}

// Construct a json array of the mapped value encoded in base64
json_t *
map_value_json(unsigned char *val_map, size_t len) {
    json_t *j_array;
    if ((j_array = json_array()) == NULL) {
        log_error("map_value_json: Error creating json array");
        return NULL;
    }

    size_t i;
    char *label;
    json_t *j_label;
    for (i = 0; i < len; i++) {
        label = (char *) base64_encode(&val_map[i * GRBL_BLCK_SIZE],
                                       GRBL_BLCK_SIZE, NULL);
        j_label = json_string(label);
        if (json_array_append_new(j_array, j_label) != 0) {
            log_error("map_value_json: Error appending json array");
            json_decref(j_array);
            json_decref(j_label);
            free(label);
            return NULL;
        }
        free(label);
    }

    /*
    // DEBUG
    char *val_map_json = json_dumps(j_array, JSON_SORT_KEYS | JSON_INDENT(2));
    printf("%s\n", val_map_json);
    free(val_map_json);
    */

    return j_array;
}
