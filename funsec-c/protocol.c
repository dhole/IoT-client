#include "protocol.h"
#include "log.c/log.h"

void
init_actions_src()
{
    actions_src[ACTION_PUB_VAL_REG_START] = PUBLISHER;
    actions_src[ACTION_PUB_VAL_REG_END] = THIRD_PARTY;
    actions_src[ACTION_PUB_VAL_REG_ABORT] = PUBLISHER | BROKER | THIRD_PARTY;

    actions_src[ACTION_SUB_FUN_REG_START] = SUBSCRIBER;
    actions_src[ACTION_SUB_FUN_REG_END] = BROKER;
    actions_src[ACTION_SUB_FUN_REG_ABORT] = SUBSCRIBER | BROKER | THIRD_PARTY;

    actions_src[ACTION_SUB_FUN_REQ_START] = SUBSCRIBER;
    actions_src[ACTION_SUB_FUN_REQ_END] = THIRD_PARTY;
    actions_src[ACTION_SUB_FUN_REQ_ABORT] = SUBSCRIBER | BROKER | THIRD_PARTY;
}

bool
allowed_action_from(enum prot_action action, enum role from)
{
    if (action >= ACTION_UNKNOWN) {
	return false;
    }
    return actions_src[action] & from;
}

bool
msg_validate_action(json_t *j_msg, enum prot_action action)
{
    bool res = true;
    json_error_t error;
    switch (action) {
    case ACTION_PUB_VAL_REG_START:
	if (json_unpack_ex(j_msg, &error, JSON_VALIDATE_ONLY,
			   "{s:s, s:i, s:{s:s, s:i, s:s}}",
			   "token", "action", "body",
			   "val_topic", "val_len", "kx_pk") != 0) {
	    res = false;
	}
	break;
    case ACTION_PUB_VAL_REG_END:
	if (json_unpack_ex(j_msg, &error, JSON_VALIDATE_ONLY,
			   "{s:s, s:i, s:{s:s}}",
			   "token", "action", "body",
			   "kx_pk") != 0) {
	    res = false;
	}
	break;
    case ACTION_PUB_VAL_REG_ABORT:
	if (json_unpack_ex(j_msg, &error, JSON_VALIDATE_ONLY,
			   "{s:s, s:i, s:{s:i, s:s, s:s}}",
			   "token", "action", "body",
			   "err_code", "reason", "id") != 0) {
	    res = false;
	}
	break;
    default:
	res = false;
    }
    if (res == false) {
	log_error("Invalid json MQTT msg according to action: %s: %s",
		  error.source, error.text);
    }
    return res;
}
