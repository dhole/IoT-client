#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>
#include <errno.h>
#include <unistd.h>
#include <jansson.h>

#include "protocol.h"
#include "utils.h"
#include "client.h"
#include "keys.h"
#include "mqtt_pub.h"
#include "log.c/log.h"

#include "json_utils.h"
#include "base64.h"
#include "message.h"

const char *argp_program_version = PROT_NAME " publisher " VERSION;
const char *argp_program_bug_address = EMAIL;

static char doc[] =
    PROT_NAME " publisher -- Client program for MQTT publishing in " PROT_NAME " " PROT_VER;

static char args_doc[] = "";

static struct argp_option options[] = {
    {"verbose",  'v', 0,      0,  "Produce verbose output" },
    {"conf",     'c', "FILE", 0,  "Configuration file"},
    {"keys",     'k', "FILE", 0,  "Keys file"},
    {"broker",   'b', "FILE", 0,  "Broker public key file"},
    {"third_party", 't', "FILE", 0,  "Third Party public key file"},
    {"host",     'h', "HOST", 0,  "MQTT Broker hostname" },
    {"port",     'p', "PORT", 0,  "MQTT Broker server port" },
    {"listen_addr", 'a', "ADDRESS", 0, "IP Address to listen for values" },
    {"listen_port", 'o', "PORT", 0,  "TCP port to listen for values" },
    { 0 }
};

struct arguments
{
    bool verbose;
    char *conf_file;
    char *keys_file;
    char *broker_key_file;
    char *tp_key_file;
    char *host;
    unsigned int port;
    char *listen_addr;
    unsigned int listen_port;
};

static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
    struct arguments *args = state->input;
    char *p;

    switch (key) {
    case 'v':
        args->verbose = true;
        break;
    case 'c':
        args->conf_file = arg;
        break;
    case 'k':
        args->keys_file = arg;
        break;
    case 'h':
        args->host = arg;
        break;
    case 'b':
        args->broker_key_file = arg;
        break;
    case 't':
        args->tp_key_file = arg;
        break;
    case 'a':
        args->listen_addr = arg;
        break;
    case 'p':
        args->port = strtoul(arg, &p, 10);
        if (errno != 0) {
            return errno;
        } else if ((p - arg) != strlen(arg)) {
            log_fatal("Invalid port argument\n");
            return EINVAL;
        }
        break;
    case 'o':
        args->listen_port = strtoul(arg, &p, 10);
        if (errno != 0) {
            return errno;
        } else if ((p - arg) != strlen(arg)) {
            log_fatal("Invalid listen_port argument\n");
            return EINVAL;
        }
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

int
main(int argc, char **argv)
{
    int err = 0;
    //json_t *j_root = json_object();
    //json_add_str(j_root, "hola", "adeu");
    //json_decref(j_root);
    //return 0;
    struct arguments args;

    /* Default values. */
    args.verbose = false;
    args.conf_file = DEF_PUB_CONF_FILE;
    args.keys_file = DEF_PUB_KEYS_FILE;
    args.broker_key_file = DEF_BROKER_KEY_FILE;
    args.tp_key_file = DEF_TP_KEY_FILE;
    args.host = DEF_HOST;
    args.port = DEF_PORT;
    args.listen_addr = DEF_LIST_ADDR;
    args.listen_port = DEF_LIST_PORT;

    if (argp_parse(&argp, argc, argv, 0, 0, &args) != 0) {
        log_fatal("Error parsing command line options\n");
        return 1;
    }

    if (!args.verbose) {
        log_set_level(LOG_INFO);
    }

    log_info("MQTT server = %s:%u", args.host, args.port);
    log_info("conf_file = %s", args.conf_file);
    log_info("keys_file = %s",args.keys_file);
    log_info("broker_key_file = %s", args.broker_key_file);
    log_info("tp_key_file = %s", args.tp_key_file);

    struct publisher pub;
    memset(&pub, 0, sizeof(struct publisher));

    if (publisher_conf_load(&pub, args.conf_file) != 0) {
        return -2;
    }
    log_debug("Loaded publisher config");
    log_info("Value subtopic: %s", pub.subtopic.name);
    log_info("Value length: %u bits", pub.subtopic.val_len);

    if (access(args.keys_file, F_OK) != -1) {
        // file exists, we load the keys from it
        if (publisher_keys_load(&pub, args.keys_file) != 0) {
            return -2;
        }
        log_debug("Loaded publisher keys");
    } else {
        // file doesn't exists, we generate new keys and store them in the file
        log_info("Generating signing key...");
        publisher_keys_init(&pub);
        if (publisher_keys_store(&pub, args.keys_file) != 0) {
            return -2;
        }
        log_debug("Stored publisher keys");
    }

    client_set_id(&pub.cl);
    log_info("pk = %s", pub.cl.id);
    if (client_broker_pk_load(&pub.cl, args.broker_key_file) != 0) {
        return -2;
    }
    log_debug("Loaded Broker public key");
    if (client_tp_pk_load(&pub.cl, args.tp_key_file) != 0) {
        return -2;
    }
    log_debug("Loaded Third Party public key");

    if (pub.rkp.t == -1) {
        pub.state = PUB_STATE_REG;
        log_debug("Publisher state is PUB_STATE_KX");
        // ratchet key hasn't been set up yet.  Let's perform a key echange with
        // the server (TP).
    } else {
        pub.state = PUB_STATE_ACTIVE;
        log_debug("Publisher state is PUB_STATE_ACTIVE");
    }

    init_actions_src();

    log_debug("Starting MQTT publisher loop");
    while ((err = mqtt_pub_loop(&pub,
                                args.host, args.port,
                                args.listen_addr, args.listen_port,
                                args.keys_file)) == 0);
    return err;
}
