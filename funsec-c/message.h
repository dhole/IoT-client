#ifndef SECFUN_MESSAGE_H
#define SECFUN_MESSAGE_H

#include <string.h>
#include <stdbool.h>
#include <jansson.h>
#include <sodium.h>

#include "keys.h"
#include "client.h"

#define TOKEN_LEN 12+1

int wmsg_verify(json_t *j_root, const char *topic,
               const unsigned char pk[crypto_sign_PUBLICKEYBYTES]);

int wmsg_sign(json_t *j_root, const char *topic,
              const unsigned char sk[crypto_sign_SECRETKEYBYTES]);

int wmsg_add_from(json_t *j_root, const char dev_id[ID_LEN]);
int wmsg_add_topic(json_t *j_root, const char *topic);
int wmsg_add_type(json_t *j_root, const enum msg_type type);
int msg_add_token(json_t *j_root, const char *token);
bool msg_match_token(json_t *j_root, const char *token);
void gen_token(char token[TOKEN_LEN]);
enum prot_action msg_get_action(json_t *j_root);
json_t *msg_wrap(json_t *j_msg, const struct client *cl, const enum msg_type type);
json_t *msg_wrap_sign(json_t *j_msg, const struct client *cl, const char *topic,
                      const enum msg_type type);

json_t *msg_make_pub_val_reg_start(const char *token, const char *val_topic,
                                   const unsigned int val_len,
                                   unsigned char kx_pk[crypto_kx_PUBLICKEYBYTES]);
json_t * msg_make_val(unsigned char *val_map, size_t len, unsigned int t, int64_t ts);
#endif
