#include <stdio.h>
#include <sodium.h>
#include <jansson.h>
#include <sys/stat.h>

#include "base64.h"
#include "client.h"
#include "json_utils.h"

void
usage(char *argv0)
{
	printf("Usage: %s [-p keys_file]/[-s pk_file sk_file]\n", argv0);
}

int
gen_pub_keys(char *keys_file)
{
    struct publisher pub;
    memset(&pub, 0, sizeof(struct publisher));
    publisher_keys_init(&pub);
    if (publisher_keys_store(&pub, keys_file) != 0) {
	return -1;
    }
    return 0;
}

int
gen_sub_keys(char *pk_file, char *sk_file)
{
    int err = 0;
    unsigned char pk[crypto_sign_PUBLICKEYBYTES];
    unsigned char sk[crypto_sign_SECRETKEYBYTES];

    crypto_sign_keypair(pk, sk);

    json_t *j_pk = NULL, *j_sk = NULL;
    if ((j_pk = json_object()) == NULL) {
	printf("Error creating json object\n");
	return -1;
    }
    if ((j_sk = json_object()) == NULL) {
	printf("Error creating json object\n");
	err = -1; goto gen_keys_clean;
    }
    if (json_add_b64(j_pk, "pk", pk, crypto_sign_PUBLICKEYBYTES) != 0) {
	err = -1; goto gen_keys_clean;
    }
    if (json_add_b64(j_sk, "sk", sk, crypto_sign_SECRETKEYBYTES) != 0) {
	err = -1; goto gen_keys_clean;
    }

    if (json_dump_file(j_pk, pk_file, JSON_SORT_KEYS | JSON_INDENT(2)) == -1) {
        printf("Error wriring json file %s\n", pk_file);
        err = -1; goto gen_keys_clean;
    }
    if (json_dump_file(j_sk, sk_file, JSON_SORT_KEYS | JSON_INDENT(2)) == -1) {
        printf("Error wriring json file %s\n", sk_file);
        err = -1; goto gen_keys_clean;
    }
    if (chmod(sk_file, S_IRUSR | S_IWUSR) != 0) {
        printf("Error setting permissions to json file %s\n", sk_file);
        err = -1; goto gen_keys_clean;
    }

gen_keys_clean:
    if (j_sk) { json_decref(j_sk); }
    if (j_pk) { json_decref(j_pk); }
    return err;
}

int
main(int argc, char **argv)
{
    if (strcmp(argv[1], "-p") == 0) {
	if (argc != 3) {
	    usage(argv[0]);
	    return -2;
	}
	char *keys_file = argv[2];
	return gen_pub_keys(keys_file);
    } else if (strcmp(argv[1], "-s") == 0) {
	if (argc != 4) {
	    usage(argv[0]);
	    return -2;
	}
	char *pk_file = argv[2];
	char *sk_file = argv[3];
	return gen_sub_keys(pk_file, sk_file);
    } else {
	usage(argv[0]);
    }
}
