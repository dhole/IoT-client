#ifndef SECFUN_TIMER_H
#define SECFUN_TIMER_H

struct timer {
    pthread_t th;
    chan_t *chan;
};

struct timer_args {
    chan_t *chan;
    chan_t *timer_chan;
};

struct timer timer_init(chan_t *chan);
void timer_free(struct timer *timer);
void timer_set(struct timer *timer, const int t);

#endif
