#ifndef SECFUN_PROTOCOL_H
#define SECFUN_PROTOCOL_H

#include <stdbool.h>
#include <jansson.h>

#define PROT_NAME "secfun"
#define PROT_VER  "v0"

#define VERSION "v0.0"

#define EMAIL "sanou@usc.edu"

#define DEF_PUB_KEYS_FILE "publisher_keys.json"
#define DEF_PUB_CONF_FILE "publisher_conf.json"
#define DEF_BROKER_KEY_FILE "broker_key.pub.json"
#define DEF_TP_KEY_FILE "tp_key.pub.json"
#define DEF_HOST "localhost"
#define DEF_PORT 1883
#define DEF_LIST_ADDR "127.0.01"
#define DEF_LIST_PORT 5000

#define PUB_DEV_TOPIC PROT_NAME "/" PROT_VER "/pub/"
#define PUB_VAL_TOPIC PROT_NAME "/" PROT_VER "/val/"
#define SUB_DEV_TOPIC PROT_NAME "/" PROT_VER "/sub/"
#define SUB_FUN_TOPIC PROT_NAME "/" PROT_VER "/fun/"

#define FIXED_SCALE 256 // Fixed point scale factor

//       +----------------------+
//       |                      |
//       v                      | send fail
//   REG_START+-----------------+
//       +                      |
//       | kx_key_init()        |
//       | send pk to T         |
//       v                      | {recv timeout, recv fail}
//   REG_SENT_PK+---------------+
//       +                      |
//       | recv pk from T       |
//       v                      | {gen_key, verify pk} fail
//   REG_RECVD_PK+--------------+
//       +
//       | verify pk OK
//       | ratchet_key_init() OK
//       v
//   REG_FINISH

enum reg_state {
    REG_STATE_START = 0,
    REG_STATE_SENT_PK,
    REG_STATE_SENT_PK_CONF,
    REG_STATE_RECVD_PK,
    REG_STATE_FINALIZE,
    REG_STATE_FINISHED,
};

enum pub_state {
    PUB_STATE_REG = 0,
    PUB_STATE_ACTIVE,
};

enum subtopic_state {
    SUBTOPIC_STATE_INIT = 0,
    SUBTOPIC_STATE_ACTIVE,
};

enum role {
    PUBLISHER   = 1 << 0,
    SUBSCRIBER  = 1 << 1,
    BROKER      = 1 << 2,
    THIRD_PARTY = 1 << 3,
    UNKNOWN     = 1 << 4,
};

enum prot_action {
    // Publisher registers to publish values to a topic (This includes the key
    // exchange to establish rkp between Publisher and TP.
    ACTION_PUB_VAL_REG_START = 0,
    ACTION_PUB_VAL_REG_END,
    ACTION_PUB_VAL_REG_ABORT,
    // Subscriber registers a function
    ACTION_SUB_FUN_REG_START = 5,
    ACTION_SUB_FUN_REG_END,
    ACTION_SUB_FUN_REG_ABORT,
    // Subscriber requests the ratchet key used to mask a function
    ACTION_SUB_FUN_REQ_START = 10,
    ACTION_SUB_FUN_REQ_END,
    ACTION_SUB_FUN_REQ_ABORT,
    //
    //ACTION_MISSING,
    ACTION_UNKNOWN,
    // Dummy action to figure maximum value of of prot_actions
    ACTION_DUMMY_LEN,
};

enum msg_type {
    MSG_TYPE_MSG = 0,
    MSG_TYPE_VALMSG,
    MSG_TYPE_FUNMSG,
};

int actions_src[ACTION_DUMMY_LEN];

void init_actions_src();
bool allowed_action_from(enum prot_action action, enum role from);
bool msg_validate_action(json_t *j_msg, enum prot_action action);

#endif
