#include <ctype.h>
#include <string.h>
#include <math.h>

#include "utils.h"

void
u642bin(uint64_t v, bool *str, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++) {
		str[i] = v & (1U << i);
	}
	return;
}

void
buf2bin(unsigned char *buf, bool *str, size_t n)
{
	size_t i;
	for (i = 0; i < n; i++) {
		str[i] = (buf[i/8] & (1U << (i%8))) ? 1 : 0;
	}
	return;
}

bool
buf2bin_test()
{
	unsigned char buf[] = {10, 42};
	bool str_good[] = {0,1,0,1,0,0,0,0,
		           0,1,0,1,0,1,0,0};
	bool str[16];
	buf2bin(buf, str, 16);
	//size_t i;
	//for (i = 0; i < 16; i++) {
	//	printf("%d ", str_good[i]);
	//}
	//printf("\n");
	//for (i = 0; i < 16; i++) {
	//	printf("%d ", str[i]);
	//}
	//printf("\n");

	if (memcmp(str_good, str, 16 * sizeof(bool)) == 0) {
		//printf("PASSED\n");
		return true;
	} else {
		//printf("NOT PASSED :(\n");
		return false;
	}
}

uint64_t
bin2u64(bool *str, size_t n)
{
	size_t i;
	uint64_t v = 0;
	for (i = 0; i < n; i++) {
		 v += str[i] * ((uint64_t) pow(2, i));
	}
	return v;
}
