#include <string.h>
#include <jansson.h>
#include <assert.h>
#include <sys/stat.h>

#include "client.h"
#include "base64.h"
#include "json_utils.h"
#include "log.c/log.h"

// Sets the client id to the base64 encoding of its signing public key
void
client_set_id(struct client *cl)
{
    char *id = (char *) base64_encode(cl->sig.pk, crypto_sign_PUBLICKEYBYTES, NULL);
    strncpy(cl->id, id, ID_LEN + 1);
    strncpy(cl->mqtt_id, id, MQTT_ID_LEN);
    cl->mqtt_id[MQTT_ID_LEN] = '\0';
    free(id);
}

// Load the Broker public key from a filename in json encoding
// TODO: Merge this function with client_tp_pk_load
int
client_broker_pk_load(struct client *cl, const char *filename)
{
    json_error_t error;
    json_t *j_root = json_load_file(filename, 0, &error);
    if (j_root == NULL) {
        log_error("client_broker_pk_load: Error reading json file %s", filename);
        return -1;
    }
    if (json_get_b64(j_root, "pk", cl->broker_pk, crypto_sign_PUBLICKEYBYTES) != 0) {
	json_decref(j_root);
	return -1;
    }
    size_t out_len;
    char *broker_pk_b64 = (char *) base64_encode(cl->broker_pk, crypto_sign_PUBLICKEYBYTES, &out_len);
    //log_debug("out_len = %d", out_len);
    //log_debug("crypto_sign_PUBLICKEYBYTESB64 = %d", crypto_sign_PUBLICKEYBYTESB64);
    //assert(out_len == crypto_sign_PUBLICKEYBYTESB64);
    strcpy(cl->broker_pk_b64, broker_pk_b64);
    free(broker_pk_b64);
    json_decref(j_root);
    return 0;
}

// Load the Third Party public key from a filename in json encoding
int
client_tp_pk_load(struct client *cl, const char *filename)
{
    json_error_t error;
    json_t *j_root = json_load_file(filename, 0, &error);
    if (j_root == NULL) {
        log_error("client_tp_pk_load: Error reading json file %s", filename);
        return -1;
    }
    if (json_get_b64(j_root, "pk", cl->tp_pk, crypto_sign_PUBLICKEYBYTES) != 0) {
	json_decref(j_root);
	return -1;
    }
    size_t out_len;
    char *tp_pk_b64 = (char *) base64_encode(cl->tp_pk, crypto_sign_PUBLICKEYBYTES, &out_len);
    //assert(out_len == crypto_sign_PUBLICKEYBYTESB64);
    strcpy(cl->tp_pk_b64, tp_pk_b64);
    free(tp_pk_b64);
    json_decref(j_root);
    return 0;
}

int
publisher_conf_load(struct publisher *pub, const char *filename)
{
    json_error_t error;
    json_t *j_root = json_load_file(filename, 0, &error);
    if (j_root == NULL) {
        log_error("publisher_conf_load: Error reading json file %s", filename);
        return -1;
    }
    const char *val_topic;
    unsigned int val_len;
    if (json_unpack(j_root, "{s:s, s:i}",
		    "val_topic", &val_topic, "val_len", &val_len) != 0) {
	json_decref(j_root);
        log_error("publisher_conf_load: Error decoding json file: %s", filename);
        return -1;
    }
    // FIXME: Figure out why json_unpack is setting val_topic to NULL.  After this is fixed, remove
    // the following code
    json_t *j_str = json_object_get(j_root, "val_topic");
    val_topic = json_string_value(j_str);
    // END
    if (strlen(val_topic) > SUBTOPIC_MAX_LEN - 1) {
        log_error("publisher_conf_load: Loaded val_topic is longer than %d", SUBTOPIC_MAX_LEN);
        return -1;
    }
    strncpy(pub->subtopic.name, val_topic, SUBTOPIC_MAX_LEN);
    pub->subtopic.val_len = val_len;

    json_decref(j_root);
    return 0;
}

// Initialize the publisher keys by initializing the signature key pair and by
// setting the ratchet round to -1 (meaning not set up).
void
publisher_keys_init(struct publisher *pub)
{
    sign_key_init(&pub->cl.sig);
    pub->rkp.t = -1;
}

// Store the publisher keys into a filename in json encoding
int
publisher_keys_store(const struct publisher *pub, const char *filename) {
    // An optimization here would consist on storing the json root with pointers
    // to sign_keys.pk, sign_keys.sk, ratchet_key.key, ratchet_key.t so that
    // every store operation doesn't involve constructing the json data structure
    // again.
    int err = 0;
    char *sig_pk_b64 =
        (char *) base64_encode(pub->cl.sig.pk, crypto_sign_PUBLICKEYBYTES, NULL);
    char *sig_sk_b64 =
        (char *) base64_encode(pub->cl.sig.sk, crypto_sign_SECRETKEYBYTES, NULL);
    char *rk_key_b64 =
        (char *) base64_encode(pub->rkp.key, crypto_kdf_KEYBYTES, NULL);

    json_t *j_root = json_pack("{s:{s:s, s:s}, s:{s:s, s:i}}",
                               "sign_keys",
                               "pk", sig_pk_b64, "sk", sig_sk_b64,
                               "ratchet_key",
                               "key", rk_key_b64, "t", pub->rkp.t);
    if (j_root == NULL) {
        log_error("publisher_keys_store: Error packing json file %s", filename);
        err = -1; goto publisher_keys_store_clean;
    }

    if (json_dump_file(j_root, filename, JSON_SORT_KEYS | JSON_INDENT(2)) == -1) {
        log_error("publisher_keys_store: Error wriring json file %s", filename);
        err = -1; goto publisher_keys_store_clean;
    }

    if (chmod(filename, S_IRUSR | S_IWUSR) != 0) {
        log_error("Error setting permissions to json file %s", filename);
        err = -1; goto publisher_keys_store_clean;
    }


publisher_keys_store_clean:
    if (j_root) { json_decref(j_root); }
    free(sig_pk_b64);
    free(sig_sk_b64);
    free(rk_key_b64);

    return err;
}

// Load the publisher keys from a filename in json encoding
int
publisher_keys_load(struct publisher *pub, const char *filename) {
    json_error_t error;
    json_t *j_root = json_load_file(filename, 0, &error);
    if (j_root == NULL) {
        log_error("publisher_keys_load: Error reading json file %s", filename);
        return -1;
    }
    json_t *j_sig, *j_rk;
    if (json_unpack(j_root, "{s:o, s:o}",
                    "sign_keys", &j_sig, "ratchet_key", &j_rk) != 0) {
        log_error("publisher_keys_load: Error parsing json file: %s", filename);
        return -1;
    }

    if (json_get_b64(j_sig, "pk", pub->cl.sig.pk, crypto_sign_PUBLICKEYBYTES) != 0) {
        log_error("publisher_keys_load: Error decoding sign_keys.pk from "
                  "json file: %s", filename);
        return -1;
    }
    if (json_get_b64(j_sig, "sk", pub->cl.sig.sk, crypto_sign_SECRETKEYBYTES) != 0) {
        log_error("publisher_keys_load: Error decoding sign_keys.sk from "
                  "json file: %s", filename);
        return -1;
    }
    if (json_get_b64(j_rk, "key", pub->rkp.key, randombytes_SEEDBYTES) != 0) {
        log_error("publisher_keys_load: Error decoding rk.key from "
                  "json file: %s", filename);
        return -1;
    }
    if (json_unpack(j_rk, "{s:I}", "t", &pub->rkp.t) != 0) {
        log_error("publisher_keys_load: Error decoding rk.t from "
                  "json file: %s", filename);
        return -1;
    }

    /*
    // DEBUG
    wmsg_sign(j_root, "topic/foo/bar", ck->sig.sk);
    printf("Signature: %d\n", wmsg_verify(j_root, "topic/foo/bar", ck->sig.pk));
    */

    json_decref(j_root);

    return 0;
}
