#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <sys/stat.h>
#include <sodium.h>
#include <jansson.h>

//#include "client.h"
#include "keys.h"

//#include "message.h"

// Advance the ratchet key using the KDF
void
ratchet_key_advance(struct ratchet_key *rk)
{
    unsigned char new_rk[crypto_kdf_KEYBYTES];
    crypto_kdf_derive_from_key(new_rk, sizeof new_rk, 0, RK_ADV_CTX, rk->key);
    memcpy(rk->key, new_rk, sizeof new_rk);
    rk->t++;
}

// Generate a secure pseudorandom id'th seed from the ratchet key
void
ratchet_key_gen_seed(const struct ratchet_key *rk,
                     unsigned char seed[randombytes_SEEDBYTES], uint64_t id)
{
    crypto_kdf_derive_from_key(seed, randombytes_SEEDBYTES, id, RK_GEN_SEED_CTX,
                               rk->key);
}

// Generate a cryptographycally secure pseudorandom buffer using the ratchet key
// to derive an id'th seed with a KDF, which we feed to a CSPRNG to get the
// buffer data.
void
ratchet_key_cprng(const struct ratchet_key *rk,
                  void * const buf, const size_t size, uint64_t id)
{
    unsigned char seed[randombytes_SEEDBYTES];
    ratchet_key_gen_seed(rk, seed, id);
    randombytes_buf_deterministic(buf, size, seed);
}

// Generate a public signature key pair
void
sign_key_init(struct sign_key *sig)
{
    crypto_sign_keypair(sig->pk, sig->sk);
}

// Generate a key exchange key pair
void
kx_key_init(struct kx_key *kk)
{
    crypto_kx_keypair(kk->pk, kk->sk);
}

// Compute ratchet key (shared secret) by using the client secret key and
// server's (TP) public key.
int
ratchet_key_init(struct ratchet_key *rk, struct kx_key *kk,
                 unsigned char server_pk[crypto_kx_PUBLICKEYBYTES])
{
    unsigned char shared_key[crypto_kx_SESSIONKEYBYTES];
    if (crypto_kx_client_session_keys(shared_key, NULL,
                                      kk->pk, kk->sk, server_pk) != 0) {
        /* Suspicious server public key, bail out */
        return -1;
    }
    // TODO generate the ratchet key from shared_key
    // rk->key <- shared_key
    memset(rk->key, 0, crypto_kdf_KEYBYTES);
    memcpy(rk->key, shared_key, fmin(crypto_kdf_KEYBYTES, crypto_kx_SESSIONKEYBYTES));
    rk->t = 0;
    return 0;
}

void
label_xor(unsigned char *dst, unsigned char *a, unsigned char *b) {
    int i;
    for (i = 0; i < GRBL_BLCK_SIZE; i++) {
        dst[i] = a[i] ^ b[i];
    }
}

// Map the id'th bit vector val using the labels generated with the ratchet key
void
label_map_value(const struct ratchet_key *rk, bool *val, size_t len,
                unsigned char *val_map)
{
    size_t i;
    unsigned char *labels = malloc(GRBL_BLCK_SIZE * len * 2);
    unsigned char *zero_labels = malloc(GRBL_BLCK_SIZE * len);
    unsigned char *delta = malloc(GRBL_BLCK_SIZE);

    ratchet_key_cprng(rk, delta,  GRBL_BLCK_SIZE, SEED_ID_DELTA);
    ////
    //printf(">>> delta:");
    //for (i = 0; i < GRBL_BLCK_SIZE; i++)
    //    printf("%02x", delta[i]);
    //printf("\n");
    ////
    ratchet_key_cprng(rk, zero_labels,  GRBL_BLCK_SIZE * len, SEED_ID_LABELS);

    *((uint8_t *) delta) |= 1;

    for (i = 0; i < len; i++) {
        memcpy(&labels[2 * i * GRBL_BLCK_SIZE],
               &zero_labels[i * GRBL_BLCK_SIZE], GRBL_BLCK_SIZE);
        label_xor(&labels[(2 * i + 1) * GRBL_BLCK_SIZE],
                  &zero_labels[i * GRBL_BLCK_SIZE], delta);
        ////
        //printf(">>> ");
        //for (j = 0; j < GRBL_BLCK_SIZE; j++)
        //    printf("%02x", labels[2 * i * GRBL_BLCK_SIZE + j]);
        //printf(" ");
        //for (j = 0; j < GRBL_BLCK_SIZE; j++)
        //    printf("%02x", labels[(2 * i + 1) * GRBL_BLCK_SIZE + j]);
        //printf("\n");
        //// 
    }

    for (i = 0; i < len; i++) {
        memcpy(&val_map[i * GRBL_BLCK_SIZE],
               &labels[(2 * i + val[i]) * GRBL_BLCK_SIZE], GRBL_BLCK_SIZE);
    }

    free(delta);
    free(zero_labels);
    free(labels);
}
