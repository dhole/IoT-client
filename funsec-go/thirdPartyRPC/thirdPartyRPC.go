package thirdPartyRPC

import (
	"../funsec"
	"encoding/gob"
)

type FunArgs struct {
	ID      funsec.FunctionID
	InputTs map[funsec.ValTopic]uint64
}

type WMsgArgs struct {
	WMsg  funsec.WMsg
	Topic string
}

type ReplyWMsg struct {
	WMsg funsec.WMsg
	err  error
}

func GobRegister() {
	gob.Register(funsec.Msg{})
	gob.Register(funsec.MsgVal{})
	gob.Register(funsec.PubValRegStart{})
	gob.Register(funsec.PubValRegEnd{})
	gob.Register(funsec.SubFunRegStart{})
	gob.Register(funsec.SubFunRegEnd{})
	gob.Register(funsec.SubFunReqStart{})
	gob.Register(funsec.SubFunReqEnd{})
	gob.Register(funsec.Abort{})
}
