package main

import (
	cir "../circuit"
	"../funsec"
	tp "../thirdPartyRPC"
	"encoding/json"
	"fmt"
	"github.com/alexflint/go-arg"
	"github.com/op/go-logging"
	"io/ioutil"
	"net"
	"net/rpc"
	"os"
	"strings"
	"time"
	//"strings"
	//proto "github.com/huin/mqtt"
)

var log = logging.MustGetLogger("tp")
var logFormat = logging.MustStringFormatter(
	`%{time:2006-01-02 15:04:05} %{color}%{level} %{shortfile} %{callpath:4} %{color:reset}%{message}`,
)

var cfs *funsec.ClientsFuncs

var sig *funsec.SignKey
var myDevID funsec.DeviceID

// FunctionManager is the type that implements RPC methods dealing with functions
type FunctionManager int

var gcsToDelete chan *cir.GarbleCircuit

func deleteGcs() {
	for {
		gc := <-gcsToDelete
		go func() {
			time.Sleep(time.Duration(30) * time.Second)
			gc.Delete()
		}()
	}
}

// Garble is the RPC that garbles the circuit corresponding to a function
func (fm *FunctionManager) Garble(funArgs *tp.FunArgs, garbleRepy *cir.CircuitData) error {
	fn := cfs.GetFn(funArgs.ID)
	if fn == nil {
		return fmt.Errorf("Function %s not found", funArgs.ID)
	}
	rkps := make(map[string]*funsec.RatchetKey, len(fn.Inputs))
	for _, valTopic := range fn.Inputs {
		cl := cfs.GetClByValTopic(valTopic)
		if cl == nil {
			return fmt.Errorf("No Publisher has registered input %s", valTopic)
		}
		rkp := cl.Pub.Rkp
		if rkp == nil {
			return fmt.Errorf("Client %s hasn't established a rkp", cl.DevID)
		}
		if funArgs.InputTs[valTopic] < rkp.T {
			return fmt.Errorf("Requested rkp round %v, but client %s is at round %v",
				funArgs.InputTs[valTopic], cl.DevID, rkp.T)
		}
		advance := funArgs.InputTs[valTopic] - rkp.T
		for j := uint64(0); j < advance; j++ {
			rkp = rkp.Advance()
		}
		rkps[string(valTopic)] = rkp
		cl.Pub.SetRkp(rkp)
	}

	c := fn.Data.CopyCircuit()
	rkf := fn.GetRkf()
	// DEBUG: Artificial advance
	//rkf = rkf.Advance()
	log.Debug("[G] Using rkf.T =", rkf.T)
	mask := rkf.GenMask(c.OutputLen)
	//log.Debug("Mask", mask)
	if err := c.Mask(mask); err != nil {
		return fmt.Errorf("Error masking circuit: %v", err)
	}
	if err := c.Finish(); err != nil {
		return fmt.Errorf("Error finishing circuit: %v", err)
	}
	c.T = rkf.T
	defer fn.SetRkf(rkf.Advance())

	start := time.Now()
	c.Garble()
	c.TimeGarble = time.Since(start).Nanoseconds()

	log.Debug("[G] Garbled circuit for function ID ", funArgs.ID)

	labelMaps := make(map[string][]cir.Label)
	for k, ws := range c.InputWires {
		labelMaps[k] = rkps[k].GenLabelMap(uint64(len(ws)))
		//fmt.Printf(">>> %v %+v\n", k, rkps[k])
		//fmt.Printf(">>> %+v\n", labelMaps[k])
	}

	start = time.Now()
	c.EncryptInputMaps(labelMaps)
	c.TimeEncryptInputs = time.Since(start).Nanoseconds()

	*garbleRepy = *c
	//gcsToDelete <- c.Gc
	return nil
}

// ClientManager is the type that implements RPC methods dealing with clients
type ClientManager int

// PubValRegStart is the RPC that performs the ActionPubValRegStart
func (cm *ClientManager) PubValRegStart(wmsgArgs *tp.WMsgArgs, replyWMsg *funsec.WMsg) error {
	cl, err := funsec.WMsgCheckVerifyServer(cfs, &wmsgArgs.WMsg, wmsgArgs.Topic)
	if err != nil {
		return err
	}
	msg, ok := wmsgArgs.WMsg.Msg.(funsec.Msg)
	if !ok {
		return fmt.Errorf("Invalid msg")
	}
	body, ok := msg.Body.(funsec.PubValRegStart)
	if !ok {
		return fmt.Errorf("Invalid body")
	}

	pub := cfs.InitPub(cl.DevID, body.ValTopic, body.ValLen)
	if pub == nil {
		return fmt.Errorf("val_topic already registered")
	}
	log.Infof("[P] Initialized Publisher %v for val_topic %v with len %v",
		cl.DevID, body.ValTopic, body.ValLen)
	kk := funsec.NewKxKeyPair()
	rkp, err := funsec.NewRatchetKeyKxServer(&kk, &body.KxPk)
	if err != nil {
		return err
	}
	//log.Debug("rkp =", funsec.Base64URLEncode(rkp.Key[:]))
	pub.SetRkp(&rkp)
	replyWMsg.From = myDevID
	replyWMsg.Msg = funsec.Msg{
		Token:  msg.Token,
		Action: funsec.ActionPubValRegEnd,
		Body: funsec.PubValRegEnd{
			KxPk: kk.Pk,
		},
	}
	replyWMsg.Sign(wmsgArgs.Topic, &sig.Sk)
	return nil
}

// SubFunReqStart is the RPC that performs the ActionSubFunRegStart
func (cm *ClientManager) SubFunReqStart(wmsgArgs *tp.WMsgArgs, replyWMsg *funsec.WMsg) error {
	cl, err := funsec.WMsgCheckVerifyServer(cfs, &wmsgArgs.WMsg, wmsgArgs.Topic)
	if err != nil {
		return err
	}
	msg, ok := wmsgArgs.WMsg.Msg.(funsec.Msg)
	if !ok {
		return fmt.Errorf("Invalid msg")
	}
	body, ok := msg.Body.(funsec.SubFunReqStart)
	if !ok {
		return fmt.Errorf("Invalid body")
	}

	fun := cfs.RegSubToFun(cl.DevID, body.FunID)
	if fun == nil {
		return fmt.Errorf("fun_id not found")
	}
	rkf := fun.GetRkf()
	log.Infof("[S] Registered Subscriber %v for function %v", cl.DevID, body.FunID)

	kk := funsec.NewKxKeyPair()
	key, err := funsec.NewSymKeyKxServer(&kk, &body.KxPk)
	if err != nil {
		return err
	}
	rkfBuf, _ := json.Marshal(rkf)
	//log.Debugf("[S] Sending %+v\n%s", rkf, rkfBuf)
	encRkf, nonce := key.Encrypt(rkfBuf)
	replyWMsg.From = myDevID
	replyWMsg.Msg = funsec.Msg{
		Token:  msg.Token,
		Action: funsec.ActionSubFunReqEnd,
		Body: funsec.SubFunReqEnd{
			FunID: body.FunID,
			KxPk:  kk.Pk,
			Nonce: *nonce,
			Rkf:   encRkf,
		},
	}
	replyWMsg.Sign(wmsgArgs.Topic, &sig.Sk)
	return nil
}

type args struct {
	Verbose  bool   `arg:"-v,help:Verbose output"`
	PubKey   string `arg:"-P,help:Public key file"`
	SecKey   string `arg:"-k,help:Private key file"`
	DataBase string `arg:"-d,help:DataBase file"`
	IP       string `arg:"-l,help:IP to listen at for RPC"`
	Port     int    `arg:"-o,help:port to listen at for RPC"`
}

func (args) Version() string {
	return fmt.Sprintf("%s third party %s", funsec.ProtName, funsec.Version)
}

func (args) Description() string {
	return fmt.Sprintf("%s third party -- Third Party for %s %s",
		funsec.ProtName, funsec.ProtName, funsec.ProtVer)
}

func main() {
	var args args
	args.Verbose = false
	args.PubKey = funsec.DefaultTPPubKeyFile
	args.SecKey = funsec.DefaultTPSecKeyFile
	args.DataBase = funsec.DefaultTPDataBaseFile
	args.IP = funsec.DefaultTPRPCListenIP
	args.Port = funsec.DefaultTPRPCListenPort
	arg.MustParse(&args)

	logBackend := logging.NewLogBackend(os.Stderr, "", 0)
	logging.SetBackend(logBackend)
	logging.SetFormatter(logFormat)
	if args.Verbose {
		logging.SetLevel(logging.DEBUG, "tp")
	} else {
		logging.SetLevel(logging.INFO, "tp")
	}

	log.Notice("Starting Third Party...")

	cfs = funsec.NewClientsFuncs()

	pubs, err := ioutil.ReadFile("publishers.txt")
	if err != nil {
		log.Critical("Error reading publishers.txt")
		return
	}
	for _, pub := range strings.Split(string(pubs), "\n") {
		if pub == "" {
			continue
		}
		log.Debug("Adding publisher", pub)
		if !cfs.AddCl(funsec.DeviceID(pub), true, false) {
			log.Critical("Error adding client")
			return
		}
	}
	subs, err := ioutil.ReadFile("subscribers.txt")
	if err != nil {
		log.Critical("Error reading subscribers.txt")
		return
	}
	for _, sub := range strings.Split(string(subs), "\n") {
		if sub == "" {
			continue
		}
		log.Debug("Adding subscriber", sub)
		if !cfs.AddCl(funsec.DeviceID(sub), false, true) {
			log.Critical("Error adding client")
			return
		}
	}

	prelude, err := ioutil.ReadFile("functions/prelude.lsp")
	if err != nil {
		log.Critical("Error reading file", "functions/prelude.lsp")
		return
	}
	funIDs := make([]funsec.FunctionID, 0)
	files, err := ioutil.ReadDir("functions/autoload")
	if err != nil {
		log.Critical("Error reading directory functions/autoload")
		return
	}
	for _, file := range files {
		funDesc, err := ioutil.ReadFile("functions/autoload/" + file.Name())
		if err != nil {
			log.Critical("Error reading file", file.Name())
			return
		}
		funID, err := cfs.AddFun(funsec.FunDesc(prelude), funsec.FunDesc(funDesc))
		if err != nil {
			log.Critical("Error adding function", file.Name())
			return
		}
		funIDs = append(funIDs, funID)
		log.Info("Added function", funID)
		// The Broker doesn't need the circuit
	}

	sig, err = funsec.LoadSigKeys(args.PubKey, args.SecKey)
	if err != nil {
		log.Critical("Unable to load keys:", err)
		return
	}
	myDevID = funsec.DeviceID(funsec.Base64URLEncode(sig.Pk[:]))

	tp.GobRegister()
	var cm ClientManager
	rpc.Register(&cm)
	var fm FunctionManager
	rpc.Register(&fm)
	l, e := net.Listen("tcp", fmt.Sprintf("%s:%d", args.IP, args.Port))
	if e != nil {
		log.Critical("listen error:", e)
		return
	}

	os.Create("tmp/thirdParty.lock")
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Error("accept error:", err)
			continue
		}
		log.Infof("Accepted new RPC connection from %s", conn.RemoteAddr())
		go rpc.ServeConn(conn)
	}
}
