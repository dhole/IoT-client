package main

import (
	cir "../circuit"
	"../funsec"
	"../mqtt"
	tp "../thirdPartyRPC"
	"bufio"
	"bytes"
	"fmt"
	"github.com/alexflint/go-arg"
	"github.com/op/go-logging"
	"io/ioutil"
	"os"
	"sync"

	//"log"
	"net"
	"net/rpc"
	"strings"
	"time"

	proto "github.com/huin/mqtt"
)

type Statistic struct {
	Inputs,
	Outputs,
	NGates,
	NAnds,
	NOrs,
	NNots,
	NXors uint64
	SizeGarbledCircuit uint32
	TimeEncryptInputs,
	TimeDecryptInputs,
	TimeGarble,
	TimeEval,
	TimeSendGarbled int64
}

var statsChan chan Statistic
var funMutex sync.Mutex
var funSerial bool
var log = logging.MustGetLogger("broker")
var logFormat = logging.MustStringFormatter(
	`%{time:2006-01-02 15:04:05} %{color}%{level} %{shortfile} %{callpath:4} %{color:reset}%{message}`,
)

var cfs *funsec.ClientsFuncs
var tpRPCClient *rpc.Client

var sig *funsec.SignKey
var myDevID funsec.DeviceID

func statsWriter(statsFilename string) {
	f, err := os.OpenFile(statsFilename, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	bf := bufio.NewWriter(f)
	defer bf.Flush()
	statsChan = make(chan Statistic, 64)

	stat, err := f.Stat()
	if err != nil {
		panic(err)
	}
	if stat.Size() == 0 {
		_, err := bf.WriteString("inputs, outputs, ngates, nxors, nands, nors, nnots, size, time_encrypt_in, time_decrypt_in, time_garble, time_eval, time_send\n")
		if err != nil {
			panic(err)
		}

	}

	log.Notice("Writing statistics to", statsFilename)
	for {
		stat := <-statsChan
		_, err := bf.WriteString(fmt.Sprintf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
			stat.Inputs, stat.Outputs, stat.NGates,
			stat.NXors, stat.NAnds, stat.NOrs, stat.NNots,
			stat.SizeGarbledCircuit,
			stat.TimeEncryptInputs, stat.TimeDecryptInputs,
			stat.TimeGarble, stat.TimeEval,
			stat.TimeSendGarbled))
		if err != nil {
			panic(err)
		}
		bf.Flush()
	}
}

// Publish an Abort message (for an abort action) to the device-specific topic.
func publishAbort(svr *mqtt.Server, topic, token string, action, errCode int, reason string) {
	log.Debugf("Sending abort to %s", topic)
	wmsg := funsec.WMsg{
		From: myDevID,
		Msg: funsec.Msg{
			Action: action,
			Body: funsec.Abort{
				ErrCode: errCode,
				Reason:  reason,
			},
			Token: token,
		},
	}
	wmsg.Sign(topic, &sig.Sk)
	if err := wmsg.ServerPublish(svr, topic); err != nil {
		log.Error("error publishing abort message:", err)
	}
}

// Filter all received messages at the Broker, and capture them if they are
// fromf the funsec protocol
func funSecFilter(svr *mqtt.Server, m *proto.Publish) bool {
	//log.Debugln("FILTER:", m.TopicName)
	//svr.Publish(&proto.Publish{
	//	Header:    proto.Header{QosLevel: 1},
	//	TopicName: "secfun/v0/pub/3otQHW1mIQuf42t_FHdV_C36AQBbApA2MMoev6GR5cY=",
	//	MessageId: 42,
	//	Payload:   proto.BytesPayload([]byte("{\"msg\": \"OLA K ASE\"}")),
	//})
	if !strings.HasPrefix(m.TopicName, funsec.TopicPrefix) {
		return false
	}
	go func() {
		topicGroup, _, valTopic := funsec.SplitTopic(m.TopicName)
		if topicGroup == "" {
			log.Error("[F] Invalid funsec topic")
			return
		}

		// Unmarshal msg payload
		var payloadBuf bytes.Buffer
		m.Payload.WritePayload(&payloadBuf)
		payload := payloadBuf.Bytes()
		wmsg, err := funsec.UnmarshalWMsg(payload)
		if err != nil {
			log.Errorf("[F] Error unmarshaling WMsg: %s", err)
			return
		}

		log.Debug("[F] Received message from", wmsg.From)
		cl, err := funsec.WMsgCheckVerifyServer(cfs, wmsg, m.TopicName)
		if err != nil {
			log.Errorf("[F] %s", err)
			return
		}

		switch topicGroup {
		case "pub":
			//log.Debug("filter: pub not implemented yet")
			funsecPub(svr, cl, wmsg, m.TopicName)
		case "sub":
			funsecSub(svr, cl, wmsg, m.TopicName)
		case "val":
			if valTopic == "" {
				log.Error("[F] No ValTopic found in message topic")
				return
			}
			funsecVal(svr, cl, wmsg, valTopic)
		default:
			log.Errorf("[F] Invalid topic group: %s", m.TopicName)
		}
	}()

	return true
}

// Process messages for the Publisher device-specific topic
func funsecPub(svr *mqtt.Server, cl *funsec.Client, wmsg *funsec.WMsg, topic string) {
	msg, ok := wmsg.Msg.(funsec.Msg)
	if !ok {
		log.Error("[P] Invalid Msg")
		return
	}
	switch msg.Action {

	case funsec.ActionPubValRegStart:
		body := msg.Body.(funsec.PubValRegStart)
		wmsgArgs := &tp.WMsgArgs{
			WMsg:  *wmsg,
			Topic: topic,
		}
		var replyWMsg funsec.WMsg
		// TODO: Add timeout
		err := tpRPCClient.Call("ClientManager.PubValRegStart", wmsgArgs, &replyWMsg)
		if err != nil {
			log.Error("[P] TP error:", err)
			publishAbort(svr, topic, msg.Token,
				funsec.ActionPubValRegAbort, 1, err.Error())
			return
		}
		//log.Debugf("ClientManager: %+v", replyWMsg)
		//log.Debugf("ClientManager: %s", string(replyWMsg_buf))
		if cfs.InitPub(cl.DevID, body.ValTopic, body.ValLen) == nil {
			errStr := "val_topic already registered"
			log.Error("[P]", errStr)
			publishAbort(svr, topic, msg.Token,
				funsec.ActionPubValRegAbort, 1, errStr)
			return
		}
		log.Infof("[P] Initialized Publisher %v for val_topic %v with len %v",
			cl.DevID, body.ValTopic, body.ValLen)
		err = replyWMsg.ServerPublish(svr, topic)
		if err != nil {
			log.Error("[P] Publish error:", err)
			publishAbort(svr, topic, msg.Token,
				funsec.ActionPubValRegAbort, 1, err.Error())
		}

	case funsec.ActionPubValRegAbort:
		//body := msg.Body.(funsec.Abort)
		// TODO: Implement this

	default:
		log.Errorf("[P] invalid action: %d", msg.Action)
		return
	}
}

// Process messages that are published as label-mapped values to a ValTopic
func funsecVal(svr *mqtt.Server, cl *funsec.Client, wmsg *funsec.WMsg, valTopic string) {
	msgVal, ok := wmsg.Msg.(funsec.MsgVal)
	if !ok {
		log.Errorf("[V] Invalid MsgVal")
		return
	}
	pub := cl.GetPub()
	if pub == nil {
		publishAbort(svr, cl.PubDevTopic, "none",
			funsec.ActionPubValRegAbort, 1, "Publisher not registered")
		return
	}
	//log.Debugf("V: %+v", msgVal.V)
	//log.Debugf("T: %+v", msgVal.T)
	//log.Debugf("Ts: %+v", msgVal.Ts)
	pub.SetVal(&funsec.Value{
		V:     msgVal.V,
		T:     msgVal.T,
		PubTs: msgVal.Ts,
		Ts:    time.Now().Unix(),
	})
	// Loop over all functions and if they take ValTopic as input, mark it as new
	fns := cfs.GetFns()
	for _, fn := range *fns {
		fn.NewInputsLock.Lock()
		if _, ok := fn.NewInputs[valTopic]; ok {
			fn.NewInputs[valTopic] = true
		}
		fn.NewInputsLock.Unlock()
	}
	log.Debug("Received val from", cl.DevID)
}

// Process messages for the Subscriber device-specific topic
func funsecSub(svr *mqtt.Server, cl *funsec.Client, wmsg *funsec.WMsg, topic string) {
	msg := wmsg.Msg.(funsec.Msg)
	switch msg.Action {

	case funsec.ActionSubFunRegStart:
		//body := msg.Body.(funsec.SubFunRegStart)
		// FUTURE TODO: Implement this
		log.Errorf("[S] SUB_FUN_REG actions not implemented yet")

	case funsec.ActionSubFunRegAbort:
		//body := msg.Body.(funsec.Abort)
		// FUTURE TODO: Implement this
		log.Errorf("[S]: SUB_FUN_REG actions not implemented yet")

	case funsec.ActionSubFunReqStart:
		body := msg.Body.(funsec.SubFunReqStart)
		if cfs.GetFn(body.FunID) == nil {
			log.Error("[S] FunctionID not found")
			publishAbort(svr, topic, msg.Token,
				funsec.ActionSubFunReqAbort, 1, "fun_id not found")
			return
		}
		wmsgArgs := &tp.WMsgArgs{
			WMsg:  *wmsg,
			Topic: topic,
		}
		var replyWMsg funsec.WMsg
		// TODO: Add timeout
		err := tpRPCClient.Call("ClientManager.SubFunReqStart", wmsgArgs, &replyWMsg)
		if err != nil {
			log.Error("[S] TP error:", err)
			publishAbort(svr, topic, msg.Token,
				funsec.ActionPubValRegAbort, 1, err.Error())
			return
		}
		if cfs.RegSubToFun(cl.DevID, body.FunID) == nil {
			log.Error("[S] FunctionID not found")
			publishAbort(svr, topic, msg.Token,
				funsec.ActionSubFunReqAbort, 1, "fun_id not found")
			return
		}
		log.Infof("[S] Registered Subscriber %v for function %v", cl.DevID, body.FunID)
		err = replyWMsg.ServerPublish(svr, topic)
		if err != nil {
			log.Error("[S] Publish error:", err)
			publishAbort(svr, topic, msg.Token,
				funsec.ActionSubFunReqAbort, 1, err.Error())
		}

	case funsec.ActionSubFunReqAbort:
		//body := msg.Body.(funsec.Abort)
		// TODO: Implement this
		log.Errorf("[S] SUB_FUN_REQ_ABORT not implemented yet")

	default:
		log.Errorf("[S] invalid action: %d", msg.Action)
	}
}

func functionLoop(svr *mqtt.Server, fun *funsec.Function, t int) {
	d := fmt.Sprintf("[F:%s]", fun.ID)
	var stat Statistic
Loop:
	for {
		time.Sleep(time.Duration(t) * time.Second)
		// Check that all function inputs are new
		fun.NewInputsLock.RLock()
		for _, n := range fun.NewInputs {
			if !n {
				fun.NewInputsLock.RUnlock()
				continue Loop
			}
		}
		fun.NewInputsLock.RUnlock()

		log.Debug(d, "New inputs")
		// Mark the function inputs as not new
		pubVals := make(map[funsec.ValTopic]*funsec.Value)
		inputTs := make(map[funsec.ValTopic]uint64)

		_vts := make([]string, 0, len(fun.Inputs))
		fun.NewInputsLock.Lock()
		for _vt, _ := range fun.NewInputs {
			//fmt.Println(_vt)
			_vts = append(_vts, _vt)
			fun.NewInputs[_vt] = false
		}
		fun.NewInputsLock.Unlock()

		for _, _vt := range _vts {
			vt := funsec.ValTopic(_vt)
			cl := cfs.GetClByValTopic(vt)
			if cl == nil {
				log.Error(d, "Can't get client of value topic", vt)
				continue Loop
			}
			pubVals[vt] = cl.Pub.Val
			inputTs[vt] = pubVals[vt].T
			//fmt.Printf(">>> Received vals\n>>> %v: %v\n", vt, pubVals[vt])
		}

		if funSerial {
			funMutex.Lock()
		}

		funArgs := tp.FunArgs{
			ID:      fun.ID,
			InputTs: inputTs,
		}
		var garbleReply cir.CircuitData
		log.Debug(d, "Waiting for garbled circuit...")

		start := time.Now()
		err := tpRPCClient.Call("FunctionManager.Garble", funArgs, &garbleReply)
		stat.TimeSendGarbled = time.Since(start).Nanoseconds()

		if err != nil {
			log.Error(d, "TP error:", err)
			if funSerial {
				funMutex.Unlock()
			}
			continue Loop
		}
		log.Debug(d, "Received garbled circuit")
		//log.Debugf("%v Received garbled circuit: %+v", d, garbleReply)

		pubLabels := make([]cir.Label, garbleReply.InputCount)
		for vt, ws := range garbleReply.InputWires {
			for i, w := range ws {
				pubLabels[w] = pubVals[funsec.ValTopic(vt)].V[i]
			}
		}

		start = time.Now()
		inputLabels := garbleReply.DecryptInputLabels(pubLabels)
		stat.TimeDecryptInputs = time.Since(start).Nanoseconds()

		start = time.Now()
		maskedOut, err := garbleReply.Gc.Eval(inputLabels)
		stat.TimeEval = time.Since(start).Nanoseconds()

		if funSerial {
			funMutex.Unlock()
		}

		stat.Inputs = garbleReply.InputCount
		stat.Outputs = garbleReply.OutputLen
		stat.TimeGarble = garbleReply.TimeGarble
		stat.TimeEncryptInputs = garbleReply.TimeEncryptInputs
		stat.NGates = garbleReply.Gc.GetNGates()
		stat.NXors = garbleReply.Gc.GetNXors()
		stat.NAnds = garbleReply.Gc.GetNAnds()
		stat.NOrs = garbleReply.Gc.GetNOrs()
		stat.NNots = garbleReply.Gc.GetNNots()
		stat.SizeGarbledCircuit = garbleReply.Size()
		statsChan <- stat

		log.Debug(d, "Evaluated garbled circuit")
		garbleReply.Gc.Delete()
		if err != nil {
			log.Error(d, "Garbled circuit evaluation error:", err)
			continue Loop
		}

		wmsg := funsec.WMsg{
			From: myDevID,
			Msg: funsec.MsgFun{
				T:  garbleReply.T,
				Ts: time.Now().Unix(),
				V:  funsec.Bin(maskedOut),
			},
		}
		wmsg.Sign(fun.Topic, &sig.Sk)
		if err := wmsg.ServerPublish(svr, fun.Topic); err != nil {
			log.Error(d, "Error publishing function output message:", err)
		}
	}
}

type args struct {
	Verbose     bool   `arg:"-v,help:Verbose output"`
	VerboseMQTT bool   `arg:"-V,help:Verbose output at MQTT level"`
	PubKey      string `arg:"-P,help:Public key file"`
	SecKey      string `arg:"-k,help:Private key file"`
	DataBase    string `arg:"-d,help:DataBase file"`
	TPHost      string `arg:"-h,help:Third Party RPC hostname"`
	TPPort      int    `arg:"-p,help:Third Party RPC port"`
	IP          string `arg:"-l,help:MQTT IP to listen at"`
	Port        int    `arg:"-o,help:MQTT port to listen at"`
	Time        int    `arg:"-t,help:Timeout to check for new function values"`
}

func (args) Version() string {
	return fmt.Sprintf("%s broker %s", funsec.ProtName, funsec.Version)
}

func (args) Description() string {
	return fmt.Sprintf("%s broker -- MQTT Broker for %s %s",
		funsec.ProtName, funsec.ProtName, funsec.ProtVer)
}

func main() {
	var args args
	args.Verbose = false
	args.VerboseMQTT = false
	args.PubKey = funsec.DefaultBrokerPubKeyFile
	args.SecKey = funsec.DefaultBrokerSecKeyFile
	args.DataBase = funsec.DefaultBrokerDataBaseFile
	args.TPHost = funsec.DefaultTPRPCHost
	args.TPPort = funsec.DefaultTPRPCListenPort
	args.IP = funsec.DefaultMQTTListenIP
	args.Port = funsec.DefaultMQTTListenPort
	args.Time = 10
	arg.MustParse(&args)

	// Log levels:
	// Notice for one time events
	// Info for infrequent events related to events triggered by clients / functions
	// Debug for frequent events related to events triggered by clients / functions
	logBackend := logging.NewLogBackend(os.Stderr, "", 0)
	logging.SetBackend(logBackend)
	logging.SetFormatter(logFormat)
	if args.Verbose {
		logging.SetLevel(logging.DEBUG, "broker")
	} else {
		logging.SetLevel(logging.INFO, "broker")
	}
	if args.VerboseMQTT {
		mqtt.SetLogLevel(logging.DEBUG)
	} else {
		mqtt.SetLogLevel(logging.INFO)
	}

	log.Notice("Starting Broker...")

	cfs = funsec.NewClientsFuncs()

	pubs, err := ioutil.ReadFile("publishers.txt")
	if err != nil {
		log.Critical("Error reading publishers.txt")
		return
	}
	for _, pub := range strings.Split(string(pubs), "\n") {
		if pub == "" {
			continue
		}
		log.Debug("Adding publisher", pub)
		if !cfs.AddCl(funsec.DeviceID(pub), true, false) {
			log.Critical("Error adding client", pub)
			return
		}
	}
	subs, err := ioutil.ReadFile("subscribers.txt")
	if err != nil {
		log.Critical("Error reading subscribers.txt")
		return
	}
	for _, sub := range strings.Split(string(subs), "\n") {
		if sub == "" {
			continue
		}
		log.Debug("Adding subscriber", sub)
		if !cfs.AddCl(funsec.DeviceID(sub), false, true) {
			log.Critical("Error adding client", sub)
			return
		}
	}

	prelude, err := ioutil.ReadFile("functions/prelude.lsp")
	if err != nil {
		log.Critical("Error reading file", "functions/prelude.lsp")
		return
	}

	funIDs := make([]funsec.FunctionID, 0)
	files, err := ioutil.ReadDir("functions/autoload")
	if err != nil {
		log.Critical("Error reading directory functions/autoload")
		return
	}
	for _, file := range files {
		funDesc, err := ioutil.ReadFile("functions/autoload/" + file.Name())
		if err != nil {
			log.Critical("Error reading file", file.Name())
			return
		}
		funID, err := cfs.AddFun(funsec.FunDesc(prelude), funsec.FunDesc(funDesc))
		if err != nil {
			log.Critical("Error adding function", file.Name())
			return
		}
		funIDs = append(funIDs, funID)
		log.Info("Added function", funID)
		// The Broker doesn't need the circuit
		cfs.GetFn(funID).Data.Gc.Delete()
	}

	sig, err = funsec.LoadSigKeys(args.PubKey, args.SecKey)
	if err != nil {
		log.Critical("Unable to load keys:", err)
		return
	}
	myDevID = funsec.DeviceID(funsec.Base64URLEncode(sig.Pk[:]))

	go statsWriter("broker_stats.csv")
	// NOTE: Disable this in production
	funSerial = true
	if funSerial {
		log.Notice("Serializing functions to collect controlled statistics")
	}

	tp.GobRegister()
	l, err := net.Listen("tcp",
		fmt.Sprintf("%s:%d", args.IP, args.Port))
	if err != nil {
		log.Fatal("listen: ", err)
		return
	}
	svr := mqtt.NewServerFilter(l, funSecFilter)
	svr.Start()

	// TODO: Reconnect on failure after timeout
	tpRPCClient, err = rpc.Dial("tcp",
		fmt.Sprintf("%s:%d", args.TPHost, args.TPPort))
	if err != nil {
		log.Fatal("dialing:", err)
	}
	log.Notice("Connected to Third Party RPC")

	os.Create("tmp/broker.lock")
	for _, funID := range funIDs {
		go functionLoop(svr, cfs.GetFn(funID), args.Time)
	}

	<-svr.Done

}
