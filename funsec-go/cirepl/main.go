package main

import (
	cir "../circuit"
	"../funsec"
	"bufio"
	"bytes"
	"encoding/gob"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	//cir.Fixed32Test()
	//cir.GarbleTest()
	//return
	var ienv cir.Env
	ienv.Init()
	if len(os.Args) > 1 {
		var res cir.Scmer
		for _, file := range os.Args[1:] {
			input, err := ioutil.ReadFile(file)
			if err != nil {
				fmt.Println("Error reading file", os.Args[1])
				return
			}
			tokens := cir.Tokenize(string(input))
			for len(tokens) > 0 {
				expr := cir.ReadTokens(&tokens)
				if expr == nil {
					continue
				}
				if _, ok := expr.(error); ok {
					fmt.Println("!=>", expr)
					return
				}
				res = ienv.EvalOrErr(expr)
				if _, ok := res.(error); ok {
					fmt.Println("!=>", res)
					return
				} else {
					fmt.Println("==>", cir.String(res))
				}
			}
		}
		_, ok := res.([]cir.Wire)
		if ok {
			fmt.Println("Finishing circuit with empty mask")
			cleanCir := ienv.C
			c := cleanCir.CopyCircuit()
			//c := ienv.C
			//cir.Mask()

			// Get some random publisher values
			vals0 := make(map[string]cir.Fixed32)
			vals1 := make(map[string]cir.Fixed32)
			i := 0
			for k, _ := range c.InputWires {
				vals0[k] = someFixed32[i%len(someFixed32)]
				vals1[k] = someFixed32[(i+1)%len(someFixed32)]
				i++
			}
			GarbleEvalTest(c, vals0)
			c = cleanCir.CopyCircuit()
			GarbleEvalTest(c, vals1)
		}
	} else {
		Repl(&ienv)
	}
}

func GarbleTest(c *cir.CircuitData) {
	c.Garble()
	//fmt.Println("OK Garble")
}

var someFixed32 = []cir.Fixed32{
	cir.NewFixed32(2.3),
	cir.NewFixed32(0.5),
	//cir.NewFixed32(12.34),
	//cir.NewFixed32(-9.12),
	//cir.NewFixed32(56.78),
	//cir.NewFixed32(-34.56),
	//cir.NewFixed32(78.91),
	//cir.NewFixed32(-23.45),
}

func GarbleEvalTest(c *cir.CircuitData, vals map[string]cir.Fixed32) {
	//// Publishers
	// Generate some random publisher ratchet keys
	pubRks := make(map[string]*funsec.RatchetKey)
	for k, _ := range c.InputWires {
		rk := funsec.NewRatchetKey()
		pubRks[k] = &rk
	}

	for k, v := range vals {
		fmt.Printf("%s = %v\n", k, v)
	}
	_pubLabels := make(map[string][]cir.Label)
	for k, v := range vals {
		bVal := v.Bin()
		labelMap := pubRks[k].GenLabelMap(32)
		labels := cir.MapLabels(labelMap, bVal)
		_pubLabels[k] = labels
	}

	// Generate a random function ratchet key
	funRk := funsec.NewRatchetKey()

	//// Third Party
	if err := c.Mask(funRk.GenMask(c.OutputLen)); err != nil {
		fmt.Println("Error masking circuit:", err)
		return
	}
	if err := c.Finish(); err != nil {
		fmt.Println("Error finishing circuit:", err)
		return
	}
	c.Garble()
	//fmt.Println("OK Garble")
	labelMaps := make(map[string][]cir.Label)
	for k, ws := range c.InputWires {
		labelMaps[k] = pubRks[k].GenLabelMap(uint64(len(ws)))
	}
	c.EncryptInputMaps(labelMaps)

	//gob.Register(cir.CircuitData{})
	//gob.Register(cir.Wire{})
	//gob.Register(funsec.Label{})

	//// Network Marshaling
	var network bytes.Buffer
	enc := gob.NewEncoder(&network)
	err := enc.Encode(c)
	if err != nil {
		log.Fatal("encode error:", err)
	}
	//fmt.Println("Marshal")
	//fmt.Printf("%+v\n", c)
	c.Gc.Delete()

	//// Network Unmarshaling
	dec := gob.NewDecoder(&network)
	c1 := cir.NewCircuitData()
	err = dec.Decode(c1)
	if err != nil {
		log.Fatal("decode error 1:", err)
	}
	//fmt.Println("Unmarshal")
	//fmt.Printf("%+v\n", c1)

	//// Broker

	pubLabels := make([]cir.Label, c1.InputCount)
	for k, ws := range c1.InputWires {
		for i, w := range ws {
			pubLabels[w] = _pubLabels[k][i]
		}
	}
	fmt.Println("len(pubLabels) =", len(pubLabels))
	inputLabels := c1.DecryptInputLabels(pubLabels)
	//fmt.Println("inputLabels =", inputLabels)
	//input := make([]bool, 0, len(vals)*32)
	//for _, v := range bVals {
	//	input = append(input, v...)
	//}
	//fmt.Println("len(inputMap) =", len(c.GetInputMap()), ", InputCount =", c.InputCount)
	//inputLabels := cir.MapLabels(c.GetInputMap(), input)
	//fmt.Println("len(inputLabels) =", len(inputLabels))
	maskedOut, err := c1.Gc.Eval(inputLabels)
	c1.Gc.Delete()
	if err != nil {
		fmt.Println(err)
		return
	}

	//// Subscriber
	byteMask := funRk.Cprng(uint64(len(maskedOut)), 0)
	mask := cir.Bytes2Bin(byteMask, uint64(len(maskedOut)))

	bOut := cir.BinXor(maskedOut, mask)
	fmt.Println("out =", bOut)

	var out cir.Fixed32
	if len(bOut) == 1 {
		if bOut[0] {
			out = cir.NewFixed32(1)
		}
	} else {
		out, err = cir.NewFixed32FromBin(bOut)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	fmt.Println("out =", out, "\n")
	//fmt.Println("OK Eval")
}

func ParseEval(ienv *cir.Env, input string) (res cir.Scmer) {
	expr := cir.Read(input)
	if _, ok := expr.(error); ok {
		return expr
	}
	return ienv.EvalOrErr(expr)
}

func Repl(ienv *cir.Env) {
	scanner := bufio.NewScanner(os.Stdin)
	for fmt.Print("> "); scanner.Scan(); fmt.Print("> ") {
		input := scanner.Text()
		res := ParseEval(ienv, input)
		if _, ok := res.(error); ok {
			fmt.Println("!=>", res)
		} else {
			fmt.Println("==>", cir.String(res))
		}
	}
}
