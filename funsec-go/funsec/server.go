package funsec

// #include "../../funsec-c/keys.h"
// #include "../../funsec-c/client.h"
import "C"

import (
	cir "../circuit"
	"crypto/rand"
	"fmt"
	"golang.org/x/crypto/sha3"
	"sync"
)

const (
	DevIDLen       int = C.ID_LEN
	CliIDLen       int = C.MQTT_ID_LEN
	ValTopicMaxLen int = C.SUBTOPIC_MAX_LEN
)

const (
	DefaultBrokerPubKeyFile   string = "broker_key.pub.json"
	DefaultBrokerSecKeyFile   string = "broker_key.json"
	DefaultTPPubKeyFile       string = "tp_key.pub.json"
	DefaultTPSecKeyFile       string = "tp_key.json"
	DefaultTPRPCListenIP      string = "127.0.0.1"
	DefaultTPRPCListenPort    int    = 7070
	DefaultMQTTListenIP       string = "127.0.0.1"
	DefaultMQTTHost           string = "127.0.0.1"
	DefaultMQTTListenPort     int    = 1883
	DefaultTPRPCHost          string = "127.0.0.1"
	DefaultBrokerDataBaseFile string = "broker.db"
	DefaultTPDataBaseFile     string = "third_party.db"
)

type ClientID string
type DeviceID string
type ValTopic string
type FunctionID string

// TODO: Define a datastructure to store a function
type FunDesc string

func (fd *FunDesc) GetFunctionID() FunctionID {
	h := make([]byte, 32)
	sha3.ShakeSum256(h, []byte(*fd))
	hB64 := Base64URLEncode(h)
	return FunctionID(hB64)
}

// TODO: Define a datastructure to store the circuit of a function
type Circuit string

// TODO: Define a datastructure to store the garbled circuit of a function
//type GarbledCircuit string

type ValDesc struct {
	Topic ValTopic
	Len   uint
	// Min and Max are used to define a range of the value in order to
	// compress it into a fixed length integer of Len bits
	Min float32
	Max float32
}

type Publisher struct {
	ValTopic ValTopic
	ValLen   uint
	// DB: Bucket with Values indexed by round T
	//Vals     []*Value   // B only // *changes often*
	Val *Value      // B only // Let's keep only one value for now
	Rkp *RatchetKey // TP only // *changes often*
	// TODO: Store past Rkp's to generate circuits with old values
	rw sync.RWMutex
}

func (pub *Publisher) SetRkp(rkp *RatchetKey) {
	pub.rw.Lock()
	defer pub.rw.Unlock()
	pub.Rkp = rkp
}

func (pub *Publisher) SetVal(val *Value) {
	//pub.rw.Lock()
	//defer pub.rw.Unlock()
	pub.Val = val
}

type Subscriber struct {
	FunIDs map[FunctionID]bool // *changes*
	rw     sync.RWMutex
}

type Value struct {
	T     uint64
	PubTs int64
	Ts    int64
	V     []cir.Label
}

//type FunctionData struct {
//	Circ  *Circuit        // TP only
//	GCirc *GarbledCircuit // *changes often* // TP generates it and sends it to Broker
//}

type Function struct {
	ID            FunctionID
	Topic         string
	Inputs        []ValTopic
	NewInputs     map[string]bool
	NewInputsLock sync.RWMutex
	Subs          map[DeviceID]bool // // Inferred from Client.Sub.FunsIDs, Client.DeviceID
	Rkf           *RatchetKey       // TP only // *changes often*
	Desc          FunDesc
	Data          *cir.CircuitData
	rw            sync.RWMutex
}

func (fun *Function) SetRkf(rkf *RatchetKey) {
	fun.rw.Lock()
	defer fun.rw.Unlock()
	fun.Rkf = rkf
}

func (fun *Function) GetRkf() *RatchetKey {
	fun.rw.RLock()
	defer fun.rw.RUnlock()
	return fun.Rkf
}

type Server struct {
	Pk    SignPk
	DevID DeviceID
}

type Client struct {
	CliID       ClientID
	DevID       DeviceID
	Pk          SignPk
	IsPub       bool
	IsSub       bool
	Pub         *Publisher // Needs locking
	Sub         Subscriber
	PubDevTopic string
	SubDevTopic string
	Broker      *Server
	ThirdParty  *Server
	rw          sync.RWMutex
}

func (cl *Client) GetPub() *Publisher {
	cl.rw.RLock()
	defer cl.rw.RUnlock()
	return cl.Pub
}

func NewClient(devID DeviceID, isPub, isSub bool) *Client {
	if len(devID) != DevIDLen {
		return nil
	}
	cliID := ClientID(devID[:CliIDLen])
	var pk SignPk
	pk_buf, err := Base64URLDecode(string(devID), SignPkLen)
	if err != nil {
		return nil
	}
	copy(pk[:], pk_buf)
	cl := Client{
		CliID: cliID,
		DevID: devID,
		Pk:    pk,
		IsPub: isPub,
		IsSub: isSub,
		Pub:   nil,
	}
	if isPub {
		cl.PubDevTopic = fmt.Sprintf("%spub/%s", TopicPrefix, devID)
	}
	if isSub {
		cl.SubDevTopic = fmt.Sprintf("%ssub/%s", TopicPrefix, devID)
		cl.Sub.FunIDs = make(map[FunctionID]bool, 0)
	}

	return &cl
}

// TODO: Write operations on ClientFuncs should reflect those changes to disk!
type ClientsFuncs struct {
	cls          map[DeviceID]*Client
	clByValTopic map[ValTopic]DeviceID // Inferred from Client.Pub.ValTopic, Client.DeviceID
	fns          map[FunctionID]*Function
	rw           sync.RWMutex
}

func NewClientsFuncs() *ClientsFuncs {
	cfs := ClientsFuncs{
		cls:          make(map[DeviceID]*Client),
		clByValTopic: make(map[ValTopic]DeviceID),
		fns:          make(map[FunctionID]*Function),
	}
	return &cfs
}

func (cfs *ClientsFuncs) GetFns() *map[FunctionID]*Function {
	return &cfs.fns
}

// Get Client by ValTopic.  If ValTopic is not in the database, return nil
func (cfs *ClientsFuncs) GetClByValTopic(vt ValTopic) *Client {
	cfs.rw.RLock()
	defer cfs.rw.RUnlock()
	devID, ok := cfs.clByValTopic[vt]
	if ok {
		return cfs.cls[devID]
	} else {
		return nil
	}
}

// Get Client by DeviceID.  If DeviceID is not in the database, return nil
func (cfs *ClientsFuncs) GetCl(devID DeviceID) *Client {
	cfs.rw.RLock()
	defer cfs.rw.RUnlock()
	return cfs.cls[devID]
}

// Add DeviceID to the Clients database, return true on success
func (cfs *ClientsFuncs) AddCl(devID DeviceID, isPub, isSub bool) bool {
	cl := NewClient(devID, isPub, isSub)
	if cl == nil {
		return false
	}
	cfs.rw.Lock()
	defer cfs.rw.Unlock()

	if _, ok := cfs.cls[devID]; ok {
		return false
	}
	cfs.cls[devID] = cl
	return true
}

// Delete DeviceID from the Clients database, return true on success
func (cfs *ClientsFuncs) DelCl(devID DeviceID) bool {
	cfs.rw.Lock()
	defer cfs.rw.Unlock()
	cl, ok := cfs.cls[devID]
	if !ok {
		return false
	}
	if cl.IsPub && cl.Pub != nil {
		delete(cfs.clByValTopic, cl.Pub.ValTopic)
	}
	if cl.IsSub {
		for funID, _ := range cl.Sub.FunIDs {
			delete(cfs.fns[funID].Subs, cl.DevID)
		}
	}
	delete(cfs.cls, devID)
	return true
}

// Add Function to the Functions database, return FuncionID or error on failure
func (cfs *ClientsFuncs) AddFun(prelude, desc FunDesc) (FunctionID, error) {

	//funID, err := NewFunctionID()
	//if err != nil {
	//	return "", err
	//}
	funID := desc.GetFunctionID()
	// Checking early to avoid wasting the cost of function parsing
	cfs.rw.RLock()
	_, ok := cfs.fns[funID]
	cfs.rw.RUnlock()
	if ok {
		return "", fmt.Errorf("FunctionID already registered")
	}

	cd, err := cir.GenCircuit(string(prelude), string(desc))
	if err != nil {
		return "", err
	}
	inputs := make([]ValTopic, 0, len(cd.InputWires))
	newInputs := make(map[string]bool)
	for k, _ := range cd.InputWires {
		inputs = append(inputs, ValTopic(k))
		newInputs[k] = false
	}

	rkf := NewRatchetKey()
	fn := Function{
		ID:        funID,
		Topic:     fmt.Sprintf("%s%s", SubFunTopic, funID),
		Inputs:    inputs,
		NewInputs: newInputs,
		Subs:      make(map[DeviceID]bool),
		Rkf:       &rkf,
		Desc:      desc,
		Data:      cd,
	}

	cfs.rw.Lock()
	defer cfs.rw.Unlock()

	if _, ok := cfs.fns[funID]; ok {
		return "", fmt.Errorf("FunctionID already registered")
	}
	cfs.fns[funID] = &fn
	return funID, nil
}

// Delete FunctionID from the Functions database, return true on success
func (cfs *ClientsFuncs) DelFn(funID FunctionID) bool {
	cfs.rw.Lock()
	defer cfs.rw.Unlock()
	fn, ok := cfs.fns[funID]
	if !ok {
		return false
	}
	for devID, _ := range fn.Subs {
		cl, ok := cfs.cls[devID]
		if !ok {
			continue
		}
		cl.Sub.rw.Lock()
		delete(cl.Sub.FunIDs, funID)
		cl.Sub.rw.Unlock()
	}
	delete(cfs.fns, funID)
	return true
}

// Get Function by FunctionID.  If FunctionID is not in the database, return nil
func (cfs *ClientsFuncs) GetFn(funID FunctionID) *Function {
	cfs.rw.RLock()
	defer cfs.rw.RUnlock()
	return cfs.fns[funID]
}

// Initializes the Client.Pub datastructure with the valTopic data
// TODO: If the client had a topic already, remove it
func (cfs *ClientsFuncs) InitPub(devID DeviceID, valTopic ValTopic, valLen uint) *Publisher {
	cfs.rw.Lock()
	defer cfs.rw.Unlock()
	if cfs.clByValTopic[valTopic] != "" && cfs.clByValTopic[valTopic] != devID {
		// valTopic is already registered by someone else
		return nil
	}
	cfs.clByValTopic[valTopic] = devID
	cl := cfs.cls[devID]

	pub := Publisher{
		ValTopic: valTopic,
		ValLen:   valLen,
	}
	cl.Pub = &pub
	return &pub
}

// Frees the Client.Pub datastructure
func (cfs *ClientsFuncs) FreePub(devID DeviceID) bool {
	cfs.rw.Lock()
	defer cfs.rw.Unlock()
	cl := cfs.cls[devID]
	if cl == nil {
		// devID doesn't exist in the DB
		cfs.rw.Unlock()
		return false
	}
	delete(cfs.clByValTopic, cl.Pub.ValTopic)
	cl.Pub = nil
	return true
}

// Registers a Subscriber to an existing function and returns the function, or
// nil if the FunctionID is not registered
func (cfs *ClientsFuncs) RegSubToFun(devID DeviceID, funID FunctionID) *Function {
	cfs.rw.RLock()
	defer cfs.rw.RUnlock()
	fn, ok := cfs.fns[funID]
	if !ok {
		return nil
	}
	cl, ok := cfs.cls[devID]
	if !ok {
		return nil
	}

	cl.Sub.rw.Lock()
	cl.Sub.FunIDs[funID] = true
	cl.Sub.rw.Unlock()

	fn.rw.Lock()
	defer fn.rw.Unlock()
	fn.Subs[devID] = true
	return fn
}

func NewFunctionID() (FunctionID, error) {
	buf := make([]byte, 15)
	_, err := rand.Read(buf)
	if err != nil {
		return "", err
	}
	buf_b64 := Base64URLEncode(buf)
	return FunctionID(buf_b64), nil
}

// Del
// Not thread safe

/// Client
// PubInit

/// Pub
// Advance ratchet
// Add Value

/// Sub
// Add Fun

/// Function
// Add Sub
// Advance ratchet
