package funsec

// #cgo LDFLAGS: -lsodium
// #include <sodium.h>
// #include "../../funsec-c/protocol.h"
// #include "../../funsec-c/keys.h"
// const char *_RK_ADV_CTX = RK_ADV_CTX;
// const char *_RK_GEN_SEED_CTX = RK_GEN_SEED_CTX;
import "C"

import (
	cir "../circuit"
	crand "crypto/rand"
	"fmt"
	"unsafe"
)

const (
	SignPkLen    int = C.crypto_sign_PUBLICKEYBYTES
	SignSkLen    int = C.crypto_sign_SECRETKEYBYTES
	SignatureLen int = C.crypto_sign_BYTES
	KxPkLen      int = C.crypto_sign_PUBLICKEYBYTES
	KxSkLen      int = C.crypto_sign_SECRETKEYBYTES
	KxKeyLen     int = C.crypto_kx_SESSIONKEYBYTES
	RkLen        int = C.crypto_kdf_KEYBYTES
	SeedLen      int = C.randombytes_SEEDBYTES
	//SymKeyLen    int = C.crypto_secretbox_KEYBYTES
	//NonceLen     int = C.crypto_secretbox_NONCEBYTES
	//MacLen       int = C.crypto_secretbox_MACBYTES
	SymKeyLen    int = C.crypto_aead_chacha20poly1305_KEYBYTES
	NonceLen     int = C.crypto_aead_chacha20poly1305_NPUBBYTES
	MacLen       int = C.crypto_aead_chacha20poly1305_ABYTES
	SeedIDDelta  int = C.SEED_ID_DELTA
	SeedIDLabels int = C.SEED_ID_LABELS
)

type SignPk [SignPkLen]byte
type SignSk [SignSkLen]byte
type Signature [SignatureLen]byte
type Nonce [NonceLen]byte

func NewNonce() *Nonce {
	var nonce Nonce
	_, err := crand.Read(nonce[:])
	if err != nil {
		panic("crand error")
	}
	return &nonce
}

func (n Nonce) MarshalText() ([]byte, error) {
	return []byte(Base64URLEncode(n[:])), nil
}

func (n *Nonce) UnmarshalText(s []byte) error {
	buf, err := Base64URLDecode(string(s), NonceLen)
	if err != nil {
		return err
	}
	copy(n[:], buf)
	return nil
}

type BinBuf []byte

func (b BinBuf) MarshalText() ([]byte, error) {
	return []byte(Base64URLEncode(b)), nil
}

func (b *BinBuf) UnmarshalText(s []byte) error {
	buf, err := Base64URLDecode(string(s), 0)
	if err != nil {
		fmt.Printf("%s\n", string(s))
		return err
	}
	*b = BinBuf(buf)
	return nil
}

type SymKey [SymKeyLen]byte

func NewSymKeyKxClient(kk *KxKey, serverPk *KxPk) (key SymKey, err error) {
	var sharedKey [KxKeyLen]byte
	e := int(C.crypto_kx_client_session_keys(
		(*C.uchar)(&sharedKey[0]),
		nil,
		(*C.uchar)(&kk.Pk[0]),
		(*C.uchar)(&kk.Sk[0]),
		(*C.uchar)(&serverPk[0])))
	if e != 0 {
		return key, fmt.Errorf("Invalid kx_pk")
	}
	copy(key[:], sharedKey[:])
	return key, nil
}

func NewSymKeyKxServer(kk *KxKey, clientPk *KxPk) (key SymKey, err error) {
	var sharedKey [KxKeyLen]byte
	e := int(C.crypto_kx_server_session_keys(
		(*C.uchar)(&sharedKey[0]),
		nil,
		(*C.uchar)(&kk.Pk[0]),
		(*C.uchar)(&kk.Sk[0]),
		(*C.uchar)(&clientPk[0])))
	if e != 0 {
		return key, fmt.Errorf("Invalid kx_pk")
	}
	copy(key[:], sharedKey[:])
	return key, nil
}

func (k *SymKey) Encrypt(msg []byte) (encMsg BinBuf, nonce *Nonce) {
	encMsg = make([]byte, MacLen+len(msg))
	nonce = NewNonce()
	//C.crypto_secretbox_easy(
	//	(*C.uchar)(&encMsg[0]),
	//	(*C.uchar)(&msg[0]),
	//	C.ulonglong(len(msg)),
	//	(*C.uchar)(&nonce[0]),
	//	(*C.uchar)(&k[0]))
	C.crypto_aead_chacha20poly1305_encrypt(
		(*C.uchar)(&encMsg[0]),
		nil,
		(*C.uchar)(&msg[0]),
		C.ulonglong(len(msg)),
		nil, 0,
		nil,
		(*C.uchar)(&nonce[0]),
		(*C.uchar)(&k[0]))
	return encMsg, nonce
}

func (k *SymKey) Decrypt(encMsg BinBuf, nonce Nonce) (msg []byte, err error) {
	if len(encMsg) < MacLen {
		return nil, fmt.Errorf("Encrypted message is too short")
	}
	msg = make([]byte, len(encMsg)-MacLen)
	//e := int(C.crypto_secretbox_open_easy(
	//	(*C.uchar)(&msg[0]),
	//	(*C.uchar)(&encMsg[0]),
	//	C.ulonglong(len(encMsg)),
	//	(*C.uchar)(&nonce[0]),
	//	(*C.uchar)(&k[0])))
	e := int(C.crypto_aead_chacha20poly1305_decrypt(
		(*C.uchar)(&msg[0]),
		nil,
		nil,
		(*C.uchar)(&encMsg[0]),
		C.ulonglong(len(encMsg)),
		nil, 0,
		(*C.uchar)(&nonce[0]),
		(*C.uchar)(&k[0])))
	if e != 0 {
		return nil, fmt.Errorf("Verification failed")
	}
	return msg, nil
}

type SignKey struct {
	Pk SignPk
	Sk SignSk
}

func NewSignKey() (sig SignKey) {
	C.crypto_sign_keypair(
		(*C.uchar)(&sig.Pk[0]),
		(*C.uchar)(&sig.Sk[0]))
	return sig
}

type KxPk [KxPkLen]byte

func (k KxPk) MarshalText() ([]byte, error) {
	return []byte(Base64URLEncode(k[:])), nil
}

func (k *KxPk) UnmarshalText(s []byte) error {
	buf, err := Base64URLDecode(string(s), KxPkLen)
	if err != nil {
		return err
	}
	copy(k[:], buf)
	return nil
}

type KxSk [KxSkLen]byte

type KxKey struct {
	Pk KxPk
	Sk KxSk
}

func NewKxKeyPair() (kk KxKey) {
	C.crypto_kx_keypair(
		(*C.uchar)(&kk.Pk[0]),
		(*C.uchar)(&kk.Sk[0]))
	return kk
}

type RkKey [RkLen]byte

func (rkk RkKey) MarshalText() ([]byte, error) {
	return []byte(Base64URLEncode(rkk[:])), nil
}

func (rkk *RkKey) UnmarshalText(s []byte) error {
	buf, err := Base64URLDecode(string(s), RkLen)
	if err != nil {
		return err
	}
	copy(rkk[:], buf)
	return nil
}

type RatchetKey struct {
	Key RkKey
	T   uint64
}

func NewRatchetKeyKxServer(kk *KxKey, clientPk *KxPk) (rk RatchetKey, err error) {
	var sharedKey [KxKeyLen]byte
	e := int(C.crypto_kx_server_session_keys(
		(*C.uchar)(&sharedKey[0]),
		nil,
		(*C.uchar)(&kk.Pk[0]),
		(*C.uchar)(&kk.Sk[0]),
		(*C.uchar)(&clientPk[0])))
	if e != 0 {
		return rk, fmt.Errorf("Invalid kx_pk")
	}
	copy(rk.Key[:], sharedKey[:])
	rk.T = 0
	return rk, nil

}

func NewRatchetKey() (rk RatchetKey) {
	C.crypto_kdf_keygen((*C.uchar)(&rk.Key[0]))
	rk.T = 0
	return rk
}

func (rk *RatchetKey) Advance() *RatchetKey {
	var newRk RatchetKey
	C.crypto_kdf_derive_from_key(
		(*C.uchar)(&newRk.Key[0]),
		C.crypto_kdf_KEYBYTES,
		0,
		C._RK_ADV_CTX,
		(*C.uchar)(&rk.Key[0]))
	newRk.T = rk.T + 1
	return &newRk
}

func (rk *RatchetKey) GenSeed(id uint64) (seed [SeedLen]byte) {
	C.crypto_kdf_derive_from_key(
		(*C.uchar)(&seed[0]),
		C.randombytes_SEEDBYTES,
		C.uint64_t(id),
		C._RK_GEN_SEED_CTX,
		(*C.uchar)(&rk.Key[0]))
	return seed
}

func (rk *RatchetKey) Cprng(buf_len uint64, id uint64) (buf []byte) {
	buf = make([]byte, buf_len)
	seed := rk.GenSeed(id)
	C.randombytes_buf_deterministic(
		unsafe.Pointer(&buf[0]),
		C.size_t(buf_len),
		(*C.uchar)(&seed[0]))
	return buf
}

func (rk *RatchetKey) GenLabelMap(l uint64) []cir.Label {
	labelMap := make([]cir.Label, l*2)

	var delta cir.Label
	_delta := rk.Cprng(cir.LabelLen, uint64(SeedIDDelta))
	copy(delta[:], _delta)
	//fmt.Println(">>> delta:", delta)
	delta[0] |= 1

	zeroLabels := rk.Cprng(l*cir.LabelLen, uint64(SeedIDLabels))

	//fmt.Println(">>> GenLabelMap")
	for i := uint64(0); i < l; i++ {
		copy(labelMap[i*2][:], zeroLabels[i*cir.LabelLen:(i+1)*cir.LabelLen])
		labelMap[i*2+1] = labelMap[i*2].Xor(delta)
		//fmt.Printf(">>> %v %v\n", labelMap[i*2], labelMap[i*2+1])
	}

	return labelMap
}

func (rk *RatchetKey) GenMask(l uint64) []bool {
	byteMask := rk.Cprng(uint64(l/8+1), 0)
	mask := cir.Bytes2Bin(byteMask, l)
	return mask
}

func SignVerify(buf []byte, sig *Signature, pk *SignPk) error {
	if C.crypto_sign_verify_detached(
		(*C.uchar)(&sig[0]),
		(*C.uchar)(&buf[0]),
		C.ulonglong(len(buf)),
		(*C.uchar)(&pk[0])) != 0 {
		return fmt.Errorf("Bad signature")
	}
	return nil
}

func Sign(buf []byte, sk *SignSk) *Signature {
	var sig Signature
	C.crypto_sign_detached(
		(*C.uchar)(&sig[0]),
		nil,
		(*C.uchar)(&buf[0]),
		C.ulonglong(len(buf)),
		(*C.uchar)(&sk[0]))
	return &sig
}
