package funsec

import (
	"fmt"
	"io/ioutil"
)

const (
	// DefaultSubscriberPubKeyFile is the default Subscriber public key file
	DefaultSubscriberPubKeyFile string = "subscriber_key.pub.json"
	// DefaultSubscriberSecKeyFile is the default Subscriber private key file
	DefaultSubscriberSecKeyFile string = "subscriber_key.json"
	// DefaultSubscriberConfFile is the default Subscriber configuration file
	DefaultSubscriberConfFile string = "subscriber_conf.json"
	// DefaultPublisherPubKeyFile is the default Publisher public key file
	DefaultPublisherPubKeyFile string = "publisher_key.pub.json"
	// DefaultPublisherSecKeyFile is the default Publisher private key file
	DefaultPublisherSecKeyFile string = "publisher_key.json"
	// DefaultPublisherConfFile is the default Publisher configuration file
	DefaultPublisherConfFile string = "publisher_conf.json"
	// DefaultSubscriberDataBase is the default Subscriber database file
	DefaultSubscriberDataBase string = "subscriber.db"
)

// LoadSubConf loads the Subscriber configuration from a file
func LoadSubConf(file string) ([]FunDesc, error) {
	m, err := loadJSONFileArray(file)
	if err != nil {
		return nil, err
	}
	fs, ok := (*m)["functions"]
	if !ok {
		return nil, fmt.Errorf("Key functions missing")
	}
	fds := make([]FunDesc, len(fs))
	for i, fd := range fs {
		input, err := ioutil.ReadFile(fd)
		if err != nil {
			fmt.Println("Error reading file", fd)
			return nil, err
		}
		fds[i] = FunDesc(string(input))
	}
	return fds, nil
}
