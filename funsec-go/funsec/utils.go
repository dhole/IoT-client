package funsec

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"
)

func loadJSONFileArray(file string) (*map[string][]string, error) {
	fileBuf, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	m := make(map[string][]string)
	if err := json.Unmarshal(fileBuf, &m); err != nil {
		return nil, err
	}
	return &m, nil
}

func loadJSONFile(file string) (*map[string]string, error) {
	fileBuf, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	m := make(map[string]string)
	if err := json.Unmarshal(fileBuf, &m); err != nil {
		return nil, err
	}
	return &m, nil
}

func getB64Key(m *map[string]string, key string, length int) ([]byte, error) {
	vB64, ok := (*m)[key]
	if !ok {
		return nil, fmt.Errorf("Key %s missing in map", key)
	}
	v, err := Base64URLDecode(vB64, length)
	if err != nil {
		return nil, err
	}
	return v, nil

}

// LoadSigKeys loads signature keypair from two files
func LoadSigKeys(pubKeyFile, secKeyFile string) (*SignKey, error) {
	pkm, err := loadJSONFile(pubKeyFile)
	if err != nil {
		return nil, err
	}
	pk, err := getB64Key(pkm, "pk", SignPkLen)
	if err != nil {
		return nil, err
	}

	skm, err := loadJSONFile(secKeyFile)
	if err != nil {
		return nil, err
	}
	sk, err := getB64Key(skm, "sk", SignSkLen)
	if err != nil {
		return nil, err
	}
	var sig SignKey
	copy(sig.Pk[:], pk)
	copy(sig.Sk[:], sk)
	return &sig, nil
}

// LoadPubKey loads a public key from a file
func LoadPubKey(pubKeyFile string, pk *SignPk) error {
	pkm, err := loadJSONFile(pubKeyFile)
	if err != nil {
		return err
	}
	pkBuf, err := getB64Key(pkm, "pk", SignPkLen)
	if err != nil {
		return err
	}
	copy(pk[:], pkBuf)
	return nil
}

// TimerSafeReset stops and resets a timer, draining its channel if necessary
func TimerSafeReset(t *time.Timer, d time.Duration) {
	if !t.Stop() {
		select {
		case <-t.C:
		default:
		}
	}
	t.Reset(d)
}
