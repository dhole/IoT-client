package funsec

// #include "../../funsec-c/protocol.h"
import "C"

import (
	"fmt"
)

const (
	ProtName string = C.PROT_NAME
	ProtVer  string = C.PROT_VER
	Version  string = "v0.0"
)

const (
	ActionPubValRegStart int = C.ACTION_PUB_VAL_REG_START
	ActionPubValRegEnd   int = C.ACTION_PUB_VAL_REG_END
	ActionPubValRegAbort int = C.ACTION_PUB_VAL_REG_ABORT
	// Subscriber registers a function
	ActionSubFunRegStart int = C.ACTION_SUB_FUN_REG_START
	ActionSubFunRegEnd   int = C.ACTION_SUB_FUN_REG_END
	ActionSubFunRegAbort int = C.ACTION_SUB_FUN_REG_ABORT
	// Subscriber requests the ratchet key used to mask a function
	ActionSubFunReqStart int = C.ACTION_SUB_FUN_REQ_START
	ActionSubFunReqEnd   int = C.ACTION_SUB_FUN_REQ_END
	ActionSubFunReqAbort int = C.ACTION_SUB_FUN_REQ_ABORT
	//
	//ACTION_MISSING,
	ActionUnknown int = C.ACTION_UNKNOWN
	// Dummy action to figure maximum value of of prot_actions
	ActionDummyLen int = C.ACTION_DUMMY_LEN
)

const (
	MsgTypeMsg    int = C.MSG_TYPE_MSG
	MsgTypeValMsg int = C.MSG_TYPE_VALMSG
	MsgTypeFunMsg int = C.MSG_TYPE_FUNMSG
)

const (
	KxStateStart    int = iota
	KxStateSentPk   int = iota
	KxStateFinalize int = iota
	KxStateFinished int = iota
)

var TopicPrefix string = fmt.Sprintf("%s/%s/", ProtName, ProtVer)

// TODO:
//#define PUB_DEV_TOPIC PROT_NAME "/" PROT_VER "/pub/"
//#define PUB_VAL_TOPIC PROT_NAME "/" PROT_VER "/val/"
//#define SUB_DEV_TOPIC PROT_NAME "/" PROT_VER "/sub/"
var SubFunTopic string = fmt.Sprintf("%sfun/", TopicPrefix)

func ActionAllowedByBroker(action int) bool {
	switch action {
	case ActionPubValRegAbort:
		return true
	case ActionSubFunRegEnd:
		return true
	case ActionSubFunRegAbort:
		return true
	case ActionSubFunReqAbort:
		return true
	default:
		return false
	}
}

func ActionAllowedByThirdParty(action int) bool {
	switch action {
	case ActionPubValRegEnd:
		return true
	case ActionPubValRegAbort:
		return true
	case ActionSubFunRegAbort:
		return true
	case ActionSubFunReqEnd:
		return true
	case ActionSubFunReqAbort:
		return true
	default:
		return false
	}
}

func ActionAllowedByPublisher(action int) bool {
	switch action {
	case ActionPubValRegStart:
		return true
	case ActionPubValRegAbort:
		return true
	default:
		return false
	}
}

func ActionAllowedBySubscriber(action int) bool {
	switch action {
	case ActionSubFunRegStart:
		return true
	case ActionSubFunRegAbort:
		return true
	case ActionSubFunReqStart:
		return true
	case ActionSubFunReqAbort:
		return true
	default:
		return false
	}
}
