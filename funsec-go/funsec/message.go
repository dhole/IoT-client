package funsec

// #cgo LDFLAGS: -lsodium
// #include <sodium.h>
// #include "../../funsec-c/keys.h"
import "C"

import (
	"../mqtt"
	"encoding/base64"
	"encoding/json"
	"fmt"
	proto "github.com/huin/mqtt"
	//"github.com/fatih/structs"
	cir "../circuit"
	pahoMQTT "github.com/eclipse/paho.mqtt.golang"
	"strings"
)

// NOTE: The elements of these structs must be ordered in lexicographical order!

type WMsg struct {
	From   DeviceID        `json:"from"`
	Msg    interface{}     `json:"-"`
	RawMsg json.RawMessage `json:"msg"`
	Sig    *Signature      `json:"-"`
	SigB64 string          `json:"sig,omitempty"`
	Topic  string          `json:"topic,omitempty"`
	Type   int             `json:"type"`
}

type MsgVal struct {
	T  uint64      `json:"t"`
	Ts int64       `json:"ts"`
	V  []cir.Label `json:"v"`
}

type MsgFun struct {
	T  uint64 `json:"t"`
	Ts int64  `json:"ts"`
	V  Bin    `json:"v"`
}

type Bin []bool

func (b Bin) MarshalText() ([]byte, error) {
	bb := BinBuf(cir.Bin2Bytes(b))
	return bb.MarshalText()
}

func (b *Bin) UnmarshalText(s []byte) error {
	var bb BinBuf
	if err := bb.UnmarshalText(s); err != nil {
		return err
	}
	*b = cir.Bytes2Bin(bb, uint64(len(bb)*8))
	return nil
}

type Msg struct {
	Action  int             `json:"action"`
	Body    interface{}     `json:"-"`
	RawBody json.RawMessage `json:"body"`
	Token   string          `json:"token"`
}

type PubValRegStart struct {
	KxPk     KxPk     `json:"kx_pk"`
	ValLen   uint     `json:"val_len"`
	ValTopic ValTopic `json:"val_topic"`
}

type PubValRegEnd struct {
	KxPk KxPk `json:"kx_pk"`
}

type Abort struct {
	ErrCode int    `json:"err_code"`
	ID      string `json:"id"`
	Reason  string `json:"reason"`
}

// FUTURE TODO: Finish this once the spec defines it
type SubFunRegStart struct {
	Fun string `json:"fun"`
}

type SubFunRegEnd struct {
	FunID FunctionID `json:"fun_id"`
}

type SubFunReqStart struct {
	FunID FunctionID `json:"fun_id"`
	KxPk  KxPk       `json:"kx_pk"`
}

type SubFunReqEnd struct {
	FunID FunctionID `json:"fun_id"`
	KxPk  KxPk       `json:"kx_pk"`
	Nonce Nonce      `json:"nonce"`
	Rkf   BinBuf     `json:"rkf"`
}

func Base64URLEncode(buf []byte) string {
	return base64.URLEncoding.EncodeToString(buf)
}

func Base64URLDecode(str string, buf_len int) ([]byte, error) {
	buf, err := base64.URLEncoding.DecodeString(str)
	if err != nil {
		return nil, err
	}
	if buf_len != 0 && len(buf) != buf_len {
		return nil, fmt.Errorf("Decoded base64 doesn't match expected length")
	}
	return buf, nil
}

// Returns err if verification fails, nil if it succeeds
func (wmsg *WMsg) Verify(topic string, pk *SignPk) error {
	if wmsg.Sig == nil {
		return fmt.Errorf("No signature found")
	}
	sig := wmsg.Sig
	wmsg.Sig = nil
	defer func() { wmsg.Sig = sig }()

	//fmt.Printf("=== VERIFY (U) ===\n%+v\n\n", wmsg)
	wmsg.Topic = topic
	defer func() { wmsg.Topic = "" }()

	wmsg_buf, err := MarshalWMsg(wmsg)
	if err != nil {
		return err
	}
	if err := SignVerify(wmsg_buf, sig, pk); err != nil {
		return err
	}
	return nil
}

func (wmsg *WMsg) Sign(topic string, sk *SignSk) error {
	wmsg.Sig = nil
	wmsg.Topic = topic
	defer func() { wmsg.Topic = "" }()
	wmsg_buf, err := MarshalWMsg(wmsg)
	if err != nil {
		return err
	}
	//fmt.Printf("=== SIGN ===\n%v\n\n", string(wmsg_buf))
	sig := Sign(wmsg_buf, sk)

	wmsg.Sig = sig
	return nil
}

func (wmsg *WMsg) ServerPublish(svr *mqtt.Server, topic string) error {
	wmsg_buf, err := MarshalWMsg(wmsg)
	if err != nil {
		return fmt.Errorf("Error marshaling WMsg: %v", err)
	}
	svr.Publish(&proto.Publish{
		Header:    proto.Header{QosLevel: 1},
		TopicName: topic,
		MessageId: svr.NextID(),
		Payload:   proto.BytesPayload(wmsg_buf),
	})
	return nil
}

func (wmsg *WMsg) ClientPublish(c pahoMQTT.Client, topic string) (pahoMQTT.Token, error) {
	wmsg_buf, err := MarshalWMsg(wmsg)
	if err != nil {
		return nil, fmt.Errorf("Error marshaling WMsg: %v", err)
	}

	token := c.Publish(topic, 1, false, wmsg_buf)
	return token, nil
}

func MarshalWMsg(wmsg *WMsg) ([]byte, error) {
	if wmsg.Sig != nil {
		wmsg.SigB64 = Base64URLEncode(wmsg.Sig[:])
	} else {
		wmsg.SigB64 = ""
	}
	switch msg := wmsg.Msg.(type) {
	case Msg:
		rawBody, err := json.Marshal(msg.Body)
		if err != nil {
			return nil, err
		}
		msg.RawBody = rawBody
		rawMsg, err := json.Marshal(msg)
		if err != nil {
			return nil, err
		}
		msg.RawBody = nil
		wmsg.RawMsg = rawMsg
		wmsg.Type = MsgTypeMsg
	case MsgVal:
		rawMsg, err := json.Marshal(msg)
		if err != nil {
			return nil, err
		}
		wmsg.RawMsg = rawMsg
		wmsg.Type = MsgTypeValMsg
	case MsgFun:
		rawMsg, err := json.Marshal(msg)
		if err != nil {
			return nil, err
		}
		wmsg.RawMsg = rawMsg
		wmsg.Type = MsgTypeFunMsg
	default:
		return nil, fmt.Errorf("Msg is of invalid type: %T", msg)
	}

	wmsg_buf, err := json.Marshal(wmsg)
	wmsg.RawMsg = nil
	if err != nil {
		return nil, err
	}
	return wmsg_buf, nil
}

func UnmarshalWMsg(buf []byte) (*WMsg, error) {
	var wmsg WMsg
	if err := json.Unmarshal(buf, &wmsg); err != nil {
		//fmt.Println("err wmsg")
		return nil, err
	}
	if wmsg.SigB64 != "" {
		sig_buf, err := Base64URLDecode(wmsg.SigB64, SignatureLen)
		if err != nil {
			return nil, err
		}
		wmsg.Sig = new(Signature)
		copy(wmsg.Sig[:], sig_buf)
		wmsg.SigB64 = ""
	}
	//fmt.Printf("%+v\n", wmsg)
	//fmt.Printf("msg: %+v\n", string(wmsg.RawMsg))
	switch wmsg.Type {
	case MsgTypeValMsg:
		msgVal := new(MsgVal)
		if err := json.Unmarshal(wmsg.RawMsg, msgVal); err != nil {
			return nil, err
		}
		wmsg.Msg = *msgVal
		wmsg.RawMsg = nil
		return &wmsg, nil

	case MsgTypeFunMsg:
		msgFun := new(MsgFun)
		if err := json.Unmarshal(wmsg.RawMsg, msgFun); err != nil {
			return nil, err
		}
		wmsg.Msg = *msgFun
		wmsg.RawMsg = nil
		return &wmsg, nil

	case MsgTypeMsg:
		msg := new(Msg)
		if err := json.Unmarshal(wmsg.RawMsg, msg); err != nil {
			return nil, err
		}

		switch msg.Action {
		case ActionPubValRegStart:
			var body PubValRegStart
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body
		case ActionPubValRegEnd:
			var body PubValRegEnd
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body
		case ActionPubValRegAbort:
			var body Abort
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body

		case ActionSubFunRegStart:
			var body SubFunRegStart
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body
		case ActionSubFunRegEnd:
			var body SubFunRegEnd
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body
		case ActionSubFunRegAbort:
			var body Abort
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body

		case ActionSubFunReqStart:
			var body SubFunReqStart
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body
		case ActionSubFunReqEnd:
			var body SubFunReqEnd
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body
		case ActionSubFunReqAbort:
			var body Abort
			if err := json.Unmarshal(msg.RawBody, &body); err != nil {
				return nil, err
			}
			msg.Body = body

		default:
			return nil, fmt.Errorf("Unknown action")
		}

		msg.RawBody = nil
		wmsg.Msg = *msg
		wmsg.RawMsg = nil

		return &wmsg, nil
	default:
		return nil, fmt.Errorf("Unknown message type")
	}
}

// Check for correctness of a WMsg received on a server and verify its signature:
// 1. Verify that the device is in the DB
// 2. Verify that the device is allowed to pub or sub
// 3. Verify that the Msg in WMsg is of the proper type
// 4. In case of an action message, verify that the sender is allowed to perform such action
// 5. Verify that the from field corresponds to the DeviceID in the topic
// 6. Verify the signature
// returns the Client corresponding to the sender or an error
func WMsgCheckVerifyServer(cfs *ClientsFuncs, wmsg *WMsg, topic string) (*Client, error) {
	topicGroup, topicDevID, _ := SplitTopic(topic)

	devID := wmsg.From
	// Verify that the device is in the DB
	cl := cfs.GetCl(devID)
	if cl == nil {
		return nil, fmt.Errorf("DeviceID %s is not in the DB", devID)
	}

	// Verify that the device is allowed to pub / sub
	if !cl.IsPub && (topicGroup == "pub" || topicGroup == "val") {
		return nil, fmt.Errorf("DeviceID %s is not a Publisher", devID)
	}

	if !cl.IsSub && (topicGroup == "sub") {
		return nil, fmt.Errorf("DeviceID %s is not a Subscriber", devID)
	}

	if topicGroup == "val" {
		_, ok := wmsg.Msg.(MsgVal)
		if !ok {
			return nil, fmt.Errorf("Invalid MsgVal in message")
		}
	} else {
		msg, ok := wmsg.Msg.(Msg)
		if !ok {
			return nil, fmt.Errorf("Invalid Msg in message")
		}
		allowed := false
		if cl.IsPub {
			allowed = allowed || ActionAllowedByPublisher(msg.Action)
		}
		if cl.IsSub {
			allowed = allowed || ActionAllowedBySubscriber(msg.Action)
		}
		if !allowed {
			return nil, fmt.Errorf("Client is not allowed to perform action %v",
				msg.Action)
		}
	}

	// Verify that from is devID
	if devID != DeviceID(topicDevID) {
		return nil, fmt.Errorf("devID found in the from (%s) field doesn't match the one from the topic (%s)",
			devID, topicDevID)
	}

	// Verify signature
	if err := wmsg.Verify(topic, &cl.Pk); err != nil {
		return nil, err
	}
	return cl, nil
}

// Check for correctness of a WMsg received on a client and verify its signature:
// 1. Verify that the message doesn't come from us
// 2. Verify that the Msg in WMsg is of the proper type
// If it's a function output value:
// 	3a. Verify that the message comes from the Broker
// If it's a device-specific message:
// 	3a. Verify that the message comes from the Broker or the Third Party
// 	3b. Verify that the sender is allowed to perform the action
// 3. Verify the signature
// Returns the unwrapped message or an error
func (cl *Client) WMsgCheckVerify(wmsg *WMsg, topic string) (interface{}, error) {
	if !strings.HasPrefix(topic, TopicPrefix) {
		return nil, fmt.Errorf("Unexpected topic, no prefix: %s", topic)
	}

	topicGroup, topicDevID, _ := SplitTopic(topic)
	devID := wmsg.From

	// 1. If the `from` is us, ignore packet
	if devID == cl.DevID {
		return nil, fmt.Errorf("Possible echo message")
	}

	var pk *SignPk
	switch topicGroup {
	case "pub": // Publisher device specific topic
		fallthrough
	case "sub": // Subscriber device specific topic
		if topicDevID != cl.DevID {
			return nil, fmt.Errorf("Unexpected topic, DeviceID doesn't match: %s",
				topic)
		}
		// 2.
		if wmsg.Type != MsgTypeMsg {
			return nil, fmt.Errorf("Message type is not Msg but %v", wmsg.Type)
		}
		msg := wmsg.Msg.(Msg)

		// 3a. If the `from` is not B or TP, ignore packet
		// 3b. Verify that the action is allowed by the `from` (either B or TP)
		if devID == cl.Broker.DevID {
			pk = &cl.Broker.Pk
			if !ActionAllowedByBroker(msg.Action) {
				return nil, fmt.Errorf(
					"Broker is not allowed to perform action %v",
					msg.Action)
			}
		} else if devID == cl.ThirdParty.DevID {
			pk = &cl.ThirdParty.Pk
			if !ActionAllowedByThirdParty(msg.Action) {
				return nil, fmt.Errorf(
					"Third Party is not allowed to perform action %v",
					msg.Action)
			}
		} else {
			return nil, fmt.Errorf("Message not coming from Broker nor Third Party")
		}
	case "fun": // Subscribed function output value
		// 2.
		if wmsg.Type != MsgTypeFunMsg {
			return nil, fmt.Errorf("Message type is not MsgFun but %v", wmsg.Type)
		}
		// 3a. If the `from` is not B, ignore packet
		if devID != cl.Broker.DevID {
			return nil, fmt.Errorf("Function output value not comming from Broker")
		}
		pk = &cl.Broker.Pk
	default:
		return nil, fmt.Errorf("Unexpected topic: %s", topic)
	}
	// 4. Verify signature using the `from`, else ignore
	if err := wmsg.Verify(topic, pk); err != nil {
		return nil, err
	}

	return wmsg.Msg, nil
}

// From a funsec topic, extract Group, DeviceID and Arg.  On error, Group is ""
func SplitTopic(topic string) (string, DeviceID, string) {
	topicTrimmed := strings.TrimPrefix(topic, TopicPrefix)
	topicSplit := strings.SplitN(topicTrimmed, "/", 3)
	if len(topicSplit) < 2 {
		return "", "", ""
	}
	topicGroup := topicSplit[0]
	topicDevID := DeviceID(topicSplit[1])
	arg := ""
	if len(topicSplit) == 3 {
		arg = topicSplit[2]
	}
	return topicGroup, topicDevID, arg
}

func MessageTest() {

	str := "{\"from\":\"BFzvcKUZfCbTc466y2jasoAyawuyHdQ8fIVVxKfow3M=\",\"msg\":{\"action\":0,\"body\":{\"kx_pk\":\"4Ia--wGavsgzYMB4rmW6LlJV_n1oIe25YGMvWcrk3l8=\",\"val_len\":16,\"val_topic\":\"USC/SAL/223/temp\"},\"token\":\"yajNmPcXJPDA\"},\"sig\":\"IeZG5NPne7lMWv--MxUW9tXbWH1NMx_1fA8K7IihaOWjAg7Vr4tB1W3a4cZZYrYFgpC6RT_Ao7LOIsRBHYUYAA==\"}"
	wmsg, err := UnmarshalWMsg([]byte(str))
	if err != nil {
		fmt.Println(err)
		fmt.Println("Error unmarshaling WMsg")
	} else {
		fmt.Printf("%+v\n", wmsg)
	}

	sig := NewSignKey()
	err = wmsg.Sign("topic/test", &sig.Sk)
	if err != nil {
		fmt.Println("Error signing", err)
		return
	}
	fmt.Printf("%+v\n", wmsg)
	err = wmsg.Verify("topic/test", &sig.Pk)
	if err != nil {
		fmt.Println("Verification failed", err)
		return
	} else {
		fmt.Println("Verification succeeded!")
	}
	wmsg_buf, err := MarshalWMsg(wmsg)
	if err != nil {
		fmt.Println(err)
		fmt.Println("Error marshaling WMsg")
	} else {
		fmt.Println(string(wmsg_buf))
	}
}

func GenToken() string {
	// TODO: Implement
	return ""
}
