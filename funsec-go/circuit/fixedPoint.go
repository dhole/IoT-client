package circuit

import (
	"fmt"
	"math"
)

const (
	Fixed32Fra uint32 = 8
	Fixed32Int uint32 = 32 - Fixed32Fra
	Fixed64Fra uint32 = Fixed32Fra * 2
	Fixed64Int uint32 = Fixed32Int * 2
)

var Fixed32Scale int32 = int32(math.Pow(2, float64(Fixed32Fra)))
var Fixed64Scale int64 = int64(math.Pow(2, float64(Fixed64Fra)))

type Fixed32 int32
type Fixed64 int64

func NewFixed32(x float64) Fixed32 {
	if x >= float64(1<<(Fixed64Int-1)) {
		panic("Fixed32 overflows at conversion from float")
	}
	i := int32(x * float64(Fixed32Scale))
	return Fixed32(i)
}

func NewFixed32FromBin(str []bool) (Fixed32, error) {
	if len(str) != 32 {
		return Fixed32(0), fmt.Errorf("len(str) is not 32")
	}
	v := Bin2uint64(str, 32)
	return Fixed32(int32(v)), nil
}

func NewFixed64(x float64) Fixed64 {
	if x >= float64(1<<(Fixed64Int-1)) {
		panic("Fixed64 overflows at conversion from float")
	}
	i := int64(x * float64(Fixed64Scale))
	return Fixed64(i)
}

func NewFixed64FromBin(str []bool) (Fixed64, error) {
	if len(str) != 64 {
		return Fixed64(0), fmt.Errorf("len(str) is not 64")
	}
	v := Bin2uint64(str, 64)
	return Fixed64(int64(v)), nil
}

func FixedString(x int64, inte, fra uint32) string {
	shift, mask := fra, int64(1<<fra-1)
	fraDig := int(math.Ceil(math.Log10(math.Pow(2, float64(fra)))))
	fraFmt := fmt.Sprintf("%%0%dd", fraDig)
	fixFmt := fmt.Sprintf("%%d.%s", fraFmt)
	if x < 0 {
		x = -x
		fixFmt = fmt.Sprintf("-%%d.%s", fraFmt)
	}
	return fmt.Sprintf(fixFmt, int64(x>>shift),
		int64(float64(x&mask)/math.Pow(2, float64(fra))*math.Pow(10, float64(fraDig))))
}

func (x Fixed32) String() string {
	return FixedString(int64(x), Fixed32Int, Fixed32Fra)
}

func (x Fixed32) Bin() []bool {
	return Uint64ToBin(uint64(x), 32)
}

func (x Fixed64) String() string {
	return FixedString(int64(x), Fixed64Int, Fixed64Fra)
}

func (x Fixed32) Add(y Fixed32) Fixed32 {
	return x + y
}

func (x Fixed32) Sub(y Fixed32) Fixed32 {
	return x - y
}

func (x Fixed32) Mul(y Fixed32) Fixed32 {
	return x * y / Fixed32(Fixed32Scale)
}

func (x Fixed32) Div(y Fixed32) Fixed32 {
	return x * Fixed32(Fixed32Scale) / y
}

// (x Fixed64) Mul() should use muli64 to avoid overflow
// https://github.com/golang/image/blob/master/math/fixed/fixed.go#L117

func Fixed32Test() {
	a := float64(56.78)
	b := float64(-12.34)
	fa := NewFixed32(a)
	fb := NewFixed32(b)
	fmt.Println("a =", fa, a)
	fmt.Println("b =", fb, b)
	fmt.Println("a+b =", fa.Add(fb), a+b)
	fmt.Println("a-b =", fa.Sub(fb), a-b)
	fmt.Println("a*b =", fa.Mul(fb), a*b)
	fmt.Println("a/b =", fa.Div(fb), a/b)
	ba := fa.Bin()
	bb := fb.Bin()
	fmt.Println(fa, "->", ba)
	fmt.Println(fb, "->", bb)
	_fa, _ := NewFixed32FromBin(ba)
	_fb, _ := NewFixed32FromBin(bb)
	fmt.Println(fa, "=", _fa)
	fmt.Println(fb, "=", _fb)
}
