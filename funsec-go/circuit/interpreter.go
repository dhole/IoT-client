/*
 * Original source
 *
 *     https://pkelchte.wordpress.com/2013/12/31/scm-go/
 *     https://gist.github.com/pkelchte/c2bd76b9f8f9cd603b3c
 *
 *     A minimal Scheme interpreter, as seen in lis.py and SICP
 *     http://norvig.com/lispy.html
 *     http://mitpress.mit.edu/sicp/full-text/sicp/book/node77.html
 *
 *     Pieter Kelchtermans 2013
 *     LICENSE: WTFPL 2.0
 *
 * Modifications
 *
 *     Copyright (c) 2017,
 *     Bhaskar Krishnamachari, Muhammad Naveed, Eduard Sanou, Kwame-Lante Wright.
 *     LICENSE: BSD 3-Clause License
 *
 */
package circuit

import (
	"fmt"
	"math"
	"reflect"
	"strconv"
	"strings"
)

var none = []Scmer{}

/*
 Eval / Apply
*/

const (
	maxDepth uint = 1024
)

func eval(expression Scmer, en *Env) (value Scmer) {
	en.callDepth++
	if en.callDepth >= maxDepth {
		panic("Reached maximum call depth")
	}
	switch e := expression.(type) {
	case bool:
		value = e
	case number:
		value = e
	case str:
		value = e
	case Wire:
		value = e
	case []Wire:
		value = e
	case SimWire:
		value = e
	case Fixed32:
		value = e
	case Fixed64:
		value = e
	case symbol:
		value = en.Find(e).vars[e]
	case []Scmer:
		if len(e) == 0 {
			value = e
			break
		}
		switch car, _ := e[0].(symbol); car {
		case "quote":
			value = e[1]
		case "if":
			if eval(e[1], en).(bool) {
				value = eval(e[2], en)
			} else {
				value = eval(e[3], en)
			}
		// We probably don't want to enable overwriting variables
		//case "set!":
		//	v := e[1].(symbol)
		//	en.Find(v).vars[v] = eval(e[2], en)
		//	value = none
		case "define":
			en.vars[e[1].(symbol)] = eval(e[2], en)
			value = none
		case "lambda":
			value = proc{e[1], e[2], en}
		case "let":
			syms := make([]Scmer, 0)
			exps := make([]Scmer, 0)
			for _, binding := range e[1].([]Scmer) {
				pair := binding.([]Scmer)
				sym := pair[0].(symbol)
				syms = append(syms, sym)
				exp := pair[1].(Scmer)
				exps = append(exps, exp)
			}
			res := []Scmer{[]Scmer{symbol("lambda"), syms, e[2].(Scmer)}}
			res = append(res, exps...)
			value = eval(res, en)
		case "begin":
			for _, i := range e[1:] {
				value = eval(i, en)
			}
		default:
			operands := e[1:]
			values := make([]Scmer, len(operands))
			for i, x := range operands {
				values[i] = eval(x, en)
			}
			value = apply(eval(e[0], en), values)
		}
	default:
		panic(fmt.Sprint("Unknown expression type - EVAL ", e))
	}
	en.callDepth--
	return
}

func apply(procedure Scmer, args []Scmer) (value Scmer) {
	switch p := procedure.(type) {
	case func(...Scmer) Scmer:
		value = p(args...)
	case proc:
		en := &Env{make(vars), p.en, p.en.callDepth, p.en.C}
		switch params := p.params.(type) {
		case []Scmer:
			for i, param := range params {
				en.vars[param.(symbol)] = args[i]
			}
		default:
			en.vars[params.(symbol)] = args
		}
		value = eval(p.body, en)
	default:
		//value = append([]Scmer{procedure}, args...)
		panic(fmt.Sprint("Unknown procedure type - APPLY ", p))
	}
	return
}

type proc struct {
	params, body Scmer
	en           *Env
}

/*
 Primitives
*/

func numToFixed32(a number) Fixed32 {
	return NewFixed32(float64(a))
}

func matchTypeArith(c *CircuitData, a interface{}, b interface{}) (interface{}, interface{}) {
	if reflect.TypeOf(a) == reflect.TypeOf(b) {
		return a, b
	}
	_, oka := a.([]Wire)
	_, okb := b.([]Wire)
	if oka || okb {
		if oka {
			return a, c.floatToWires(float64(b.(number)))
		} else {
			return c.floatToWires(float64(a.(number))), b
		}
	}
	_, oka = a.(Fixed32)
	_, okb = b.(Fixed32)
	if oka || okb {
		if oka {
			return a, numToFixed32(b.(number))
		} else {
			return numToFixed32(a.(number)), b
		}
	}
	return nil, nil
}

/*
 Environments
*/

type vars map[symbol]Scmer
type Env struct {
	vars
	outer     *Env
	callDepth uint
	C         *CircuitData
}

func (e *Env) Find(s symbol) *Env {
	if _, ok := e.vars[s]; ok {
		return e
	} else {
		return e.outer.Find(s)
	}
}

func (en *Env) EvalOrErr(expression Scmer) (value Scmer) {
	defer func() {
		if r := recover(); r != nil {
			value = fmt.Errorf("Evaluation error: %v", r)
		}
	}()
	value = eval(expression, en)
	if ws, ok := value.([]Wire); ok {
		en.C.output = &ws
		en.C.OutputLen = uint64(len(value.([]Wire)))
	}
	return
}

func (en *Env) Init() {
	en.vars = vars{
		"+": func(a ...Scmer) Scmer {
			x, y := matchTypeArith(en.C, a[0], a[1])
			switch x := x.(type) {
			case number:
				return x + y.(number)
			case []Wire:
				z, _ := en.C.Gc.Add(en.C.ctxt, x, y.([]Wire))
				return z
			case Fixed32:
				return x.Add(y.(Fixed32))
			default:
				panic("add2: Invalid arg types")
			}
		},
		"-": func(a ...Scmer) Scmer {
			x, y := matchTypeArith(en.C, a[0], a[1])
			switch x := x.(type) {
			case number:
				return x - y.(number)
			case []Wire:
				return en.C.Gc.Sub(en.C.ctxt, x, y.([]Wire))
			case Fixed32:
				return x.Sub(y.(Fixed32))
			default:
				panic("sub2: Invalid arg types")
			}
		},
		"*": func(a ...Scmer) Scmer {
			x, y := matchTypeArith(en.C, a[0], a[1])
			switch x := x.(type) {
			case number:
				return x * y.(number)
			case []Wire:
				return en.C.Gc.Mul(en.C.ctxt, x, y.([]Wire))
			case Fixed32:
				return x.Mul(y.(Fixed32))
			default:
				panic("mul2: Invalid arg types")
			}
		},
		"/": func(a ...Scmer) Scmer {
			x, y := matchTypeArith(en.C, a[0], a[1])
			switch x := x.(type) {
			case number:
				return x / y.(number)
			case []Wire:
				if y, ok := a[1].(number); ok {
					invY := NewFixed32(1).Div(NewFixed32(float64(y)))
					return en.C.Gc.Mul(en.C.ctxt, x, en.C.fixed32ToWires(invY))
				}
				return en.C.Gc.Div(en.C.ctxt, x, y.([]Wire))
			case Fixed32:
				return x.Div(y.(Fixed32))
			default:
				panic("div2: Invalid arg types")
			}
		},
		"min2": func(a ...Scmer) Scmer {
			x, y := matchTypeArith(en.C, a[0], a[1])
			switch x := x.(type) {
			case number:
				if x <= y.(number) {
					return x
				} else {
					return y
				}
			case []Wire:
				return en.C.Gc.Min(en.C.ctxt, x, y.([]Wire))
			case Fixed32:
				if x <= y.(Fixed32) {
					return x
				} else {
					return y
				}
			default:
				panic("min2: Invalid arg types")
			}
		},
		"max2": func(a ...Scmer) Scmer {
			x, y := matchTypeArith(en.C, a[0], a[1])
			switch x := x.(type) {
			case number:
				if x >= y.(number) {
					return x
				} else {
					return y
				}
			case []Wire:
				return en.C.Gc.Max(en.C.ctxt, x, y.([]Wire))
			case Fixed32:
				if x >= y.(Fixed32) {
					return x
				} else {
					return y
				}
			default:
				panic("max2: Invalid arg types")
			}
		},
		"les": func(a ...Scmer) Scmer {
			x, y := matchTypeArith(en.C, a[0], a[1])
			switch x := x.(type) {
			case number:
				if x < y.(number) {
					return true
				} else {
					return false
				}
			case []Wire:
				return en.C.Gc.Les(en.C.ctxt, x, y.([]Wire))
			case Fixed32:
				if x < y.(Fixed32) {
					return true
				} else {
					return false
				}
			default:
				panic("min2: Invalid arg types")
			}
		},
		"~": func(a ...Scmer) Scmer {
			switch x := a[0].(type) {
			case []Wire:
				return en.C.Gc.Not(en.C.ctxt, x)
			case Fixed32:
				return Fixed32(^uint32(x))
			default:
				panic("not: Invalid arg type")
			}
		},
		"abs": func(a ...Scmer) Scmer {
			switch x := a[0].(type) {
			case number:
				return number(math.Abs(float64(x)))
			case []Wire:
				return en.C.Gc.Abs(en.C.ctxt, x)
			//case Fixed32:
			//	panic("abs for Fixed32 not implemented")
			default:
				panic("abs: Invalid arg type")
			}
		},
		"shl": func(a ...Scmer) Scmer {
			switch x := a[0].(type) {
			case []Wire:
				return en.C.Gc.ShiftL(en.C.ctxt, x)
			default:
				panic("abs: Invalid arg type")
			}
		},
		"shr": func(a ...Scmer) Scmer {
			switch x := a[0].(type) {
			case []Wire:
				return en.C.Gc.ShiftR(en.C.ctxt, x)
			default:
				panic("abs: Invalid arg type")
			}
		},
		"inc": func(a ...Scmer) Scmer {
			switch x := a[0].(type) {
			case []Wire:
				return en.C.Gc.Inc(en.C.ctxt, x)
			case Fixed32:
				return Fixed32(uint32(x) + 1)
			default:
				panic("not: Invalid arg type")
			}
		},
		"<=": func(a ...Scmer) Scmer {
			return a[0].(number) <= a[1].(number)
		},
		"equal?": func(a ...Scmer) Scmer {
			return reflect.DeepEqual(a[0], a[1])
		},
		"cons": func(a ...Scmer) Scmer {
			switch car := a[0]; cdr := a[1].(type) {
			case []Scmer:
				return append([]Scmer{car}, cdr...)
			default:
				return []Scmer{car, cdr}
			}
		},
		"car": func(a ...Scmer) Scmer {
			l := a[0].([]Scmer)
			if len(l) > 0 {
				return l[0]
			} else {
				return []Scmer{}
			}
		},
		"cdr": func(a ...Scmer) Scmer {
			return a[0].([]Scmer)[1:]
		},
		"list": func(a ...Scmer) Scmer {
			l := make([]Scmer, len(a))
			for i, e := range a {
				l[i] = eval(e, en)
			}
			return l
		},
		"val": func(a ...Scmer) Scmer {
			v := a[0].(str)
			ws, ok := en.C.InputWires[string(v)]
			if !ok {
				ws = make([]Wire, wireBusLen)
				for i, _ := range ws {
					ws[i] = en.C.nextInputWire()
				}
				en.C.InputWires[string(v)] = ws
			}
			return ws
		},
		"sim": func(a ...Scmer) Scmer {
			v := a[0].(number)
			return NewFixed32(float64(v))
		},
		// Usually we won't use this function, the casting will be implicit
		"const": func(a ...Scmer) Scmer {
			if v, ok := a[0].(bool); ok {
				if v {
					return []Wire{en.C.wireOne()}
				} else {
					return []Wire{en.C.wireZero()}
				}
			}
			v := a[0].(number)
			return en.C.floatToWires(float64(v))
		},
		"start-building": func(a ...Scmer) Scmer {
			en.C.inputSet = true
			if en.C.Gc != nil {
				panic("started-building already invoked")
			}
			en.C.StartBuilding()
			return none
		},
		"#t": true,
		"#f": false,
	}
	en.outer = nil
	en.callDepth = 0
	en.C = NewCircuitData()
}

/*
 Parsing
*/

//symbols, numbers, expressions, procedures, lists, ... all implement this
//interface, which enables passing them along in the interpreter
type Scmer interface{}

// Implicit types:
//   []Scmer //list
//   bool
//   Wire
//   []Wire

type symbol string  //symbols are represented by strings
type number float64 //numbers by float64
type str string     //str are quoted strings without spaces
type SimWire bool   //SimWire is a simluation of a wire

func (w *Wire) String() string {
	return string(*w)
}

func GenCircuit(inputs ...string) (*CircuitData, error) {
	var ienv Env
	ienv.Init()
	var res Scmer
	for _, input := range inputs {
		tokens := Tokenize(string(input))
		for len(tokens) > 0 {
			expr := ReadTokens(&tokens)
			if expr == nil {
				continue
			}
			if expr, ok := expr.(error); ok {
				return nil, expr
			}
			res = ienv.EvalOrErr(expr)
			if res, ok := res.(error); ok {
				return nil, res
			}
		}
	}
	_, ok := res.([]Wire)
	if ok {
		return ienv.C, nil
	}
	return nil, fmt.Errorf("Evaluation output is not []Wire")
}

func Read(s string) (expression Scmer) {
	tokens := Tokenize(s)
	expression = ReadTokens(&tokens)
	return expression
}

func ReadTokens(tokens *[]string) (expression Scmer) {
	defer func() {
		if r := recover(); r != nil {
			expression = fmt.Errorf("Parsing error: %v", r)
		}
	}()
	expression = readFrom(tokens)
	return expression
}

//Syntactic Analysis
func readFrom(tokens *[]string) (expression Scmer) {
	//pop first element from tokens
	token := (*tokens)[0]
	*tokens = (*tokens)[1:]
	switch token {
	case "(": //a list begins
		L := make([]Scmer, 0)
		for (*tokens)[0] != ")" {
			L = append(L, readFrom(tokens))
		}
		*tokens = (*tokens)[1:]
		return L
	default: //an atom occurs
		if f, err := strconv.ParseFloat(token, 64); err == nil {
			return number(f)
		}
		if len(token) >= 2 && token[0] == '"' && token[len(token)-1] == '"' {
			return str(token[1 : len(token)-1])
		}
		return symbol(token)
	}
}

//Lexical Analysis
func Tokenize(s string) []string {
	// Remove comments
	lines := strings.Split(s, "\n")
	for i, l := range lines {
		if c := strings.Index(l, ";"); c != -1 {
			lines[i] = lines[i][:c]
		}
	}
	s = strings.Join(lines, " ")
	s = strings.Replace(s, "\t", " ", -1)
	s = strings.Replace(s, "(", "( ", -1)
	s = strings.Replace(s, ")", " )", -1)
	tokens := strings.Fields(s)
	return tokens
}

/*
 Interactivity
*/

func String(v Scmer) string {
	switch v := v.(type) {
	case []Scmer:
		l := make([]string, len(v))
		for i, x := range v {
			l[i] = String(x)
		}
		return "(" + strings.Join(l, " ") + ")"
	case str:
		return fmt.Sprintf("\"%s\"", v)
	case SimWire:
		return fmt.Sprintf("s%v", v)
	case Fixed32:
		return fmt.Sprintf("s%v", v)
	case Fixed64:
		return fmt.Sprintf("ss%v", v)
	case Wire:
		return fmt.Sprintf("w%06x", v)
	case []Wire:
		ws := make([]string, len(v))
		for i, w := range v {
			ws[i] = String(w)
		}
		return "<ws: " + strings.Join(ws, " ") + ">"
	default:
		return fmt.Sprint(v)
	}
}
