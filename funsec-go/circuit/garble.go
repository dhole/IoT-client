package circuit

// #cgo LDFLAGS: -L/home/dev/Documents/research/IoT/libgarble/src/.libs/ -L/home/dev/Documents/research/IoT/libgarble/builder/.libs/ -lgarble -lgarblec -lmsgpackc
// #cgo CFLAGS: -I../../../libgarble/src/ -I../../../libgarble/builder/
// #include <string.h>
// #include "garble.h"
// #include "circuit_builder.h"
// #include "circuits.h"
//
// block
// bytes_to_block(uint8_t *b)
// {
// 	block blck;
// 	uint8_t *v = (uint8_t*) &blck;
// 	v[000] = b[000]; v[001] = b[001]; v[002] = b[002]; v[003] = b[003];
// 	v[004] = b[004]; v[005] = b[005]; v[006] = b[006]; v[007] = b[007];
// 	v[010] = b[010]; v[011] = b[011]; v[012] = b[012]; v[013] = b[013];
// 	v[014] = b[014]; v[015] = b[015]; v[016] = b[016]; v[017] = b[017];
//      return blck;
// }
//
// void
// block_to_bytes(block blck, uint8_t *b)
// {
// 	uint8_t *v = (uint8_t*) &blck;
// 	b[000] = v[000]; b[001] = v[001]; b[002] = v[002]; b[003] = v[003];
// 	b[004] = v[004]; b[005] = v[005]; b[006] = v[006]; b[007] = v[007];
// 	b[010] = v[010]; b[011] = v[011]; b[012] = v[012]; b[013] = v[013];
// 	b[014] = v[014]; b[015] = v[015]; b[016] = v[016]; b[017] = v[017];
// }
//
// void
// copy_blocks(block *dst, block *src, size_t n)
// {
//     memcpy(dst, src, n * sizeof(block));
// }
//
// void
// garble_clear_wires(garble_circuit *gc)
// {
//     memset(gc->wires, '\0', 2 * gc->r * sizeof(block));
//     free(gc->wires);
//     gc->wires = NULL;
// }
//
// void
// garble_circuit_copy(garble_circuit *dst, garble_circuit *src, garble_context *ctxt)
// {
//      memset(dst, '\0', sizeof(garble_circuit));
//    	//*dst = *src; // Copy n, m, q, r, nxors, type, fixed_label, global_key
//      dst->n = src->n; dst->m = src->m; dst->q = src->q; dst->r = src->r;
//      dst->nxors = src->nxors; dst->nands = src->nands;
//      dst->nors = src->nors; dst->nnots = src->nnots;
//      dst->type = src->type;
//
//      dst->gates = malloc(ctxt->n_gates * sizeof(garble_gate));
// 	memcpy(dst->gates, src->gates, src->q * sizeof(garble_gate));
//      dst->table = NULL;
//      dst->wires = NULL;
//      dst->outputs = malloc(src->m * sizeof(int));
// 	memcpy(dst->outputs, src->outputs, src->m * sizeof(int));
//      dst->output_perms = NULL;
// }
//
// void
// garble_set_m(garble_circuit *gc, size_t m)
// {
//     gc->m = m;
//     if (gc->outputs)
//         free(gc->outputs);
//     gc->outputs = calloc(gc->m, sizeof(int));
// }
//
import "C"

import (
	//	"fmt"
	"crypto/aes"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"unsafe"
)

const (
	GarbleOk  int    = C.GARBLE_OK
	GarbleErr int    = C.GARBLE_ERR
	LabelLen  uint64 = 128 / 8
)

var wireBusLen = 32

func Bytes2Bin(buf []byte, n uint64) (str []bool) {
	str = make([]bool, n)
	for i := uint64(0); i < n; i++ {
		if buf[i/8]&(1<<(i%8)) == 0 {
			str[i] = false
		} else {
			str[i] = true
		}
	}
	return str
}

func Bin2Bytes(str []bool) (buf []byte) {
	buf = make([]byte, (len(str)+7)/8)
	for i, v := range str {
		if v {
			buf[i/8] |= byte(1) << (uint(i) % 8)
		}
	}
	return buf
}

func BinXor(x, y []bool) (z []bool) {
	z = make([]bool, len(x))
	for i, _ := range x {
		if x[i] != y[i] {
			z[i] = true
		} else {
			z[i] = false
		}
	}
	return z
}

func Base64URLEncode(buf []byte) string {
	return base64.URLEncoding.EncodeToString(buf)
}

func Base64URLDecode(str string, buf_len int) ([]byte, error) {
	buf, err := base64.URLEncoding.DecodeString(str)
	if err != nil {
		return nil, err
	}
	if buf_len != 0 && len(buf) != buf_len {
		return nil, fmt.Errorf("Decoded base64 doesn't match expected length")
	}
	return buf, nil
}

type Label [LabelLen]byte

func (l Label) String() string {
	return hex.EncodeToString(l[:])
}

func (l Label) MarshalText() ([]byte, error) {
	return []byte(Base64URLEncode(l[:])), nil
}

func (l *Label) UnmarshalText(s []byte) error {
	buf, err := Base64URLDecode(string(s), int(LabelLen))
	if err != nil {
		return err
	}
	copy(l[:], buf)
	return nil
}

func (x *Label) Xor(y Label) (z Label) {
	for i := 0; i < len(x); i++ {
		z[i] = x[i] ^ y[i]
	}
	return z
}

func Labels2CBlocks(bs []Label) []C.block {
	cbs := make([]C.block, len(bs))
	for i, _ := range bs {
		cbs[i] = C.bytes_to_block((*C.uint8_t)(&bs[i][0]))
	}
	return cbs
}

func CBlocks2Labels(cbs []C.block) []Label {
	bs := make([]Label, len(cbs))
	for i, _ := range cbs {
		C.block_to_bytes(cbs[i], (*C.uint8_t)(&bs[i][0]))
	}
	return bs
}

func MapLabels(labelMap []Label, val []bool) []Label {
	labels := make([]Label, len(labelMap)/2)
	for i, v := range val {
		if v {
			labels[i] = labelMap[i*2+1]
		} else {
			labels[i] = labelMap[i*2]
		}
	}
	return labels
}

func AESEncrypt(key, src Label) (dst Label) {
	c, _ := aes.NewCipher(key[:])
	c.Encrypt(dst[:], src[:])
	return dst
}

func AESDecrypt(key, src Label) (dst Label) {
	c, _ := aes.NewCipher(key[:])
	c.Decrypt(dst[:], src[:])
	return dst
}

//Wire represents a circuit wire that can be input/output of a circuit gate
type Wire C.int

type CircuitData struct {
	Gc                *GarbleCircuit
	ctxt              *GarbleCtxt
	output            *[]Wire
	OutputLen         uint64
	InputCount        uint64
	inputSet          bool
	InputWires        map[string][]Wire
	CipherInputs      *[]Label
	inputMap          *[]Label
	T                 uint64 // Round corresponding to the ratchet key used for masking
	TimeEncryptInputs int64
	TimeGarble        int64
}

func NewCircuitData() *CircuitData {
	return &CircuitData{
		Gc:           nil,
		ctxt:         nil,
		InputCount:   0,
		inputSet:     false,
		InputWires:   map[string][]Wire{},
		CipherInputs: nil,
		inputMap:     nil, // Private
	}
}

func (c *CircuitData) InitCircuit() {
	gc := NewGarbleCircuit()
	ctxt := new(GarbleCtxt)
	c.Gc = gc
	c.ctxt = ctxt
}

func (c *CircuitData) Size() uint32 {
	size := c.Gc.Size()
	size += 64 / 8 // OutputLen
	size += 64 / 8 // InputCount
	// We asume that a wire is 32b, and that inputs are indexed by 64b UUID instead of strings.
	size += uint32(len(c.InputWires))*(64/8) + uint32(c.InputCount)*(32/8) // InputWires
	size += uint32(len(*c.CipherInputs)) * uint32(LabelLen)                // CipherInputs
	size += 64 / 8                                                         // T

	return size
}

func (c *CircuitData) StartBuilding() {
	c.InitCircuit()
	c.Gc.StartBuilding(c.ctxt, c.InputCount)
}

func (c *CircuitData) nextInputWire() Wire {
	if c.inputSet {
		panic("Inputs must be defined before building the circuit")
	}
	defer func() {
		c.InputCount++
	}()
	return Wire(c.InputCount)
}

func (c *CircuitData) wireOne() Wire {
	if !c.inputSet {
		c.inputSet = true
	}
	return Wire(c.InputCount + 1)
}

func (c *CircuitData) wireZero() Wire {
	if !c.inputSet {
		c.inputSet = true
	}
	return Wire(c.InputCount)
}

func (c *CircuitData) floatToWires(a float64) []Wire {
	return c.fixed32ToWires(NewFixed32(a))
}

func (c *CircuitData) fixed32ToWires(a Fixed32) []Wire {
	bin := Uint64ToBin(uint64(a), 32)
	ws := make([]Wire, 32)
	for i, b := range bin {
		if b {
			ws[i] = c.wireOne()
		} else {
			ws[i] = c.wireZero()
		}
	}
	return ws
}

func (c *CircuitData) CopyCircuit() *CircuitData {
	var newC CircuitData
	var gc GarbleCircuit
	C.garble_circuit_copy((*C.garble_circuit)(&gc), (*C.garble_circuit)(c.Gc),
		(*C.garble_context)(c.ctxt))
	newC.Gc = &gc
	ctxt := *c.ctxt
	newC.ctxt = &ctxt
	output := make([]Wire, len(*c.output))
	copy(output, *c.output)
	newC.output = &output
	newC.OutputLen = c.OutputLen
	newC.InputCount = c.InputCount
	newC.inputSet = c.inputSet
	newC.InputWires = make(map[string][]Wire)
	for k, v := range c.InputWires {
		newC.InputWires[k] = v
	}
	newC.CipherInputs = nil
	newC.inputMap = nil
	return &newC
}

func (c *CircuitData) Mask(mask []bool) error {
	if c.output == nil {
		return fmt.Errorf("Evaluation output is not []Wire")
	}
	wireMask := make([]Wire, len(mask))
	for i, v := range mask {
		if v {
			wireMask[i] = c.wireOne()
		} else {
			wireMask[i] = c.wireZero()
		}
	}
	//fmt.Println("len(wireMask) =", len(wireMask))
	//fmt.Println(wireMask)
	maskedOutput := c.Gc.Xor(c.ctxt, *c.output, wireMask)
	c.output = &maskedOutput
	return nil
}

func (c *CircuitData) Finish() error {
	if c.output == nil {
		return fmt.Errorf("Evaluation output is not []Wire")
	}
	c.Gc.FinishBuilding(c.ctxt, *c.output)
	c.ctxt = nil
	return nil
}

func (c *CircuitData) Garble() error {
	inputMap, err := c.Gc.Garble()
	if err != nil {
		return err
	}
	c.inputMap = &inputMap
	return nil
}

// Debugging function
func (c *CircuitData) GetInputMap() []Label {
	return *c.inputMap
}

func EncryptInputMaps(pubLabelMap, cirLabelMap []Label) (cipher []Label) {
	cipher = make([]Label, len(pubLabelMap))
	for i := 0; i < len(pubLabelMap)/2; i++ {
		pubZero := &pubLabelMap[i*2]
		pubOne := &pubLabelMap[i*2+1]
		cirZero := &cirLabelMap[i*2]
		cirOne := &cirLabelMap[i*2+1]
		//fmt.Printf(">>> Encrypt (%+v) %+v %+v\n", pubZero, cirZero, cirOne)

		cipherZero := AESEncrypt(*pubZero, *cirZero)
		cipherOne := AESEncrypt(*pubOne, *cirOne)
		if pubZero[0]&1 == 0 {
			cipher[i*2] = cipherZero
			cipher[i*2+1] = cipherOne
		} else {
			cipher[i*2] = cipherOne
			cipher[i*2+1] = cipherZero
		}
	}
	return cipher
}

//func (c *CircuitData) EncryptInputMaps(rks map[string]*funsec.RatchetKey) error {
func (c *CircuitData) EncryptInputMaps(labelMaps map[string][]Label) error {
	pubLabelMap := make([]Label, 2*c.Gc.n)
	for k, ws := range c.InputWires {
		//rk := rks[k]
		//if rk == nil {
		//	return fmt.Errorf("ValTopic ratchet key not in the arguments")
		//}
		//labelMap := rk.GenLabelMap(32)
		labelMap, ok := labelMaps[k]
		if !ok {
			return fmt.Errorf("ValTopic labelMap not in the arguments")
		}
		for i, w := range ws {
			pubLabelMap[2*w] = labelMap[2*i]
			pubLabelMap[2*w+1] = labelMap[2*i+1]
		}

	}
	cipher := EncryptInputMaps(pubLabelMap, *c.inputMap)
	c.CipherInputs = &cipher
	c.inputMap = nil
	return nil
}

func (c *CircuitData) DecryptInputLabels(pubLabels []Label) (cirLabels []Label) {
	cirLabels = make([]Label, len(pubLabels))
	for i, pl := range pubLabels {
		if pl[0]&1 == 0 {
			cirLabels[i] = AESDecrypt(pl, (*c.CipherInputs)[i*2])
		} else {
			cirLabels[i] = AESDecrypt(pl, (*c.CipherInputs)[i*2+1])
		}
		//fmt.Printf(">>> Decrypt (%+v) %+v %+v\n",
		//	pl,
		//	AESDecrypt(pl, (*c.CipherInputs)[i*2]),
		//	AESDecrypt(pl, (*c.CipherInputs)[i*2+1]))
	}
	return cirLabels
}

//func (c *CircuitData) Eval(rks map[string]*funsec.RatchetKey) error {
//	// TODO
//	return nil
//}

func GarbleTest() {
	c := NewCircuitData()
	in := make([]Wire, 2)
	in[0] = c.nextInputWire()
	in[1] = c.nextInputWire()
	//in[2] = c.nextInputWire()
	//in[3] = c.nextInputWire()
	c.StartBuilding()
	//out, _ := c.Gc.Add(c.ctxt, in, in)

	//out := c.Gc.Les(c.ctxt, in[:2], in[2:])
	//c.output = &[]Wire{out}

	out := c.Gc.BitOr(c.ctxt, in[0], in[1])
	c.output = &[]Wire{out}

	//out := c.Gc.Not(c.ctxt, in)
	//c.output = &out

	c.Finish()
	fmt.Println("Finish")
	c.Garble()
	fmt.Println("Garble")
	//a := []bool{true, true, false, false}
	a := []bool{true, false}
	fmt.Println("a =", a)
	fmt.Println("InputMap =", c.GetInputMap())
	inputLabels := MapLabels(c.GetInputMap(), a)
	fmt.Println("inputLabels =", inputLabels)
	x, err := c.Gc.Eval(inputLabels)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Eval")
	fmt.Println("x =", x)
}

type GarbleCircuit C.garble_circuit
type GarbleCtxt C.garble_context

func NewGarbleCircuit() *GarbleCircuit {
	var gc C.garble_circuit
	// FIXME: HALFGATES doesn't implement the OR gate.
	//if int(C.garble_new(&gc, 0, 0, C.GARBLE_TYPE_HALFGATES)) == GarbleErr {
	if int(C.garble_new(&gc, 0, 0, C.GARBLE_TYPE_STANDARD)) == GarbleErr {
		panic("Unable to create new garble circuit")
	}
	_gc := GarbleCircuit(gc)
	return &_gc
}

func (gc *GarbleCircuit) Delete() {
	C.garble_delete((*C.garble_circuit)(gc))
}

func (gc *GarbleCircuit) UnmarshalBinary(buf []byte) error {
	cBuf := C.CBytes(buf)
	defer C.free(cBuf)
	if C.garble_from_buffer((*C.garble_circuit)(gc),
		(*C.char)(cBuf), false, false) == C.GARBLE_ERR {
		return fmt.Errorf("Error decoding buffered garbled circuit")
	}
	//C.garble_fprint(C.stderr, (*C.garble_circuit)(gc))
	return nil
}

func (gc *GarbleCircuit) MarshalBinary() ([]byte, error) {
	//C.garble_fprint(C.stderr, (*C.garble_circuit)(gc))
	cBuf := C.garble_to_buffer((*C.garble_circuit)(gc), nil, false, false)
	defer C.free(unsafe.Pointer(cBuf))
	buf := C.GoBytes(unsafe.Pointer(cBuf),
		C.int(C.garble_size((*C.garble_circuit)(gc), false, false)))
	return buf, nil
}

func (gc *GarbleCircuit) Size() uint32 {
	size := uint32(C.garble_size((*C.garble_circuit)(gc), false, false))
	return size
}

func (gc *GarbleCircuit) GetNGates() uint64 {
	return uint64(gc.q)
}

func (gc *GarbleCircuit) GetNXors() uint64 {
	return uint64(gc.nxors)
}

func (gc *GarbleCircuit) GetNAnds() uint64 {
	return uint64(gc.nands)
}

func (gc *GarbleCircuit) GetNOrs() uint64 {
	return uint64(gc.nors)
}

func (gc *GarbleCircuit) GetNNots() uint64 {
	return uint64(gc.nnots)
}

func (gc *GarbleCircuit) SetN(n uint64) {
	gc.n = C.size_t(n)
}

func (gc *GarbleCircuit) SetM(m uint64) {
	C.garble_set_m((*C.garble_circuit)(gc), C.size_t(m))
}

func (gc *GarbleCircuit) StartBuilding(ctxt *GarbleCtxt, inputCount uint64) {
	gc.SetN(inputCount)
	C.builder_start_building((*C.garble_circuit)(gc), (*C.garble_context)(ctxt))
}

func (gc *GarbleCircuit) FinishBuilding(ctxt *GarbleCtxt, output []Wire) {
	gc.SetM(uint64(len(output)))
	C.builder_finish_building((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		(*C.int)(&output[0]))
}

func (gc *GarbleCircuit) Garble() ([]Label, error) {
	//_inputMap := Labels2CBlocks(inputMap)
	_inputMap := make([]C.block, 2*gc.n)
	if int(C.garble_garble((*C.garble_circuit)(gc), nil, nil)) == GarbleErr {
		return nil, fmt.Errorf("Garble failed")
	}
	C.copy_blocks(&_inputMap[0], gc.wires, 2*gc.n)
	inputMap := CBlocks2Labels(_inputMap)
	// Clear wires to hide the wire label mappings
	C.garble_clear_wires((*C.garble_circuit)(gc))
	return inputMap, nil
}

func (gc *GarbleCircuit) Eval(inputLabels []Label) ([]bool, error) {
	//C.garble_fprint(C.stderr, (*C.garble_circuit)(gc))
	_inputLabels := Labels2CBlocks(inputLabels)
	_outputLabels := make([]C.block, gc.m)
	_outputs := make([]C._Bool, gc.m)
	//fmt.Println("gc.m = ", gc.m)
	if int(C.garble_eval((*C.garble_circuit)(gc),
		&_inputLabels[0], &_outputLabels[0], (*C._Bool)(&_outputs[0]))) == GarbleErr {
		return nil, fmt.Errorf("Eval failed")
	}
	outputs := make([]bool, len(_outputs))
	for i, v := range _outputs {
		if v {
			outputs[i] = true
		} else {
			outputs[i] = false
		}
	}
	return outputs, nil
}

func (gc *GarbleCircuit) Add(ctxt *GarbleCtxt, x, y []Wire) ([]Wire, Wire) {
	if len(x) != len(y) {
		panic("Circuit Add: input lengths don't match")
	}
	output := make([]Wire, len(x))
	inputs := append(x, y...)
	var carry Wire
	C.circuit_add((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output[0]), (*C.int)(&carry))
	return output, carry
}

func (gc *GarbleCircuit) Sub(ctxt *GarbleCtxt, x, y []Wire) []Wire {
	if len(x) != len(y) {
		panic("Circuit Sub: input lengths don't match")
	}
	output := make([]Wire, len(x))
	inputs := append(x, y...)
	C.circuit_sub((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) Mul(ctxt *GarbleCtxt, x, y []Wire) []Wire {
	if len(x) != len(y) {
		panic("Circuit Mul: input lengths don't match")
	}
	output := make([]Wire, len(x))
	inputs := append(x, y...)
	C.circuit_mul_fxp_NN((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output[0]), C.uint64_t(Fixed32Fra), true)
	return output
}

func (gc *GarbleCircuit) Div(ctxt *GarbleCtxt, x, y []Wire) []Wire {
	if len(x) != len(y) {
		panic("Circuit Div: input lengths don't match")
	}
	output := make([]Wire, len(x))
	inputs := append(x, y...)
	C.circuit_div_fxp((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output[0]), C.uint64_t(Fixed32Fra), true)
	return output
}

func (gc *GarbleCircuit) Inc(ctxt *GarbleCtxt, x []Wire) []Wire {
	output := make([]Wire, len(x))
	C.circuit_inc((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)),
		(*C.int)(&x[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) Abs(ctxt *GarbleCtxt, x []Wire) []Wire {
	output := make([]Wire, len(x))
	C.circuit_abs((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)),
		(*C.int)(&x[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) Mux(ctxt *GarbleCtxt, s Wire, x, y []Wire) []Wire {
	if len(x) != len(y) {
		panic("Circuit Mux: input lengths don't match")
	}
	output := make([]Wire, len(x))
	inputs := append(x, y...)
	C.circuit_mux((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(C.int)(s), (*C.int)(&inputs[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) Min(ctxt *GarbleCtxt, x, y []Wire) []Wire {
	if len(x) != len(y) {
		panic("Circuit Min: input lengths don't match")
	}
	output := make([]Wire, len(x))
	inputs := append(x, y...)
	C.circuit_min((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output[0]), true)
	return output
}

func (gc *GarbleCircuit) Max(ctxt *GarbleCtxt, x, y []Wire) []Wire {
	if len(x) != len(y) {
		panic("Circuit Max: input lengths don't match")
	}
	output := make([]Wire, len(x))
	inputs := append(x, y...)
	C.circuit_max((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output[0]), true)
	return output
}

func (gc *GarbleCircuit) Equ(ctxt *GarbleCtxt, x, y []Wire) Wire {
	if len(x) != len(y) {
		panic("Circuit Equ: input lengths don't match")
	}
	var output Wire
	inputs := append(x, y...)
	C.circuit_equ((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output))
	return output
}

// Returns 0 if the first number is less.
// Returns 1 if the second number is less.
// Returns 0 if the numbers are equal.
func (gc *GarbleCircuit) Les(ctxt *GarbleCtxt, x, y []Wire) []Wire {
	if len(x) != len(y) {
		panic("Circuit Les: input lengths don't match")
	}
	output := make([]Wire, 1)
	inputs := append(x, y...)
	C.circuit_les((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) Leq(ctxt *GarbleCtxt, x, y []Wire) Wire {
	if len(x) != len(y) {
		panic("Circuit Leq: input lengths don't match")
	}
	var output Wire
	inputs := append(x, y...)
	C.circuit_leq((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output))
	return output
}

func (gc *GarbleCircuit) Gre(ctxt *GarbleCtxt, x, y []Wire) Wire {
	if len(x) != len(y) {
		panic("Circuit Gre: input lengths don't match")
	}
	var output Wire
	inputs := append(x, y...)
	C.circuit_gre((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output))
	return output
}

func (gc *GarbleCircuit) Geq(ctxt *GarbleCtxt, x, y []Wire) Wire {
	if len(x) != len(y) {
		panic("Circuit Geq: input lengths don't match")
	}
	var output Wire
	inputs := append(x, y...)
	C.circuit_geq((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output))
	return output
}

func (gc *GarbleCircuit) Xor(ctxt *GarbleCtxt, x, y []Wire) []Wire {
	if len(x) != len(y) {
		panic("Circuit Xor: input lengths don't match")
	}
	output := make([]Wire, len(x))
	inputs := append(x, y...)
	C.circuit_xor((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)*2),
		(*C.int)(&inputs[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) ShiftL(ctxt *GarbleCtxt, x []Wire) []Wire {
	output := make([]Wire, len(x))
	C.circuit_shl((*C.garble_circuit)(gc),
		C.uint64_t(len(x)),
		(*C.int)(&x[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) ShiftR(ctxt *GarbleCtxt, x []Wire) []Wire {
	output := make([]Wire, len(x))
	C.circuit_shr((*C.garble_circuit)(gc),
		C.uint64_t(len(x)),
		(*C.int)(&x[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) Not(ctxt *GarbleCtxt, x []Wire) []Wire {
	output := make([]Wire, len(x))
	C.circuit_not((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.uint64_t(len(x)),
		(*C.int)(&x[0]), (*C.int)(&output[0]))
	return output
}

func (gc *GarbleCircuit) BitOr(ctxt *GarbleCtxt, x, y Wire) Wire {
	output := Wire(C.builder_next_wire((*C.garble_context)(ctxt)))
	C.gate_OR((*C.garble_circuit)(gc), (*C.garble_context)(ctxt),
		C.int(x), C.int(y), C.int(output))
	return output
}

/* Building a circuit

   garble_type_e type = GARBLE_TYPE_HALFGATES
   garble_circuit gc;
   garble_context ctxt;

   int *inputs = malloc(n * sizeof(int));
   int *outputs = malloc(m * sizeof(int));

   garble_new(gc, n, m, type); // OK
   // after builder_start_building, gc->n can't be touched
   builder_start_building(gc, &ctxt); // OK
   // skip, we already do this with nextInputWire()
   builder_init_wires(inputs, n); // OK

   fold(gc, &ctxt, inputs, outputs, k, b, circuit_add_NN);
   //circuit_add(gc, &ctxt, b * 2, inputs, outputs, NULL);
   // after builder_finish_building, gc->m can't be touched
   builder_finish_building(gc, &ctxt, outputs); // OK

*/

/* Garbling

   block *inputLabels = garble_allocate_blocks(2 * n);
   block *outputMap = garble_allocate_blocks(2 * m);

   if (garble_garble(&gc, NULL, outputMap) == GARBLE_ERR) {
       fprintf(stderr, "garble failed\n");
       return 1;
   }

   // Fill inputLabels with the input wire mapping from gc
   memcpy(inputLabels, gc.wires, 2 * gc.n * sizeof(block));

*/

/* Evaluating

   block *extractedLabels = garble_allocate_blocks(n);
   block *computedOutputMap = garble_allocate_blocks(m);
   bool *outputVals = calloc(m, sizeof(bool));

   // Map inputs to extractedLabels using the mapping inputLabels
   garble_extract_labels(extractedLabels, inputLabels, inputs, gc.n);
   garble_eval(&gc, extractedLabels, computedOutputMap, outputVals);
*/

func Uint64ToBin(v uint64, n uint32) []bool {
	str := make([]bool, n)
	for i := uint32(0); i < n; i++ {
		if (v & (1 << i)) == 0 {
			str[i] = false
		} else {
			str[i] = true
		}
	}
	return str
}

func Bin2uint64(str []bool, n uint32) uint64 {
	v := uint64(0)
	for i := uint32(0); i < n; i++ {
		if str[i] {
			v += 1 << uint32(i)
		}
	}
	return v
}
