package main

import (
	cir "../circuit"
	"../funsec"
	"encoding/json"
	"fmt"
	"github.com/alexflint/go-arg"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/op/go-logging"
	"os"
	"time"
	//"strings"
	//proto "github.com/huin/mqtt"
)

var log = logging.MustGetLogger("subscriber")
var logFormat = logging.MustStringFormatter(
	`%{time:2006-01-02 15:04:05} %{color}%{level} %{shortfile} %{callpath:4} %{color:reset}%{message}`,
)

var cl *funsec.Client
var funs map[funsec.FunctionID]*funsec.Function
var msgQueue map[funsec.FunctionID](chan *funsec.Msg)
var exit chan bool

var sig *funsec.SignKey

func funRegister(fun *funsec.Function) {
	// TODO: Future work
}

func funRequest(c mqtt.Client, fun *funsec.Function, q chan *funsec.Msg) {

	// KX_STATE_START = 0,
	// KX_STATE_SENT_PK,
	// KX_STATE_SENT_PK_CONF,
	// KX_STATE_RECVD_PK,
	// KX_STATE_FINALIZE,
	// KX_STATE_FINISHED,
	d := fmt.Sprintf("[R:%s]", fun.ID)
	state := funsec.KxStateStart

	var msg *funsec.Msg
	var msgToken string
	var kk funsec.KxKey

	var newMsg bool
	var timeout bool
	var restart bool
	t := time.NewTimer(999 * time.Hour)
	t.Stop()
	for {
		switch state {
		case funsec.KxStateStart:
			log.Debug(d, "KxStateStart")
			msgToken = funsec.GenToken()
			kk = funsec.NewKxKeyPair()
			wmsg := funsec.WMsg{
				From: cl.DevID,
				Msg: funsec.Msg{
					Action: funsec.ActionSubFunReqStart,
					Token:  msgToken,
					Body: funsec.SubFunReqStart{
						FunID: fun.ID,
						KxPk:  kk.Pk,
					},
				},
			}
			wmsg.Sign(cl.SubDevTopic, &sig.Sk)
			token, err := wmsg.ClientPublish(c, cl.SubDevTopic)
			if err != nil {
				log.Critical(d, "Unable to send ActionSubFunReqStart:", err)
				exit <- true
				return
			}
			res := token.WaitTimeout(10 * time.Second)
			if !res || (token.Error() != nil) {
				log.Error(d, "Error sending ActionSubFunReqStart:", token.Error())
				restart = true
				break
			}
			state = funsec.KxStateSentPk
			funsec.TimerSafeReset(t, 10*time.Second)
		case funsec.KxStateSentPk:
			log.Debug(d, "KxStateSentPk")
			if timeout {
				log.Error(d, "Waiting for ActionSubFunReqEnd timeout")
				restart = true
				break
			} else if newMsg {
				if msg.Token != msgToken {
					log.Debug(d, "Received message with unknown token")
					break
				}
				if msg.Action != funsec.ActionSubFunReqEnd {
					log.Debug(d, "Received message with unexpected action:",
						msg.Action)
					break
				}
				state = funsec.KxStateFinalize
				continue
			}
		case funsec.KxStateFinalize:
			log.Debug(d, "KxStateFinalize")
			body := msg.Body.(funsec.SubFunReqEnd)
			key, err := funsec.NewSymKeyKxClient(&kk, &body.KxPk)
			if err != nil {
				log.Error(d, "Key exchange error:", err)
				restart = true
				break
			}
			rkfBuf, err := key.Decrypt(body.Rkf, body.Nonce)
			if err != nil {
				log.Error(d, "Error decrypting rkf:", err)
				restart = true
				break
			}
			var rkf funsec.RatchetKey
			if err := json.Unmarshal(rkfBuf, &rkf); err != nil {
				log.Error(d, "Error unmarshaling rkf:", err)
				restart = true
				break
			}
			//log.Errorf("%s Received %+v", d, rkf)
			fun.SetRkf(&rkf)
			token := c.Subscribe(fun.Topic, 1, nil)
			res := token.WaitTimeout(10 * time.Second)
			if !res || (token.Error() != nil) {
				log.Errorf("%s Error subscribing to %s: %v",
					d, fun.Topic, token.Error())
				restart = true
				break
			}
			log.Info(d, "Subscribed to", fun.Topic)
			state = funsec.KxStateFinished

		case funsec.KxStateFinished:
			log.Debug(d, "KxStateFinished")
		}

		newMsg = false
		timeout = false
		if restart {
			state = funsec.KxStateStart
			log.Debug(d, "Going back to KxStateStart, retrying in 30 seconds...")
			funsec.TimerSafeReset(t, 30*time.Second)
		}
		select {
		case <-t.C:
			timeout = true
		case msg = <-q:
			newMsg = true
		}
		if newMsg && msg.Action == funsec.ActionSubFunReqAbort {
			fun.SetRkf(nil)
			token := c.Unsubscribe(fun.Topic)
			res := token.WaitTimeout(10 * time.Second)
			if !res || (token.Error() != nil) {
				log.Errorf(d, "Error unsubscribing from %s: %v",
					fun.Topic, token.Error())
			}
			state = funsec.KxStateStart
			newMsg = false
		}
	}
}

// Function called when a message (not delivered to the device specific topic) arrives
// This is called in a goroutine
func msgArrvd(c mqtt.Client, m mqtt.Message) {
	// TODO: Check that the topic is from a registered known function
	// TODO: Process message (verify signature, unmarshal, yada yada)
	log.Debugf("[M] TOPIC: %s\n", m.Topic())
	log.Debugf("[M] MSG: %s\n", m.Payload())

	topicGroup, _funID, _ := funsec.SplitTopic(m.Topic())
	if topicGroup != "fun" {
		log.Errorf("[M] Topic is not a Subscription Function")
		return
	}
	funID := funsec.FunctionID(_funID)
	fun, ok := funs[funID]
	if !ok {
		log.Errorf("[M] Not subscribed to function ID %v", funID)
		return
	}
	rkf := fun.GetRkf()
	if rkf == nil {
		log.Errorf("[M] Function ID %v doesn't have a ratchet key yet", funID)
		return
	}
	wmsg, err := funsec.UnmarshalWMsg(m.Payload())
	if err != nil {
		log.Errorf("[M] error unmarshaling WMsg: %s", err)
		return
	}

	log.Debug("[M] Received subscription message")
	_msg, err := cl.WMsgCheckVerify(wmsg, m.Topic())
	if err != nil {
		log.Errorf("[M] %s", err)
		return
	}
	msgFun, ok := _msg.(funsec.MsgFun)
	if !ok {
		log.Errorf("[M] Message type is not MsgFun")
		return
	}

	if msgFun.T < rkf.T {
		log.Errorf("Requested rkf round %v, but we are at round %v",
			msgFun.T, rkf.T)
		return
	}
	//log.Debug("[M] Prev rkf.T =", rkf.T, "got msgFun.T =", msgFun.T)
	advance := msgFun.T - rkf.T
	for i := uint64(0); i < advance; i++ {
		//log.Debug("[M] Advance")
		rkf = rkf.Advance()
	}
	fun.SetRkf(rkf)

	//byteMask := rkf.Cprng(uint64(len(msgFun.V)), 0)
	//mask := cir.Bytes2Bin(byteMask, uint64(len(msgFun.V)))

	log.Debug("[M] Using rkf.T =", rkf.T)
	mask := rkf.GenMask(uint64(len(msgFun.V)))
	//log.Debug("Mask", mask)
	bOut := cir.BinXor(msgFun.V, mask)

	out, err := cir.NewFixed32FromBin(bOut)
	if err != nil {
		log.Errorf("[M] Can't decode value: %v", err)
		return
	}
	log.Info("[M] Subscription", funID, "val =", out)
}

// Function called when a message delivered to the device specific topic arrives
// This is called in a goroutine
func devTopicMsgArrvd(c mqtt.Client, m mqtt.Message) {
	log.Debugf("[D] DevTopic TOPIC: %s\n", m.Topic())
	log.Debugf("[D] DevTopic MSG: %s\n", m.Payload())

	wmsg, err := funsec.UnmarshalWMsg(m.Payload())
	if err != nil {
		log.Errorf("[D] error unmarshaling WMsg: %s", err)
		return
	}
	// DEBUG
	//log.Debugf("MSG %+v", wmsg)

	log.Debug("[D] Received device specific message")
	_msg, err := cl.WMsgCheckVerify(wmsg, m.Topic())
	if err != nil {
		log.Errorf("[D] %s", err)
		return
	}
	msg, ok := _msg.(funsec.Msg)
	if !ok {
		log.Errorf("[M] Message type is not Msg")
		return
	}

	var funID funsec.FunctionID
	switch msg.Action {
	case funsec.ActionSubFunRegEnd:
		body := msg.Body.(funsec.SubFunRegEnd)
		funID = body.FunID
	case funsec.ActionSubFunReqEnd:
		body := msg.Body.(funsec.SubFunReqEnd)
		funID = body.FunID
	case funsec.ActionSubFunRegAbort:
		body := msg.Body.(funsec.Abort)
		funID = funsec.FunctionID(body.ID)
	case funsec.ActionSubFunReqAbort:
		body := msg.Body.(funsec.Abort)
		funID = funsec.FunctionID(body.ID)
	default:
		log.Errorf("[D] Unexpected action in msg: %d", msg.Action)
		return
	}

	// TODO: Pass the msg to funRequest through FunID channel
	q, ok := msgQueue[funID]
	if !ok {
		log.Errorf("[D] Unknown FunctionID %s", funID)
		return
	}
	q <- &msg
}

func connectBroker(c mqtt.Client) {
	log.Debug("Connecting to Broker...")
	for {
		token := c.Connect()
		res := token.WaitTimeout(10 * time.Second)
		if !res || (token.Error() != nil) {
			log.Error("Error connecting to Broker:", token.Error())
			log.Info("Trying again in 30 seconds...")
			time.Sleep(30 * time.Second)
			continue
		}
		break
	}
	log.Notice("Successfully connected to Broker")

	// NOTE: Client.Subscribe() seems to be thread safe, it reads client
	// elements using mutexes and puts the message in a channel.
	log.Debug("Subscribing...")
	for {
		token := c.Subscribe(cl.SubDevTopic, 1, devTopicMsgArrvd)
		res := token.WaitTimeout(10 * time.Second)
		if !res || (token.Error() != nil) {
			log.Errorf("Error subscribing to %s: %v", cl.SubDevTopic, token.Error())
			log.Info("Trying again in 30 seconds...")
			time.Sleep(30 * time.Second)
			continue
		}
		break
	}
	log.Notice("Subscribed to device specific topic", cl.SubDevTopic)

}

func reconnectBroker(c mqtt.Client, err error) {
	log.Errorf("Connection to Broker lost: %v", err)
	connectBroker(c)
	// TODO: Resubscribe to successfully requested functions (those for
	// which we already have the ratchet key)
}

type args struct {
	Verbose    bool   `arg:"-v,help:Verbose output"`
	Config     string `arg:"-c,help:Configuration file"`
	PubKey     string `arg:"-k,help:Public key file"`
	SecKey     string `arg:"-P,help:Private key file"`
	Broker     string `arg:"-b,help:Broker public key file"`
	ThirdParty string `arg:"-t,help:Third Party public key file"`
	DataBase   string `arg:"-d,help:DataBase file"`
	Host       string `arg:"-h,help:MQTT Broker hostname"`
	Port       int    `arg:"-p,help:MQTT Broker port"`
}

func (args) Version() string {
	return fmt.Sprintf("%s third party %s", funsec.ProtName, funsec.Version)
}

func (args) Description() string {
	return fmt.Sprintf("%s third party -- Third Party for %s %s",
		funsec.ProtName, funsec.ProtName, funsec.ProtVer)
}

func main() {
	var args args
	args.Verbose = false
	args.Config = funsec.DefaultSubscriberConfFile
	args.PubKey = funsec.DefaultSubscriberPubKeyFile
	args.SecKey = funsec.DefaultSubscriberSecKeyFile
	args.Broker = funsec.DefaultBrokerPubKeyFile
	args.ThirdParty = funsec.DefaultTPPubKeyFile
	args.DataBase = funsec.DefaultSubscriberDataBase
	args.Host = funsec.DefaultMQTTHost
	args.Port = funsec.DefaultMQTTListenPort
	arg.MustParse(&args)

	logBackend := logging.NewLogBackend(os.Stderr, "", 0)
	logging.SetBackend(logBackend)
	logging.SetFormatter(logFormat)
	if args.Verbose {
		logging.SetLevel(logging.DEBUG, "subscriber")
	} else {
		logging.SetLevel(logging.INFO, "subscriber")
	}

	log.Notice("Starting Subscriber...")

	var err error
	sig, err = funsec.LoadSigKeys(args.PubKey, args.SecKey)
	if err != nil {
		log.Critical("Unable to load Subscriber keys:", err)
		return
	}
	funDescs, err := funsec.LoadSubConf(args.Config)
	if err != nil {
		log.Critical("Unable to load Subscriber config:", err)
		return
	}
	cl = funsec.NewClient(funsec.DeviceID(funsec.Base64URLEncode(sig.Pk[:])), false, true)
	if cl == nil {
		log.Critical("Error creating Client struct")
		return
	}
	log.Notice("Subscriber DeviceID:", cl.DevID)
	cl.Broker = &funsec.Server{}
	err = funsec.LoadPubKey(args.Broker, &cl.Broker.Pk)
	if err != nil {
		log.Critical("Unable to load Broker key:", err)
		return
	}
	cl.Broker.DevID = funsec.DeviceID(funsec.Base64URLEncode(cl.Broker.Pk[:]))
	cl.ThirdParty = &funsec.Server{}
	err = funsec.LoadPubKey(args.ThirdParty, &cl.ThirdParty.Pk)
	if err != nil {
		log.Critical("Unable to load Third Party key:", err)
		return
	}
	cl.ThirdParty.DevID = funsec.DeviceID(funsec.Base64URLEncode(cl.ThirdParty.Pk[:]))

	opts := mqtt.NewClientOptions()
	opts.SetProtocolVersion(3)
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", args.Host, args.Port))
	opts.SetClientID(string(cl.CliID))
	opts.SetDefaultPublishHandler(msgArrvd)
	opts.SetConnectionLostHandler(reconnectBroker)

	c := mqtt.NewClient(opts)
	connectBroker(c)
	defer c.Disconnect(500)
	msgQueue = make(map[funsec.FunctionID](chan *funsec.Msg))
	funs = make(map[funsec.FunctionID]*funsec.Function)
	for _, funDesc := range funDescs {
		fID := funDesc.GetFunctionID()
		cl.Sub.FunIDs[fID] = true
		funs[fID] = &funsec.Function{
			ID:    fID,
			Topic: fmt.Sprintf("%s%s", funsec.SubFunTopic, fID),
			Desc:  funDesc,
		}
		msgQueue[fID] = make(chan *funsec.Msg)
		go funRequest(c, funs[fID], msgQueue[fID])
	}

	// NOTE: Client.Publish() seems to be thread safe, it reads client
	// elements using mutexes and puts the message in a channel.
	//Publish 5 messages to /go-mqtt/sample at qos 1 and wait for the receipt
	//from the server after sending each message
	//for i := 0; i < 5; i++ {
	//	text := fmt.Sprintf("this is msg #%d!", i)
	//	token := c.Publish("go-mqtt/sample", 1, false, text)
	//	token.Wait()
	//	log.Debug("Got it")
	//}

	// Block forever
	for !<-exit {
		return
	}
}
