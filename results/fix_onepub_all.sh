#! /bin/sh

cp mean_stats.csv mean_stats.0.csv
cp sum_stats.csv  sum_stats.0.csv
cp mul_stats.csv  mul_stats.0.csv
cp var_stats.csv  var_stats.0.csv
cp min_stats.csv  min_stats.0.csv

./fix_onepub.py < mean_stats.0.csv > mean_stats.csv
./fix_onepub.py < sum_stats.0.csv  > sum_stats.csv
./fix_onepub.py < mul_stats.0.csv  > mul_stats.csv
./fix_onepub.py < var_stats.0.csv  > var_stats.csv
./fix_onepub.py < min_stats.0.csv  > min_stats.csv
