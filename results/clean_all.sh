#! /bin/sh

./clean.sh mean_stats.csv > mean_stats_clean.csv
./clean.sh sum_stats.csv > sum_stats_clean.csv
./clean.sh mul_stats.csv > mul_stats_clean.csv
./clean.sh var_stats.csv > var_stats_clean.csv
./clean.sh min_stats.csv > min_stats_clean.csv
