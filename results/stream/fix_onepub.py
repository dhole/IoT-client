#! /usr/bin/env python3

import sys, re

k = 1
prefix = "# 2017"
for line in sys.stdin:
    if len(line) > len(prefix):
        if line[:len(prefix)] == prefix:
            k = int(re.findall('(?<=n=)[0-9]*', line)[0])
            #print(k)
            print(line, end="")
            continue
    elems = line.split(" ")
    b = elems[0]
    b = int(b[:len(b)-1])
    print(b * k, ", ", sep="", end="")
    print(' '.join(elems[1:]), end="")

