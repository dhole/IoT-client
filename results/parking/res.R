funs = c("mean", "max", "min", "var", "max_free", "min_free")

for (i in 1:length(funs)) {
    fun <- funs[i]
    filename = paste("parking_", fun, "_stats_clean.csv", sep="")
    mydata <- read.csv(filename, sep=",")
    mydata$time_encrypt_in <- mydata$time_encrypt_in / 1000000
    mydata$time_decrypt_in <- mydata$time_decrypt_in / 1000000
    mydata$time_eval <- mydata$time_eval / 1000000
    mydata$time_garble <- mydata$time_garble / 1000000
    mydata$time_send <- mydata$time_send / 1000000

    delta_in = (288*9 - 9)
    mydata$size <- mydata$size + delta_in*(64/8 + 32*(32/8))

    mydata$time_send <- mydata$time_send - mydata$time_encrypt_in - mydata$time_garble

    # Simulate the transfer over a 1Gbit network
    net = 1024 * 1024 * 1024 / 8
    mydata$time_send <- mydata$time_send + (mydata$size / net) * 1000
    mydata$time_garble <- mydata$time_garble + delta_in*0.0324
    mydata$time_eval <- mydata$time_eval + delta_in*0.0215

    mydata$inputs <- mydata$inputs / 32
    mydata$outputs <- mydata$outputs / 32

    time_garble <- mean(mydata$time_garble)
    time_send <- mean(mydata$time_send)
    time_eval <- mean(mydata$time_eval)

    size <- mean(mydata$size)
    print(paste(fun, "time_garble =", time_garble, "time_send =", time_send, "time_eval =", time_eval, "size =", size / 1024 / 1024))
}
