#! /bin/sh

./clean.sh parking_max_free_stats.csv > parking_max_free_stats_clean.csv
./clean.sh parking_max_stats.csv      > parking_max_stats_clean.csv
./clean.sh parking_mean_stats.csv     > parking_mean_stats_clean.csv
./clean.sh parking_min_free_stats.csv > parking_min_free_stats_clean.csv
./clean.sh parking_min_stats.csv      > parking_min_stats_clean.csv
./clean.sh parking_var_stats.csv      > parking_var_stats_clean.csv

