#! /bin/bash
#set -x

#FUNS="sum mul mean var min"
FUNS="sum"

#trap 'tmux kill-session -t FunSecTest; kill_test; killall -9 publisher; exit' SIGINT SIGTERM EXIT

REMOTE_HOST="dev@192.168.0.103"
kill_test() {
  while [ "`wc -l broker_stats.csv | cut -d ' ' -f 1`" -lt "6" ];
  do
    sleep 2
  done
  kill $(jobs -pr)
  killall run_test.sh
  tmux kill-session -t FunSecTest
  ssh "$REMOTE_HOST" "killall -9 publisher"
  ssh "$REMOTE_HOST" "killall run_publishers.sh"
  ssh "$REMOTE_HOST" "killall -9 publisher"
}

mkdir -p results
for fun in $FUNS;
do
  for n in 10 `seq 50 50 1000`;
  do
    echo $fun $n
    rm broker_stats.csv
    touch broker_stats.csv
    kill_test &
    ./run_test.sh -t=6 -n=${n} -f=${fun} --remote
    echo "# `date +'%F %T'`, n=$n" >> results/${fun}_stats.csv
    tail -n +2 broker_stats.csv >> results/${fun}_stats.csv
  done
done
