#! /bin/bash

set -x
ulimit -n 4096

WORK=~/Documents/research/IoT
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$WORK/build.paho/src/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$WORK/libgarble/src/.libs/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$WORK/libgarble/builder/.libs/

REMOTE_HOST="dev@192.168.0.103"

N=2
T=5
FUN="sum"
ONE_PUB="no"
REMOTE="no"
SKIP_FUN="no"
for arg in "$@";
do
  if [ "$arg" == "--skip" ]
  then
    exit 0
  fi
  if [ "$arg" == "--build" ]
  then
    echo "Building go binaries..."
    go build -o thirdParty funsec-go/thirdParty/main.go
    echo "Finished building thirdParty"
    go build -o broker funsec-go/broker/main.go
    echo "Finished building broker"
    go build -o subscriber funsec-go/subscriber/main.go
    echo "Finished building subscriber"
  elif [ "${arg%=*}" == "-n" ]
  then
    N="${arg#*=}"
  elif [ "${arg%=*}" == "-t" ]
  then
    T="${arg#*=}"
  elif [ "${arg%=*}" == "-f" ]
  then
    FUN="${arg#*=}"
  elif [ "$arg" == "--onepub" ]
  then
    ONE_PUB="yes"
  elif [ "$arg" == "--remote" ]
  then
    REMOTE="yes"
  elif [ "$arg" == "--skipfun" ]
  then
    SKIP_FUN="yes"
  fi
done

tmux kill-session -t FunSecTest
#cp publisher_keys.json.0 publisher_keys.json
#trap 'kill -9 $(jobs -pr)' SIGINT SIGTERM EXIT

rm publishers.txt
if [ "$SKIP_FUN" == "no" ]
then
  rm functions/test.lsp
fi
rm tmp/broker.lock
rm tmp/thirdParty.lock
rm -r tmp
mkdir tmp
mkdir tmp/pubs
mkdir tmp/subs

if [ "$ONE_PUB" == "no" ]
then
  M=$N
else
  M=1
fi

for i in `seq 1 $M`;
do
  ./funsec-c/gen_keys -p tmp/pubs/${i}_keys.json
  cat tmp/pubs/${i}_keys.json | grep "\"pk\"" |\
    sed -e 's/"pk": "//g' | sed -e 's/ //g' | sed -e 's/"//g' | sed -e 's/,//g' >> publishers.txt

  cat > tmp/pubs/${i}_conf.json << EOF
  {
    "val_topic": "$i",
    "val_len": 32
  }
EOF
done

if [ "$SKIP_FUN" == "no" ]
then
  echo "(begin" >> functions/test.lsp

  for i in `seq 1 $M`;
  do
    echo "  (define v$i (val \"$i\"))" >> functions/test.lsp
  done

  if [ "$ONE_PUB" == "no" ]
  then
    l=`seq -s " " -f "v%.0f" 1 $N`
  else
    l=`perl -E "say \"v1 \" x $N"`
  fi
  echo "  (define l (list $l))" >> functions/test.lsp
  echo "  (start-building)" >> functions/test.lsp
  echo "  ($FUN l)" >> functions/test.lsp
  echo ")" >> functions/test.lsp
fi

send_values() {
  N=$1
  T=$2
  #while [ ! -f tmp/broker.lock ];
  #do
  #  sleep 1
  #done

  sleep 1
  for j in `seq 1 100`;
  do
    for i in `seq 1 $N`;
    do
      echo -n "$i" | nc 127.0.0.1 "$(( 6000 + ${i} ))"
    done
    sleep $T
  done
}

sleep 2
tmux -f tmux.conf new-session -d -n FunSecTest -s FunSecTest "./thirdParty -v ; sh -i"
tmux split-window -h -p 67 -t 1 "bash"
tmux split-window -h -p 50 -t 2 "while [ ! -f tmp/broker.lock ]; do sleep 1; done; \
  ./subscriber -v ; sh -i"
tmux split-window -v -t 1 "while [ ! -f tmp/thirdParty.lock ]; do sleep 1; done; \
  ./broker -l 0.0.0.0 -t $T -v -V ; sh -i"

while [ ! -f tmp/broker.lock ]
do
  sleep 1
done

set +x
if [ "$REMOTE" == "no" ]
then
  tmux split-window -v -p 90 -t 3 "while [ ! -f tmp/broker.lock ]; do sleep 1; done; \
    ./funsec-c/publisher -v -c tmp/pubs/1_conf.json -k tmp/pubs/1_keys.json -o 6001 ; sh -i"
  tmux split-window -v -p 50 -t 4 "while [ ! -f tmp/broker.lock ]; do sleep 1; done; \
    ./funsec-c/publisher -v -c tmp/pubs/2_conf.json -k tmp/pubs/2_keys.json -o 6002 ; sh -i"

  for i in `seq 3 $M`;
  do
    ./funsec-c/publisher -v -c tmp/pubs/${i}_conf.json -k tmp/pubs/${i}_keys.json -o $(( 6000 + ${i} )) \
      > /dev/null 2>&1 &
  done

  sleep 2
  send_values $M $T &
else
  ssh "$REMOTE_HOST" "rm -r ~/pubsub/pubs/"
  rsync -a tmp/pubs "$REMOTE_HOST":~/pubsub/
  ssh "$REMOTE_HOST" "./run_publishers.sh $M $T" &
fi

tmux -2 attach-session -t FunSecTest
tmux kill-session -t FunSecTest

if [ "$REMOTE" == "no" ]
then
  killall -9 publisher
else
  ssh "$REMOTE_HOST" "killall run_publishers.sh"
  ssh "$REMOTE_HOST" "killall -9 publisher"
fi

#pkill -P $$
