#! /bin/bash
#set -x

start_fun() {
  N=$1
  M=`python3 -c "x=$N; print(x-1, end='')"`
  rm functions/test.lsp

  echo "(begin" >> functions/test.lsp

  ps=""
  ds=""
  for i in `seq 1 $N`;
  do
    echo "  (define v$i (val \"$i\"))" >> functions/test.lsp
    l=`perl -E "say \"v$i \" x $M"`
    ps="$ps$l"
    ds="$ds$l"
  done
  echo "  (define ps (list $ps))" >> functions/test.lsp
  echo "  (define ds (list $ds))" >> functions/test.lsp

  echo "  (start-building)" >> functions/test.lsp
  echo "  (car (cdr (linreg ds ps)))" >> functions/test.lsp

  echo ")" >> functions/test.lsp
}

#trap 'tmux kill-session -t FunSecTest; kill_test; killall -9 publisher; exit' SIGINT SIGTERM EXIT

REMOTE_HOST="dev@192.168.0.103"
kill_test() {
  while [ "`wc -l broker_stats.csv | cut -d ' ' -f 1`" -lt "6" ];
  do
    sleep 2
  done
  kill $(jobs -pr)
  killall -9 publisher
  killall run_test.sh
  killall -9 publisher
  tmux kill-session -t FunSecTest
  ssh "$REMOTE_HOST" "killall -9 publisher"
  ssh "$REMOTE_HOST" "killall run_publishers.sh"
  ssh "$REMOTE_HOST" "killall -9 publisher"
}

mkdir -p results

for i in 5 10 15 20 25 30 35;
do
  #N=`python3 -c "x=$i; print(x*(x-1), end='')"`
  echo $i
  start_fun $i

  rm broker_stats.csv
  touch broker_stats.csv
  kill_test &
  ./run_test.sh -t=12 -n=$i --skipfun --remote
  echo "# `date +'%F %T'`, n=${i}" >> results/turonet_stats.csv
  tail -n +2 broker_stats.csv >> results/turonet_stats.csv
done
