#! /bin/bash
#set -x

start_fn_fun() {
  FUN=$1
  N=$2
  rm functions/test.lsp

  echo "(begin" >> functions/test.lsp

  for i in `seq 1 2`;
  do
    echo "  (define v$i (val \"$i\"))" >> functions/test.lsp
    l=`perl -E "say \"v$i \" x $N"`
    echo "  (define l$i (list $l))" >> functions/test.lsp
  done

  echo "  (start-building)" >> functions/test.lsp
  if [ "$FUN" == "linreg" ]
  then
    echo "  (car ($FUN l1 l2))" >> functions/test.lsp
  else
    echo "  ($FUN l1 l2)" >> functions/test.lsp
  fi

  echo ")" >> functions/test.lsp
}

FUNS="corr2 linreg"

#trap 'tmux kill-session -t FunSecTest; kill_test; killall -9 publisher; exit' SIGINT SIGTERM EXIT

REMOTE_HOST="dev@192.168.0.103"
kill_test() {
  while [ "`wc -l broker_stats.csv | cut -d ' ' -f 1`" -lt "6" ];
  do
    sleep 2
  done
  kill $(jobs -pr)
  killall -9 publisher
  killall run_test.sh
  killall -9 publisher
  tmux kill-session -t FunSecTest
  #ssh "$REMOTE_HOST" "killall run_publishers.sh"
  #ssh "$REMOTE_HOST" "killall -9 publisher"
}

mkdir -p results

for fun in $FUNS;
do
  for i in 100 200 300 400 500;
  do
    echo $fun $i
    start_fn_fun $fun $i

    rm broker_stats.csv
    touch broker_stats.csv
    kill_test &
    ./run_test.sh -t=12 -n=2 --skipfun
    echo "# `date +'%F %T'`, n=${i}" >> results/stream_${fun}_stats.csv
    tail -n +2 broker_stats.csv >> results/stream_${fun}_stats.csv
  done
done
